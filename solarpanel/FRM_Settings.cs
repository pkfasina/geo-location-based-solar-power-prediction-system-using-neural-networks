﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.OleDb;
using wmgCMS;
namespace solarpanel
{
   
    public partial class FRM_Settings : Form
    {
        wmgCMS.WaterMarkTextBox cls = new WaterMarkTextBox();
        sqlconnectionClass1 sqlconnection_obj = new sqlconnectionClass1();
        public FRM_Settings()
        {
            InitializeComponent();
        }
        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }
       private void  loadappliance()
        {
            comboappl.Items.Clear();
            OleDbConnection con = sqlconnection_obj.Database_Connection_Method();
            con.Open();
            OleDbCommand sqlcmd_select = new OleDbCommand("select DISTINCT  APPLIANCE FROM [power] ORDER BY APPLIANCE", con);
            OleDbDataAdapter da = new OleDbDataAdapter(sqlcmd_select);
            sqlcmd_select.ExecuteNonQuery();
            con.Close();
            DataSet ds = new DataSet();
            da.Fill(ds);
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                comboappl.Items.Add(ds.Tables[0].Rows[i][0].ToString());
            }


           
        }

        private void FRM_Settings_Load(object sender, EventArgs e)
        {
            for (int j = 1; j < 25; j++)
            {
                combohours.Items.Add(j.ToString());
            }

            loadappliance();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtappliance.Text != "" && txtload.Text != "")
                {
                    OleDbConnection con = sqlconnection_obj.Database_Connection_Method();
                    con.Open();
                    OleDbCommand sqlcmd_select = new OleDbCommand("insert into [power] ([APPLIANCE],[LOAD]) values ('" + txtappliance.Text + "','" + txtload.Text + "')", con);
                    sqlcmd_select.ExecuteNonQuery();
                    con.Close();
                    MessageBox.Show("Data Saved...");
                    txtload.Clear(); txtappliance.Clear();
                    loadappliance();
                }
                else
                {
                    MessageBox.Show("enter data");
                }
            }
            catch (Exception) { }
        }

        private void dgvpower_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
          
        }

        private void dgvpower_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void comboappl_KeyDown(object sender, KeyEventArgs e)
        {
           
        }

        private void comboappl_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void comboappl_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                comboload.Items.Clear();
                    comboload.Focus();
                    if (comboappl.SelectedItem.ToString() != "")
                    {
                        OleDbConnection con = sqlconnection_obj.Database_Connection_Method();
                        con.Open();
                        OleDbCommand sqlcmd_select = new OleDbCommand("select [LOAD] FROM [power] WHERE APPLIANCE='" + comboappl.SelectedItem.ToString() + "'", con);
                        OleDbDataAdapter da = new OleDbDataAdapter(sqlcmd_select);
                        sqlcmd_select.ExecuteNonQuery();
                        con.Close();
                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            comboload.Items.Add(ds.Tables[0].Rows[i][0].ToString());
                        }


                        
                    }
                
            }
            catch (Exception) { }
        }

        private void combohours_TextChanged(object sender, EventArgs e)
        {
            calc();
        }

        private void calc()
        {
            try
            {
                if ((comboappl.SelectedItem.ToString()) != "" && (comboload.SelectedItem.ToString() != "") && (txtno.Text != "") && (combohours.SelectedItem.ToString() != ""))
                    //&& (combohours.Text != ""))

                {
                    int lo = int.Parse(comboload.SelectedItem.ToString());
                    int no = int.Parse(txtno.Text);
                    int hr = int.Parse(combohours.SelectedItem.ToString());
                    txtunit.Text = (lo * no * hr).ToString();
                }
            }
            catch (Exception) { }
        }

        private void btnrowadd_Click(object sender, EventArgs e)
        {
            try
            {
                if ((comboappl.SelectedItem.ToString()) != "" && (comboload.SelectedItem.ToString() != "") && (txtno.Text != "") && (combohours.SelectedItem.ToString() != "")&&(txtunit.Text!=""))
                {

                    dgvpower.Rows.Add();      //add row
                    if (dgvpower.Height < 220)
                    {
                        dgvpower.Height += 25;
                    }
                    int x = dgvpower.Rows.Count;

                    dgvpower[0, x - 1].Value = comboappl.SelectedItem.ToString();
                    dgvpower[1, x - 1].Value = comboload.SelectedItem.ToString();
                    dgvpower[2, x - 1].Value = txtno.Text;
                    dgvpower[3, x - 1].Value = combohours.SelectedItem.ToString(); ;
                    dgvpower[4, x - 1].Value = txtunit.Text;

                    if (txttot.Text == "0" || txttot.Text == "")
                    {
                        txttot.Text = dgvpower[4, x - 1].Value.ToString();
                    }
                    else
                    {
                        txttot.Text = (float.Parse(txttot.Text) + float.Parse(dgvpower[4, x - 1].Value.ToString())).ToString();
                    }
                    comboappl.Text = ""; comboload.Text = ""; txtno.Clear(); combohours.Text = ""; txtunit.Clear();
                    this.ActiveControl = comboappl;
                }
                else
                {
                    MessageBox.Show("Pls Fill Required...");
                }


                //FRM_SOLAR_POWER_SIMULATE frmSet = (FRM_SOLAR_POWER_SIMULATE)Application.OpenForms["FRM_SOLAR_POWER_SIMULATE"];
                //frmSet.txt_user_poer_requirement.Text = txttot.Text;


                //this.Hide();
                //frmSet.Refresh();



            }
            catch (Exception) { }
        }

        private void FRM_Settings_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                FRM_Home frm_obj = (FRM_Home)this.MdiParent;
                frm_obj.g2.Visible = true; frm_obj.m1.Visible = true;   //frm_obj.g1.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void comboappl_Click(object sender, EventArgs e)
        {
           
        }

        private void txtno_Click(object sender, EventArgs e)
        {

            //txtno.WaterMarkText = "ss";
        }

        private void combohours_SelectedIndexChanged(object sender, EventArgs e)
        {
            //calc();
        }

        private void txtno_TextChanged(object sender, EventArgs e)
        {
            //calc();
        }

        private void comboload_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            dgvpower.Rows.Clear(); txttot.Clear();
        }

        private void txtunit_TextChanged(object sender, EventArgs e)
        {
            calc();
        }

        private void txttot_TextChanged(object sender, EventArgs e)
        {
            //FRM_SOLAR_POWER_SIMULATE frmSet = (FRM_SOLAR_POWER_SIMULATE)Application.OpenForms["FRM_SOLAR_POWER_SIMULATE"];
            //frmSet.txt_user_poer_requirement.Text = txttot.Text;


            //FRM_SOLAR_POWER_SIMULATE f = new FRM_SOLAR_POWER_SIMULATE();
            //f.txt_user_poer_requirement.Text = txttot.Text;

            



        }

        private void button2_Click(object sender, EventArgs e)
        {
            FRM_SOLAR_POWER_SIMULATE frm_simulate_obj = (FRM_SOLAR_POWER_SIMULATE)Application.OpenForms["FRM_SOLAR_POWER_SIMULATE"];

            frm_simulate_obj.txt_user_poer_requirement.Text = txttot.Text;
            this.Hide();
            frm_simulate_obj.Refresh();
        }
    }
}
