﻿namespace solarpanel
{
    partial class FRM_CLIMATEFETCHER
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer_timnpics = new System.Windows.Forms.Timer(this.components);
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txt_latitude = new System.Windows.Forms.TextBox();
            this.lbl_LONGITUDE = new System.Windows.Forms.Label();
            this.lbl_LATITUDE = new System.Windows.Forms.Label();
            this.combo_location = new System.Windows.Forms.ComboBox();
            this.lbl_location = new System.Windows.Forms.Label();
            this.BTN_LOC_fetch = new System.Windows.Forms.Button();
            this.txt_longitude = new System.Windows.Forms.TextBox();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // timer_timnpics
            // 
            this.timer_timnpics.Tick += new System.EventHandler(this.timer_timnpics_Tick);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txt_latitude);
            this.groupBox1.Controls.Add(this.lbl_LONGITUDE);
            this.groupBox1.Controls.Add(this.lbl_LATITUDE);
            this.groupBox1.Controls.Add(this.combo_location);
            this.groupBox1.Controls.Add(this.lbl_location);
            this.groupBox1.Location = new System.Drawing.Point(24, 44);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(643, 214);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // txt_latitude
            // 
            this.txt_latitude.BackColor = System.Drawing.Color.White;
            this.txt_latitude.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_latitude.ForeColor = System.Drawing.SystemColors.Highlight;
            this.txt_latitude.Location = new System.Drawing.Point(246, 111);
            this.txt_latitude.Margin = new System.Windows.Forms.Padding(4);
            this.txt_latitude.Name = "txt_latitude";
            this.txt_latitude.ReadOnly = true;
            this.txt_latitude.Size = new System.Drawing.Size(168, 30);
            this.txt_latitude.TabIndex = 14;
            this.txt_latitude.TextChanged += new System.EventHandler(this.txt_latitude_TextChanged);
            // 
            // lbl_LONGITUDE
            // 
            this.lbl_LONGITUDE.AutoSize = true;
            this.lbl_LONGITUDE.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_LONGITUDE.Location = new System.Drawing.Point(478, 123);
            this.lbl_LONGITUDE.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_LONGITUDE.Name = "lbl_LONGITUDE";
            this.lbl_LONGITUDE.Size = new System.Drawing.Size(81, 18);
            this.lbl_LONGITUDE.TabIndex = 13;
            this.lbl_LONGITUDE.Text = "Longitude";
            // 
            // lbl_LATITUDE
            // 
            this.lbl_LATITUDE.AutoSize = true;
            this.lbl_LATITUDE.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_LATITUDE.Location = new System.Drawing.Point(99, 116);
            this.lbl_LATITUDE.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_LATITUDE.Name = "lbl_LATITUDE";
            this.lbl_LATITUDE.Size = new System.Drawing.Size(67, 18);
            this.lbl_LATITUDE.TabIndex = 12;
            this.lbl_LATITUDE.Text = "Latitude";
            // 
            // combo_location
            // 
            this.combo_location.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.combo_location.FormattingEnabled = true;
            this.combo_location.Location = new System.Drawing.Point(246, 29);
            this.combo_location.Margin = new System.Windows.Forms.Padding(4);
            this.combo_location.Name = "combo_location";
            this.combo_location.Size = new System.Drawing.Size(253, 25);
            this.combo_location.TabIndex = 3;
            this.combo_location.Text = "         --- Select Place ---";
            this.combo_location.SelectedIndexChanged += new System.EventHandler(this.combo_location_SelectedIndexChanged);
            // 
            // lbl_location
            // 
            this.lbl_location.AutoSize = true;
            this.lbl_location.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_location.Location = new System.Drawing.Point(99, 41);
            this.lbl_location.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_location.Name = "lbl_location";
            this.lbl_location.Size = new System.Drawing.Size(78, 18);
            this.lbl_location.TabIndex = 2;
            this.lbl_location.Text = "Location ";
            // 
            // BTN_LOC_fetch
            // 
            this.BTN_LOC_fetch.BackColor = System.Drawing.Color.DodgerBlue;
            this.BTN_LOC_fetch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_LOC_fetch.ForeColor = System.Drawing.Color.White;
            this.BTN_LOC_fetch.Location = new System.Drawing.Point(609, 66);
            this.BTN_LOC_fetch.Margin = new System.Windows.Forms.Padding(4);
            this.BTN_LOC_fetch.Name = "BTN_LOC_fetch";
            this.BTN_LOC_fetch.Size = new System.Drawing.Size(169, 37);
            this.BTN_LOC_fetch.TabIndex = 16;
            this.BTN_LOC_fetch.Text = "Fetch Values";
            this.BTN_LOC_fetch.UseVisualStyleBackColor = false;
            this.BTN_LOC_fetch.Click += new System.EventHandler(this.BTN_LOC_fetch_Click);
            // 
            // txt_longitude
            // 
            this.txt_longitude.BackColor = System.Drawing.Color.White;
            this.txt_longitude.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_longitude.ForeColor = System.Drawing.SystemColors.Highlight;
            this.txt_longitude.Location = new System.Drawing.Point(609, 155);
            this.txt_longitude.Margin = new System.Windows.Forms.Padding(4);
            this.txt_longitude.Name = "txt_longitude";
            this.txt_longitude.ReadOnly = true;
            this.txt_longitude.Size = new System.Drawing.Size(168, 30);
            this.txt_longitude.TabIndex = 15;
            this.txt_longitude.TextChanged += new System.EventHandler(this.txt_longitude_TextChanged);
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // FRM_CLIMATEFETCHER
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(953, 341);
            this.Controls.Add(this.txt_longitude);
            this.Controls.Add(this.BTN_LOC_fetch);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FRM_CLIMATEFETCHER";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Climate Fetcher";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FRM_CLIMATEFETCHER_FormClosed);
            this.Load += new System.EventHandler(this.FRM_CLIMATEFETCHER_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Timer timer_timnpics;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox combo_location;
        private System.Windows.Forms.Label lbl_location;
        private System.Windows.Forms.TextBox txt_longitude;
        private System.Windows.Forms.TextBox txt_latitude;
        private System.Windows.Forms.Label lbl_LONGITUDE;
        private System.Windows.Forms.Label lbl_LATITUDE;
        private System.Windows.Forms.Button BTN_LOC_fetch;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
    }
}