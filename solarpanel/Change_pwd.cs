﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.OleDb;
namespace solarpanel
{
    public partial class Change_pwd : Form
    {
        sqlconnectionClass1 sqlconnection_obj = new sqlconnectionClass1();
        public Change_pwd()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                FRM_Home f = new FRM_Home();


                if (txtemail.Text != "" && txtoldpwd.Text != "")
                {
                    OleDbConnection con = sqlconnection_obj.Database_Connection_Method();
                    con.Open();
                    OleDbCommand sqlcmd_select = new OleDbCommand("select * from [User_Registration] where EMAILADDRESS='" + txtemail.Text + "'and PASSWORD='" + txtoldpwd.Text + "' ", con);
                    OleDbDataReader rdr = sqlcmd_select.ExecuteReader();
                    rdr.Read();
                    string usrname = rdr[3].ToString();
                    string opass = rdr[2].ToString();
                    rdr.Close();
                    con.Close();
                    string newPassword = txtnewpwd.Text;
                    string confirmPassword = txtconfmpwd.Text;
                    if (FRM_MainHome.username == usrname)
                    {
                        if (usrname != "" && opass != "")
                        {
                            if (newPassword == confirmPassword)
                            {
                                string sqlquery = "UPDATE [User_Registration] SET [PASSWORD]='" + newPassword + "' where [EMAILADDRESS]='" + txtemail.Text + "'";
                                string sqlquery1 = "UPDATE [Login] SET [PASSWORD]='" + newPassword + "' where [USER_NAME]='" + usrname + "'";
                                con.Open();
                                OleDbCommand cmd = new OleDbCommand(sqlquery, con);
                                OleDbCommand cmd1 = new OleDbCommand(sqlquery1, con);
                                cmd.ExecuteNonQuery();
                                cmd1.ExecuteNonQuery();
                                con.Close();


                                MessageBox.Show("password changed successfully"); txtconfmpwd.Clear(); txtnewpwd.Clear(); txtemail.Clear(); txtoldpwd.Clear();

                            }
                            else
                            {
                                MessageBox.Show("password mismatch");
                            }
                        }
                    }
                    else { MessageBox.Show("invalid username"); }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error" + ex.ToString());
            }

        }

        private void Change_pwd_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                FRM_Home frm_obj = (FRM_Home)this.MdiParent;
                frm_obj.groupBox2.Show(); frm_obj.monthCalendar1.Show();
            }
            catch (Exception)
            {
                //MessageBox.Show(ex.ToString());
            }
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
                FRM_Home frm_obj = (FRM_Home)Application.OpenForms["FRM_Home"];
                frm_obj.shw();

            }
            catch (Exception) { }
        }

        private void txtemail_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
