﻿namespace solarpanel
{
    partial class FRM_SOLAR_POWER_SIMULATE
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FRM_SOLAR_POWER_SIMULATE));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txt_current = new System.Windows.Forms.TextBox();
            this.txt_voltage = new System.Windows.Forms.TextBox();
            this.lbl_current = new System.Windows.Forms.Label();
            this.lbl_voltage = new System.Windows.Forms.Label();
            this.lbl_USERPOWERREQUIREMENT = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lbl_resultlength = new System.Windows.Forms.Label();
            this.lbl_lenght_day = new System.Windows.Forms.Label();
            this.lbl_result_totalCost = new System.Windows.Forms.Label();
            this.lbl_result_no_modules_parallel = new System.Windows.Forms.Label();
            this.lbl_result_no_modules_serial = new System.Windows.Forms.Label();
            this.lbl_result_angle = new System.Windows.Forms.Label();
            this.lbl_result_no_modules = new System.Windows.Forms.Label();
            this.lbl_totalcost = new System.Windows.Forms.Label();
            this.lbl_No_Module_serial = new System.Windows.Forms.Label();
            this.lbl_no_modules_parel = new System.Windows.Forms.Label();
            this.lbl_angle = new System.Windows.Forms.Label();
            this.lbl_no_modules = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.BTN_CLEAR = new System.Windows.Forms.Button();
            this.BTN_Calculate = new System.Windows.Forms.Button();
            this.BTN_mannualcorrection = new System.Windows.Forms.Button();
            this.BTN_LOCATIONFETCHER = new System.Windows.Forms.Button();
            this.BTN_Climatefetcher = new System.Windows.Forms.Button();
            this.BTN_BASECELL_CONFIG = new System.Windows.Forms.Button();
            this.lbl_time = new System.Windows.Forms.Label();
            this.lbl_loginname = new System.Windows.Forms.Label();
            this.linklbl_home = new System.Windows.Forms.LinkLabel();
            this.timer_timnpics = new System.Windows.Forms.Timer(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutProjectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.benifitsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.lbl_result_humid = new System.Windows.Forms.Label();
            this.lbl_humidi = new System.Windows.Forms.Label();
            this.lbl_result_pressure = new System.Windows.Forms.Label();
            this.lbl_pressure = new System.Windows.Forms.Label();
            this.lbl_result_longi = new System.Windows.Forms.Label();
            this.lbl_result_climate = new System.Windows.Forms.Label();
            this.lbl_result_latitude = new System.Windows.Forms.Label();
            this.lbl_result_location = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.lbl_result_modulerating = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lbl_result_voltage = new System.Windows.Forms.Label();
            this.lbl_result_cost_module = new System.Windows.Forms.Label();
            this.lbl_result_modultarea = new System.Windows.Forms.Label();
            this.lbl_result_typematerial = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lbl_coast = new System.Windows.Forms.Label();
            this.lbl_modulearea = new System.Windows.Forms.Label();
            this.lbl_panelmaterial = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.lbl_result_errorlevel = new System.Windows.Forms.Label();
            this.lbl_result_climatecondition = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.dateTimePic_calc = new System.Windows.Forms.DateTimePicker();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.txt_user_poer_requirement = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.printPreviewDialog1 = new System.Windows.Forms.PrintPreviewDialog();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txt_current);
            this.groupBox1.Controls.Add(this.txt_voltage);
            this.groupBox1.Controls.Add(this.lbl_current);
            this.groupBox1.Controls.Add(this.lbl_voltage);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(1303, 102);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(398, 172);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Battery Specifications";
            // 
            // txt_current
            // 
            this.txt_current.Location = new System.Drawing.Point(185, 112);
            this.txt_current.Margin = new System.Windows.Forms.Padding(4);
            this.txt_current.Name = "txt_current";
            this.txt_current.Size = new System.Drawing.Size(177, 23);
            this.txt_current.TabIndex = 3;
            this.txt_current.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_user_poer_requirement_KeyPress);
            // 
            // txt_voltage
            // 
            this.txt_voltage.Location = new System.Drawing.Point(185, 42);
            this.txt_voltage.Margin = new System.Windows.Forms.Padding(4);
            this.txt_voltage.Name = "txt_voltage";
            this.txt_voltage.Size = new System.Drawing.Size(177, 23);
            this.txt_voltage.TabIndex = 2;
            this.txt_voltage.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_user_poer_requirement_KeyPress);
            // 
            // lbl_current
            // 
            this.lbl_current.AutoSize = true;
            this.lbl_current.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_current.Location = new System.Drawing.Point(15, 112);
            this.lbl_current.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_current.Name = "lbl_current";
            this.lbl_current.Size = new System.Drawing.Size(100, 17);
            this.lbl_current.TabIndex = 1;
            this.lbl_current.Text = "Current (AH)";
            // 
            // lbl_voltage
            // 
            this.lbl_voltage.AutoSize = true;
            this.lbl_voltage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.lbl_voltage.Location = new System.Drawing.Point(20, 42);
            this.lbl_voltage.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_voltage.Name = "lbl_voltage";
            this.lbl_voltage.Size = new System.Drawing.Size(85, 17);
            this.lbl_voltage.TabIndex = 0;
            this.lbl_voltage.Text = "Voltage(V)";
            // 
            // lbl_USERPOWERREQUIREMENT
            // 
            this.lbl_USERPOWERREQUIREMENT.AutoSize = true;
            this.lbl_USERPOWERREQUIREMENT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.lbl_USERPOWERREQUIREMENT.Location = new System.Drawing.Point(653, 120);
            this.lbl_USERPOWERREQUIREMENT.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_USERPOWERREQUIREMENT.Name = "lbl_USERPOWERREQUIREMENT";
            this.lbl_USERPOWERREQUIREMENT.Size = new System.Drawing.Size(281, 17);
            this.lbl_USERPOWERREQUIREMENT.TabIndex = 1;
            this.lbl_USERPOWERREQUIREMENT.Text = "USER POWER REQUIREMENT (KWH)";
            this.lbl_USERPOWERREQUIREMENT.Click += new System.EventHandler(this.lbl_USERPOWERREQUIREMENT_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.lbl_resultlength);
            this.groupBox2.Controls.Add(this.lbl_lenght_day);
            this.groupBox2.Controls.Add(this.lbl_result_totalCost);
            this.groupBox2.Controls.Add(this.lbl_result_no_modules_parallel);
            this.groupBox2.Controls.Add(this.lbl_result_no_modules_serial);
            this.groupBox2.Controls.Add(this.lbl_result_angle);
            this.groupBox2.Controls.Add(this.lbl_result_no_modules);
            this.groupBox2.Controls.Add(this.lbl_totalcost);
            this.groupBox2.Controls.Add(this.lbl_No_Module_serial);
            this.groupBox2.Controls.Add(this.lbl_no_modules_parel);
            this.groupBox2.Controls.Add(this.lbl_angle);
            this.groupBox2.Controls.Add(this.lbl_no_modules);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(16, 522);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(1269, 267);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "EXPERT INFO";
            // 
            // label11
            // 
            this.label11.BackColor = System.Drawing.Color.GhostWhite;
            this.label11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label11.Location = new System.Drawing.Point(857, 192);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(235, 27);
            this.label11.TabIndex = 16;
            this.label11.Text = "                                 ";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(642, 192);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(59, 17);
            this.label10.TabIndex = 15;
            this.label10.Text = "Energy";
            // 
            // lbl_resultlength
            // 
            this.lbl_resultlength.BackColor = System.Drawing.Color.GhostWhite;
            this.lbl_resultlength.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_resultlength.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_resultlength.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl_resultlength.Location = new System.Drawing.Point(857, 136);
            this.lbl_resultlength.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_resultlength.Name = "lbl_resultlength";
            this.lbl_resultlength.Size = new System.Drawing.Size(235, 27);
            this.lbl_resultlength.TabIndex = 14;
            this.lbl_resultlength.Text = "                                 ";
            this.lbl_resultlength.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_lenght_day
            // 
            this.lbl_lenght_day.AutoSize = true;
            this.lbl_lenght_day.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_lenght_day.Location = new System.Drawing.Point(592, 143);
            this.lbl_lenght_day.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_lenght_day.Name = "lbl_lenght_day";
            this.lbl_lenght_day.Size = new System.Drawing.Size(146, 17);
            this.lbl_lenght_day.TabIndex = 13;
            this.lbl_lenght_day.Text = "Length Of The Day";
            // 
            // lbl_result_totalCost
            // 
            this.lbl_result_totalCost.BackColor = System.Drawing.Color.GhostWhite;
            this.lbl_result_totalCost.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_result_totalCost.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_result_totalCost.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl_result_totalCost.Location = new System.Drawing.Point(263, 152);
            this.lbl_result_totalCost.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_result_totalCost.Name = "lbl_result_totalCost";
            this.lbl_result_totalCost.Size = new System.Drawing.Size(235, 27);
            this.lbl_result_totalCost.TabIndex = 12;
            this.lbl_result_totalCost.Text = "                                 ";
            this.lbl_result_totalCost.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_result_no_modules_parallel
            // 
            this.lbl_result_no_modules_parallel.BackColor = System.Drawing.Color.GhostWhite;
            this.lbl_result_no_modules_parallel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_result_no_modules_parallel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_result_no_modules_parallel.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl_result_no_modules_parallel.Location = new System.Drawing.Point(857, 26);
            this.lbl_result_no_modules_parallel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_result_no_modules_parallel.Name = "lbl_result_no_modules_parallel";
            this.lbl_result_no_modules_parallel.Size = new System.Drawing.Size(235, 27);
            this.lbl_result_no_modules_parallel.TabIndex = 11;
            this.lbl_result_no_modules_parallel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_result_no_modules_serial
            // 
            this.lbl_result_no_modules_serial.BackColor = System.Drawing.Color.GhostWhite;
            this.lbl_result_no_modules_serial.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_result_no_modules_serial.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_result_no_modules_serial.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl_result_no_modules_serial.Location = new System.Drawing.Point(857, 84);
            this.lbl_result_no_modules_serial.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_result_no_modules_serial.Name = "lbl_result_no_modules_serial";
            this.lbl_result_no_modules_serial.Size = new System.Drawing.Size(235, 27);
            this.lbl_result_no_modules_serial.TabIndex = 10;
            this.lbl_result_no_modules_serial.Text = "                                 ";
            this.lbl_result_no_modules_serial.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_result_angle
            // 
            this.lbl_result_angle.BackColor = System.Drawing.Color.GhostWhite;
            this.lbl_result_angle.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_result_angle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_result_angle.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl_result_angle.Location = new System.Drawing.Point(263, 103);
            this.lbl_result_angle.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_result_angle.Name = "lbl_result_angle";
            this.lbl_result_angle.Size = new System.Drawing.Size(235, 27);
            this.lbl_result_angle.TabIndex = 9;
            this.lbl_result_angle.Text = "                                 ";
            this.lbl_result_angle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_result_no_modules
            // 
            this.lbl_result_no_modules.BackColor = System.Drawing.Color.GhostWhite;
            this.lbl_result_no_modules.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_result_no_modules.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_result_no_modules.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl_result_no_modules.Location = new System.Drawing.Point(263, 46);
            this.lbl_result_no_modules.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_result_no_modules.Name = "lbl_result_no_modules";
            this.lbl_result_no_modules.Size = new System.Drawing.Size(235, 27);
            this.lbl_result_no_modules.TabIndex = 8;
            this.lbl_result_no_modules.Text = "                                 ";
            this.lbl_result_no_modules.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_totalcost
            // 
            this.lbl_totalcost.AutoSize = true;
            this.lbl_totalcost.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_totalcost.Location = new System.Drawing.Point(99, 159);
            this.lbl_totalcost.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_totalcost.Name = "lbl_totalcost";
            this.lbl_totalcost.Size = new System.Drawing.Size(82, 17);
            this.lbl_totalcost.TabIndex = 7;
            this.lbl_totalcost.Text = "Total Cost";
            // 
            // lbl_No_Module_serial
            // 
            this.lbl_No_Module_serial.AutoSize = true;
            this.lbl_No_Module_serial.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_No_Module_serial.Location = new System.Drawing.Point(592, 91);
            this.lbl_No_Module_serial.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_No_Module_serial.Name = "lbl_No_Module_serial";
            this.lbl_No_Module_serial.Size = new System.Drawing.Size(167, 17);
            this.lbl_No_Module_serial.TabIndex = 6;
            this.lbl_No_Module_serial.Text = "No. Of Modules Serial";
            // 
            // lbl_no_modules_parel
            // 
            this.lbl_no_modules_parel.AutoSize = true;
            this.lbl_no_modules_parel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_no_modules_parel.Location = new System.Drawing.Point(592, 36);
            this.lbl_no_modules_parel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_no_modules_parel.Name = "lbl_no_modules_parel";
            this.lbl_no_modules_parel.Size = new System.Drawing.Size(180, 17);
            this.lbl_no_modules_parel.TabIndex = 5;
            this.lbl_no_modules_parel.Text = "No. Of Modules Parallel";
            // 
            // lbl_angle
            // 
            this.lbl_angle.AutoSize = true;
            this.lbl_angle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_angle.Location = new System.Drawing.Point(110, 110);
            this.lbl_angle.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_angle.Name = "lbl_angle";
            this.lbl_angle.Size = new System.Drawing.Size(49, 17);
            this.lbl_angle.TabIndex = 4;
            this.lbl_angle.Text = "Angle";
            // 
            // lbl_no_modules
            // 
            this.lbl_no_modules.AutoSize = true;
            this.lbl_no_modules.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_no_modules.Location = new System.Drawing.Point(99, 53);
            this.lbl_no_modules.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_no_modules.Name = "lbl_no_modules";
            this.lbl_no_modules.Size = new System.Drawing.Size(120, 17);
            this.lbl_no_modules.TabIndex = 2;
            this.lbl_no_modules.Text = "No. Of Modules";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.button1);
            this.groupBox3.Controls.Add(this.BTN_CLEAR);
            this.groupBox3.Controls.Add(this.BTN_Calculate);
            this.groupBox3.Controls.Add(this.BTN_mannualcorrection);
            this.groupBox3.Controls.Add(this.BTN_LOCATIONFETCHER);
            this.groupBox3.Controls.Add(this.BTN_Climatefetcher);
            this.groupBox3.Controls.Add(this.BTN_BASECELL_CONFIG);
            this.groupBox3.Location = new System.Drawing.Point(1303, 283);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox3.Size = new System.Drawing.Size(399, 506);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.DodgerBlue;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.button1.Location = new System.Drawing.Point(56, 95);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(261, 47);
            this.button1.TabIndex = 7;
            this.button1.Text = "Power Calculator";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // BTN_CLEAR
            // 
            this.BTN_CLEAR.BackColor = System.Drawing.Color.DodgerBlue;
            this.BTN_CLEAR.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_CLEAR.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.BTN_CLEAR.Location = new System.Drawing.Point(56, 443);
            this.BTN_CLEAR.Margin = new System.Windows.Forms.Padding(4);
            this.BTN_CLEAR.Name = "BTN_CLEAR";
            this.BTN_CLEAR.Size = new System.Drawing.Size(261, 43);
            this.BTN_CLEAR.TabIndex = 6;
            this.BTN_CLEAR.Text = "Clear";
            this.BTN_CLEAR.UseVisualStyleBackColor = false;
            this.BTN_CLEAR.Click += new System.EventHandler(this.BTN_CLEAR_Click);
            // 
            // BTN_Calculate
            // 
            this.BTN_Calculate.BackColor = System.Drawing.Color.DodgerBlue;
            this.BTN_Calculate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_Calculate.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.BTN_Calculate.Location = new System.Drawing.Point(56, 375);
            this.BTN_Calculate.Margin = new System.Windows.Forms.Padding(4);
            this.BTN_Calculate.Name = "BTN_Calculate";
            this.BTN_Calculate.Size = new System.Drawing.Size(261, 43);
            this.BTN_Calculate.TabIndex = 5;
            this.BTN_Calculate.Text = "Calculate";
            this.BTN_Calculate.UseVisualStyleBackColor = false;
            this.BTN_Calculate.Click += new System.EventHandler(this.BTN_Calculate_Click);
            // 
            // BTN_mannualcorrection
            // 
            this.BTN_mannualcorrection.BackColor = System.Drawing.Color.DodgerBlue;
            this.BTN_mannualcorrection.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_mannualcorrection.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.BTN_mannualcorrection.Location = new System.Drawing.Point(56, 307);
            this.BTN_mannualcorrection.Margin = new System.Windows.Forms.Padding(4);
            this.BTN_mannualcorrection.Name = "BTN_mannualcorrection";
            this.BTN_mannualcorrection.Size = new System.Drawing.Size(261, 43);
            this.BTN_mannualcorrection.TabIndex = 4;
            this.BTN_mannualcorrection.Text = "Mannual Correction";
            this.BTN_mannualcorrection.UseVisualStyleBackColor = false;
            this.BTN_mannualcorrection.Click += new System.EventHandler(this.BTN_mannualcorrection_Click);
            // 
            // BTN_LOCATIONFETCHER
            // 
            this.BTN_LOCATIONFETCHER.BackColor = System.Drawing.Color.DodgerBlue;
            this.BTN_LOCATIONFETCHER.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_LOCATIONFETCHER.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.BTN_LOCATIONFETCHER.Location = new System.Drawing.Point(56, 169);
            this.BTN_LOCATIONFETCHER.Margin = new System.Windows.Forms.Padding(4);
            this.BTN_LOCATIONFETCHER.Name = "BTN_LOCATIONFETCHER";
            this.BTN_LOCATIONFETCHER.Size = new System.Drawing.Size(261, 47);
            this.BTN_LOCATIONFETCHER.TabIndex = 3;
            this.BTN_LOCATIONFETCHER.Text = "Location Fetcher";
            this.BTN_LOCATIONFETCHER.UseVisualStyleBackColor = false;
            this.BTN_LOCATIONFETCHER.Click += new System.EventHandler(this.BTN_LOCATIONFETCHER_Click);
            // 
            // BTN_Climatefetcher
            // 
            this.BTN_Climatefetcher.BackColor = System.Drawing.Color.DodgerBlue;
            this.BTN_Climatefetcher.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_Climatefetcher.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.BTN_Climatefetcher.Location = new System.Drawing.Point(56, 239);
            this.BTN_Climatefetcher.Margin = new System.Windows.Forms.Padding(4);
            this.BTN_Climatefetcher.Name = "BTN_Climatefetcher";
            this.BTN_Climatefetcher.Size = new System.Drawing.Size(261, 47);
            this.BTN_Climatefetcher.TabIndex = 2;
            this.BTN_Climatefetcher.Text = "Climate Fetcher";
            this.BTN_Climatefetcher.UseVisualStyleBackColor = false;
            this.BTN_Climatefetcher.Click += new System.EventHandler(this.BTN_Climatefetcher_Click);
            // 
            // BTN_BASECELL_CONFIG
            // 
            this.BTN_BASECELL_CONFIG.BackColor = System.Drawing.Color.DodgerBlue;
            this.BTN_BASECELL_CONFIG.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_BASECELL_CONFIG.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.BTN_BASECELL_CONFIG.Location = new System.Drawing.Point(56, 23);
            this.BTN_BASECELL_CONFIG.Margin = new System.Windows.Forms.Padding(4);
            this.BTN_BASECELL_CONFIG.Name = "BTN_BASECELL_CONFIG";
            this.BTN_BASECELL_CONFIG.Size = new System.Drawing.Size(261, 47);
            this.BTN_BASECELL_CONFIG.TabIndex = 1;
            this.BTN_BASECELL_CONFIG.Text = "Base Cell Configuration";
            this.BTN_BASECELL_CONFIG.UseVisualStyleBackColor = false;
            this.BTN_BASECELL_CONFIG.Click += new System.EventHandler(this.BTN_BASECELL_CONFIG_Click);
            // 
            // lbl_time
            // 
            this.lbl_time.AutoSize = true;
            this.lbl_time.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_time.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl_time.Location = new System.Drawing.Point(17, 816);
            this.lbl_time.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_time.Name = "lbl_time";
            this.lbl_time.Size = new System.Drawing.Size(40, 18);
            this.lbl_time.TabIndex = 6;
            this.lbl_time.Text = "time";
            // 
            // lbl_loginname
            // 
            this.lbl_loginname.AutoSize = true;
            this.lbl_loginname.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_loginname.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl_loginname.Location = new System.Drawing.Point(1466, 6);
            this.lbl_loginname.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_loginname.Name = "lbl_loginname";
            this.lbl_loginname.Size = new System.Drawing.Size(100, 18);
            this.lbl_loginname.TabIndex = 7;
            this.lbl_loginname.Text = "Username : ";
            // 
            // linklbl_home
            // 
            this.linklbl_home.AutoSize = true;
            this.linklbl_home.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.linklbl_home.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linklbl_home.Location = new System.Drawing.Point(731, 1);
            this.linklbl_home.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.linklbl_home.Name = "linklbl_home";
            this.linklbl_home.Size = new System.Drawing.Size(0, 25);
            this.linklbl_home.TabIndex = 8;
            this.linklbl_home.Visible = false;
            this.linklbl_home.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linklbl_home_LinkClicked);
            // 
            // timer_timnpics
            // 
            this.timer_timnpics.Tick += new System.EventHandler(this.timer_timnpics_Tick);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem,
            this.helpToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1787, 28);
            this.menuStrip1.TabIndex = 9;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutProjectToolStripMenuItem,
            this.benifitsToolStripMenuItem});
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(62, 24);
            this.aboutToolStripMenuItem.Text = "About";
            // 
            // aboutProjectToolStripMenuItem
            // 
            this.aboutProjectToolStripMenuItem.Name = "aboutProjectToolStripMenuItem";
            this.aboutProjectToolStripMenuItem.Size = new System.Drawing.Size(169, 24);
            this.aboutProjectToolStripMenuItem.Text = "About Project";
            this.aboutProjectToolStripMenuItem.Click += new System.EventHandler(this.aboutProjectToolStripMenuItem_Click);
            // 
            // benifitsToolStripMenuItem
            // 
            this.benifitsToolStripMenuItem.Name = "benifitsToolStripMenuItem";
            this.benifitsToolStripMenuItem.Size = new System.Drawing.Size(169, 24);
            this.benifitsToolStripMenuItem.Text = "Benifits";
            this.benifitsToolStripMenuItem.Click += new System.EventHandler(this.benifitsToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(53, 24);
            this.helpToolStripMenuItem.Text = "Help";
            this.helpToolStripMenuItem.Click += new System.EventHandler(this.helpToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(62, 24);
            this.exitToolStripMenuItem.Text = "Home";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label14);
            this.groupBox4.Controls.Add(this.label15);
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Controls.Add(this.lbl_result_humid);
            this.groupBox4.Controls.Add(this.lbl_humidi);
            this.groupBox4.Controls.Add(this.lbl_result_pressure);
            this.groupBox4.Controls.Add(this.lbl_pressure);
            this.groupBox4.Controls.Add(this.lbl_result_longi);
            this.groupBox4.Controls.Add(this.lbl_result_climate);
            this.groupBox4.Controls.Add(this.lbl_result_latitude);
            this.groupBox4.Controls.Add(this.lbl_result_location);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.label3);
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(656, 184);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox4.Size = new System.Drawing.Size(629, 330);
            this.groupBox4.TabIndex = 11;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Location And Climate";
            this.groupBox4.Enter += new System.EventHandler(this.groupBox4_Enter);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Red;
            this.label14.Location = new System.Drawing.Point(482, 268);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(21, 17);
            this.label14.TabIndex = 18;
            this.label14.Text = "%";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Red;
            this.label15.Location = new System.Drawing.Point(482, 217);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(35, 17);
            this.label15.TabIndex = 19;
            this.label15.Text = "hpa";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Red;
            this.label16.Location = new System.Drawing.Point(482, 163);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(82, 17);
            this.label16.TabIndex = 20;
            this.label16.Text = "fahrenheit";
            // 
            // lbl_result_humid
            // 
            this.lbl_result_humid.BackColor = System.Drawing.Color.GhostWhite;
            this.lbl_result_humid.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_result_humid.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_result_humid.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl_result_humid.Location = new System.Drawing.Point(153, 261);
            this.lbl_result_humid.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_result_humid.Name = "lbl_result_humid";
            this.lbl_result_humid.Size = new System.Drawing.Size(321, 27);
            this.lbl_result_humid.TabIndex = 19;
            this.lbl_result_humid.Text = "00.00";
            this.lbl_result_humid.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_humidi
            // 
            this.lbl_humidi.AutoSize = true;
            this.lbl_humidi.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_humidi.Location = new System.Drawing.Point(21, 271);
            this.lbl_humidi.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_humidi.Name = "lbl_humidi";
            this.lbl_humidi.Size = new System.Drawing.Size(70, 17);
            this.lbl_humidi.TabIndex = 18;
            this.lbl_humidi.Text = "Humidity";
            // 
            // lbl_result_pressure
            // 
            this.lbl_result_pressure.BackColor = System.Drawing.Color.GhostWhite;
            this.lbl_result_pressure.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_result_pressure.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_result_pressure.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl_result_pressure.Location = new System.Drawing.Point(153, 214);
            this.lbl_result_pressure.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_result_pressure.Name = "lbl_result_pressure";
            this.lbl_result_pressure.Size = new System.Drawing.Size(321, 27);
            this.lbl_result_pressure.TabIndex = 17;
            this.lbl_result_pressure.Text = "00.00";
            this.lbl_result_pressure.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_result_pressure.Click += new System.EventHandler(this.lbl_result_pressure_Click);
            // 
            // lbl_pressure
            // 
            this.lbl_pressure.AutoSize = true;
            this.lbl_pressure.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_pressure.Location = new System.Drawing.Point(18, 217);
            this.lbl_pressure.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_pressure.Name = "lbl_pressure";
            this.lbl_pressure.Size = new System.Drawing.Size(73, 17);
            this.lbl_pressure.TabIndex = 16;
            this.lbl_pressure.Text = "Pressure";
            // 
            // lbl_result_longi
            // 
            this.lbl_result_longi.BackColor = System.Drawing.Color.GhostWhite;
            this.lbl_result_longi.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_result_longi.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_result_longi.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl_result_longi.Location = new System.Drawing.Point(414, 85);
            this.lbl_result_longi.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_result_longi.Name = "lbl_result_longi";
            this.lbl_result_longi.Size = new System.Drawing.Size(135, 27);
            this.lbl_result_longi.TabIndex = 15;
            this.lbl_result_longi.Text = "00.00";
            this.lbl_result_longi.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_result_longi.Click += new System.EventHandler(this.lbl_result_longi_Click);
            // 
            // lbl_result_climate
            // 
            this.lbl_result_climate.BackColor = System.Drawing.Color.GhostWhite;
            this.lbl_result_climate.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_result_climate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_result_climate.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl_result_climate.Location = new System.Drawing.Point(153, 159);
            this.lbl_result_climate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_result_climate.Name = "lbl_result_climate";
            this.lbl_result_climate.Size = new System.Drawing.Size(321, 27);
            this.lbl_result_climate.TabIndex = 14;
            this.lbl_result_climate.Text = "00.00";
            this.lbl_result_climate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_result_climate.Click += new System.EventHandler(this.lbl_result_climate_Click);
            // 
            // lbl_result_latitude
            // 
            this.lbl_result_latitude.BackColor = System.Drawing.Color.GhostWhite;
            this.lbl_result_latitude.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_result_latitude.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_result_latitude.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl_result_latitude.Location = new System.Drawing.Point(153, 87);
            this.lbl_result_latitude.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_result_latitude.Name = "lbl_result_latitude";
            this.lbl_result_latitude.Size = new System.Drawing.Size(135, 27);
            this.lbl_result_latitude.TabIndex = 10;
            this.lbl_result_latitude.Text = "00.00 ";
            this.lbl_result_latitude.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_result_latitude.Click += new System.EventHandler(this.lbl_result_latitude_Click);
            // 
            // lbl_result_location
            // 
            this.lbl_result_location.BackColor = System.Drawing.Color.GhostWhite;
            this.lbl_result_location.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_result_location.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_result_location.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl_result_location.Location = new System.Drawing.Point(153, 30);
            this.lbl_result_location.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_result_location.Name = "lbl_result_location";
            this.lbl_result_location.Size = new System.Drawing.Size(315, 36);
            this.lbl_result_location.TabIndex = 9;
            this.lbl_result_location.Text = "--Select--";
            this.lbl_result_location.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_result_location.Click += new System.EventHandler(this.lbl_result_location_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(307, 94);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 17);
            this.label4.TabIndex = 6;
            this.label4.Text = "Longitude";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(13, 159);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(101, 17);
            this.label3.TabIndex = 5;
            this.label3.Text = "Temperature";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(28, 98);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "Lattitude";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(28, 33);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "Location";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.lbl_result_modulerating);
            this.groupBox5.Controls.Add(this.label9);
            this.groupBox5.Controls.Add(this.lbl_result_voltage);
            this.groupBox5.Controls.Add(this.lbl_result_cost_module);
            this.groupBox5.Controls.Add(this.lbl_result_modultarea);
            this.groupBox5.Controls.Add(this.lbl_result_typematerial);
            this.groupBox5.Controls.Add(this.label5);
            this.groupBox5.Controls.Add(this.lbl_coast);
            this.groupBox5.Controls.Add(this.lbl_modulearea);
            this.groupBox5.Controls.Add(this.lbl_panelmaterial);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.Location = new System.Drawing.Point(16, 220);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox5.Size = new System.Drawing.Size(629, 294);
            this.groupBox5.TabIndex = 12;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Cell Configuration";
            this.groupBox5.Enter += new System.EventHandler(this.groupBox5_Enter);
            // 
            // lbl_result_modulerating
            // 
            this.lbl_result_modulerating.BackColor = System.Drawing.Color.GhostWhite;
            this.lbl_result_modulerating.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_result_modulerating.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_result_modulerating.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl_result_modulerating.Location = new System.Drawing.Point(293, 243);
            this.lbl_result_modulerating.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_result_modulerating.Name = "lbl_result_modulerating";
            this.lbl_result_modulerating.Size = new System.Drawing.Size(328, 27);
            this.lbl_result_modulerating.TabIndex = 16;
            this.lbl_result_modulerating.Text = "00.00";
            this.lbl_result_modulerating.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(21, 249);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(112, 17);
            this.label9.TabIndex = 15;
            this.label9.Text = "Module Rating";
            // 
            // lbl_result_voltage
            // 
            this.lbl_result_voltage.BackColor = System.Drawing.Color.GhostWhite;
            this.lbl_result_voltage.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_result_voltage.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_result_voltage.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl_result_voltage.Location = new System.Drawing.Point(293, 190);
            this.lbl_result_voltage.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_result_voltage.Name = "lbl_result_voltage";
            this.lbl_result_voltage.Size = new System.Drawing.Size(328, 27);
            this.lbl_result_voltage.TabIndex = 14;
            this.lbl_result_voltage.Text = "00.00";
            this.lbl_result_voltage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_result_voltage.Click += new System.EventHandler(this.lbl_result_voltage_Click);
            // 
            // lbl_result_cost_module
            // 
            this.lbl_result_cost_module.BackColor = System.Drawing.Color.GhostWhite;
            this.lbl_result_cost_module.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_result_cost_module.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_result_cost_module.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl_result_cost_module.Location = new System.Drawing.Point(293, 144);
            this.lbl_result_cost_module.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_result_cost_module.Name = "lbl_result_cost_module";
            this.lbl_result_cost_module.Size = new System.Drawing.Size(328, 27);
            this.lbl_result_cost_module.TabIndex = 13;
            this.lbl_result_cost_module.Text = "00.00";
            this.lbl_result_cost_module.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_result_cost_module.Click += new System.EventHandler(this.lbl_result_cost_module_Click);
            // 
            // lbl_result_modultarea
            // 
            this.lbl_result_modultarea.BackColor = System.Drawing.Color.GhostWhite;
            this.lbl_result_modultarea.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_result_modultarea.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_result_modultarea.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl_result_modultarea.Location = new System.Drawing.Point(293, 94);
            this.lbl_result_modultarea.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_result_modultarea.Name = "lbl_result_modultarea";
            this.lbl_result_modultarea.Size = new System.Drawing.Size(328, 27);
            this.lbl_result_modultarea.TabIndex = 12;
            this.lbl_result_modultarea.Text = "Unit x Unit";
            this.lbl_result_modultarea.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_result_modultarea.Click += new System.EventHandler(this.lbl_result_modultarea_Click);
            // 
            // lbl_result_typematerial
            // 
            this.lbl_result_typematerial.BackColor = System.Drawing.Color.GhostWhite;
            this.lbl_result_typematerial.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_result_typematerial.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_result_typematerial.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl_result_typematerial.Location = new System.Drawing.Point(293, 43);
            this.lbl_result_typematerial.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_result_typematerial.Name = "lbl_result_typematerial";
            this.lbl_result_typematerial.Size = new System.Drawing.Size(328, 27);
            this.lbl_result_typematerial.TabIndex = 11;
            this.lbl_result_typematerial.Text = "--Select--";
            this.lbl_result_typematerial.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_result_typematerial.Click += new System.EventHandler(this.lbl_result_typematerial_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(17, 199);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(105, 17);
            this.label5.TabIndex = 10;
            this.label5.Text = "Voltage / Cell";
            // 
            // lbl_coast
            // 
            this.lbl_coast.AutoSize = true;
            this.lbl_coast.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.lbl_coast.Location = new System.Drawing.Point(17, 150);
            this.lbl_coast.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_coast.Name = "lbl_coast";
            this.lbl_coast.Size = new System.Drawing.Size(107, 17);
            this.lbl_coast.TabIndex = 9;
            this.lbl_coast.Text = "Cost / Module";
            // 
            // lbl_modulearea
            // 
            this.lbl_modulearea.AutoSize = true;
            this.lbl_modulearea.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.lbl_modulearea.Location = new System.Drawing.Point(17, 101);
            this.lbl_modulearea.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_modulearea.Name = "lbl_modulearea";
            this.lbl_modulearea.Size = new System.Drawing.Size(99, 17);
            this.lbl_modulearea.TabIndex = 8;
            this.lbl_modulearea.Text = "Module Area";
            // 
            // lbl_panelmaterial
            // 
            this.lbl_panelmaterial.AutoSize = true;
            this.lbl_panelmaterial.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.lbl_panelmaterial.Location = new System.Drawing.Point(6, 49);
            this.lbl_panelmaterial.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_panelmaterial.Name = "lbl_panelmaterial";
            this.lbl_panelmaterial.Size = new System.Drawing.Size(175, 17);
            this.lbl_panelmaterial.TabIndex = 7;
            this.lbl_panelmaterial.Text = "Type Of Panel Material";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.lbl_result_errorlevel);
            this.groupBox6.Controls.Add(this.lbl_result_climatecondition);
            this.groupBox6.Controls.Add(this.label6);
            this.groupBox6.Controls.Add(this.label7);
            this.groupBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox6.Location = new System.Drawing.Point(20, 40);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox6.Size = new System.Drawing.Size(625, 172);
            this.groupBox6.TabIndex = 13;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Manual Correction";
            // 
            // lbl_result_errorlevel
            // 
            this.lbl_result_errorlevel.BackColor = System.Drawing.Color.GhostWhite;
            this.lbl_result_errorlevel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_result_errorlevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_result_errorlevel.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl_result_errorlevel.Location = new System.Drawing.Point(310, 104);
            this.lbl_result_errorlevel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_result_errorlevel.Name = "lbl_result_errorlevel";
            this.lbl_result_errorlevel.Size = new System.Drawing.Size(235, 27);
            this.lbl_result_errorlevel.TabIndex = 81;
            this.lbl_result_errorlevel.Text = "00.00";
            this.lbl_result_errorlevel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_result_errorlevel.Click += new System.EventHandler(this.lbl_result_errorlevel_Click);
            // 
            // lbl_result_climatecondition
            // 
            this.lbl_result_climatecondition.BackColor = System.Drawing.Color.GhostWhite;
            this.lbl_result_climatecondition.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_result_climatecondition.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_result_climatecondition.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl_result_climatecondition.Location = new System.Drawing.Point(310, 41);
            this.lbl_result_climatecondition.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_result_climatecondition.Name = "lbl_result_climatecondition";
            this.lbl_result_climatecondition.Size = new System.Drawing.Size(235, 27);
            this.lbl_result_climatecondition.TabIndex = 80;
            this.lbl_result_climatecondition.Text = "--Select--";
            this.lbl_result_climatecondition.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_result_climatecondition.Click += new System.EventHandler(this.lbl_result_climatecondition_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(17, 107);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(116, 17);
            this.label6.TabIndex = 79;
            this.label6.Text = "ERROR LEVEL";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(13, 47);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(163, 17);
            this.label7.TabIndex = 78;
            this.label7.Text = "CLIMATE CONDITION";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Silver;
            this.label8.Location = new System.Drawing.Point(697, 58);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(95, 18);
            this.label8.TabIndex = 14;
            this.label8.Text = "Select Date";
            // 
            // dateTimePic_calc
            // 
            this.dateTimePic_calc.CalendarForeColor = System.Drawing.Color.Silver;
            this.dateTimePic_calc.CalendarMonthBackground = System.Drawing.Color.Silver;
            this.dateTimePic_calc.CalendarTitleBackColor = System.Drawing.Color.Silver;
            this.dateTimePic_calc.CalendarTitleForeColor = System.Drawing.Color.Silver;
            this.dateTimePic_calc.CalendarTrailingForeColor = System.Drawing.Color.Silver;
            this.dateTimePic_calc.Location = new System.Drawing.Point(832, 55);
            this.dateTimePic_calc.Margin = new System.Windows.Forms.Padding(4);
            this.dateTimePic_calc.Name = "dateTimePic_calc";
            this.dateTimePic_calc.Size = new System.Drawing.Size(396, 22);
            this.dateTimePic_calc.TabIndex = 15;
            this.dateTimePic_calc.Visible = false;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // txt_user_poer_requirement
            // 
            this.txt_user_poer_requirement.BackColor = System.Drawing.Color.GhostWhite;
            this.txt_user_poer_requirement.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.txt_user_poer_requirement.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_user_poer_requirement.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.txt_user_poer_requirement.Location = new System.Drawing.Point(1050, 112);
            this.txt_user_poer_requirement.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.txt_user_poer_requirement.Name = "txt_user_poer_requirement";
            this.txt_user_poer_requirement.Size = new System.Drawing.Size(235, 27);
            this.txt_user_poer_requirement.TabIndex = 17;
            this.txt_user_poer_requirement.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txt_user_poer_requirement.Click += new System.EventHandler(this.txt_user_poer_requirement_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.DodgerBlue;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.button2.Location = new System.Drawing.Point(1580, 804);
            this.button2.Margin = new System.Windows.Forms.Padding(4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(122, 43);
            this.button2.TabIndex = 18;
            this.button2.Text = "Print";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // printDocument1
            // 
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument1_PrintPage);
            // 
            // printPreviewDialog1
            // 
            this.printPreviewDialog1.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.ClientSize = new System.Drawing.Size(400, 300);
            this.printPreviewDialog1.Enabled = true;
            this.printPreviewDialog1.Icon = ((System.Drawing.Icon)(resources.GetObject("printPreviewDialog1.Icon")));
            this.printPreviewDialog1.Name = "printPreviewDialog1";
            this.printPreviewDialog1.Visible = false;
            this.printPreviewDialog1.Load += new System.EventHandler(this.printPreviewDialog1_Load);
            // 
            // FRM_SOLAR_POWER_SIMULATE
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.Silver;
            this.ClientSize = new System.Drawing.Size(1787, 861);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.txt_user_poer_requirement);
            this.Controls.Add(this.dateTimePic_calc);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.linklbl_home);
            this.Controls.Add(this.lbl_loginname);
            this.Controls.Add(this.lbl_time);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lbl_USERPOWERREQUIREMENT);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FRM_SOLAR_POWER_SIMULATE";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Solar Power";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FRM_SOLAR_POWER_SIMULATE_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FRM_SOLAR_POWER_SIMULATE_FormClosed);
            this.Load += new System.EventHandler(this.FRM_SOLAR_POWER_SIMULATE_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txt_current;
        private System.Windows.Forms.TextBox txt_voltage;
        private System.Windows.Forms.Label lbl_current;
        private System.Windows.Forms.Label lbl_voltage;
        private System.Windows.Forms.Label lbl_USERPOWERREQUIREMENT;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label lbl_time;
        private System.Windows.Forms.Label lbl_loginname;
        private System.Windows.Forms.Label lbl_result_totalCost;
        private System.Windows.Forms.Label lbl_result_no_modules_parallel;
        private System.Windows.Forms.Label lbl_result_no_modules_serial;
        private System.Windows.Forms.Label lbl_result_angle;
        private System.Windows.Forms.Label lbl_result_no_modules;
        private System.Windows.Forms.Label lbl_totalcost;
        private System.Windows.Forms.Label lbl_No_Module_serial;
        private System.Windows.Forms.Label lbl_no_modules_parel;
        private System.Windows.Forms.Label lbl_angle;
        private System.Windows.Forms.Label lbl_no_modules;
        private System.Windows.Forms.Button BTN_Calculate;
        private System.Windows.Forms.Button BTN_mannualcorrection;
        private System.Windows.Forms.Button BTN_LOCATIONFETCHER;
        private System.Windows.Forms.Button BTN_Climatefetcher;
        private System.Windows.Forms.Button BTN_BASECELL_CONFIG;
        private System.Windows.Forms.LinkLabel linklbl_home;
        private System.Windows.Forms.Timer timer_timnpics;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutProjectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem benifitsToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lbl_coast;
        private System.Windows.Forms.Label lbl_modulearea;
        private System.Windows.Forms.Label lbl_panelmaterial;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button BTN_CLEAR;
        private System.Windows.Forms.Label lbl_humidi;
        private System.Windows.Forms.Label lbl_pressure;
        private System.Windows.Forms.Label lbl_resultlength;
        private System.Windows.Forms.Label lbl_lenght_day;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DateTimePicker dateTimePic_calc;
        private System.Windows.Forms.Label label9;
        public System.Windows.Forms.Label lbl_result_longi;
        public System.Windows.Forms.Label lbl_result_latitude;
        public System.Windows.Forms.Label lbl_result_location;
        public System.Windows.Forms.Label lbl_result_humid;
        public System.Windows.Forms.Label lbl_result_pressure;
        public System.Windows.Forms.Label lbl_result_climate;
        public System.Windows.Forms.Label lbl_result_voltage;
        public System.Windows.Forms.Label lbl_result_cost_module;
        public System.Windows.Forms.Label lbl_result_modultarea;
        public System.Windows.Forms.Label lbl_result_typematerial;
        public System.Windows.Forms.Label lbl_result_modulerating;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        public System.Windows.Forms.Label lbl_result_errorlevel;
        public System.Windows.Forms.Label lbl_result_climatecondition;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button button1;
        public System.Windows.Forms.Label txt_user_poer_requirement;
        private System.Windows.Forms.Button button2;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.PrintPreviewDialog printPreviewDialog1;
    }
}