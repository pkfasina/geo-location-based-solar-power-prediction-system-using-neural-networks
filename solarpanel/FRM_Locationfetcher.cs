﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml.Linq;
using System.Xml;
using System.Net;
using System.Diagnostics;
using System.Data.OleDb;
namespace solarpanel
{
    public partial class FRM_Locationfetcher : Form
    {
        public FRM_Locationfetcher()
        {
            InitializeComponent();
        }

        String place;



        public static string humid_selected = "00.00", presure_selected = "00.00", temp_selected = "00.00";
        public static string humid_temp = "00.00", presure_temp = "00.00", temptr_temp = "00.00";
        public static string selected_climate_info = "00.00";

        public static string selected_Palce = "--Select--", selected_latitud = "00.00", selected_longi = "00.00";
        sqlconnectionClass1 sqlconnection_obj = new sqlconnectionClass1();



       

        private void FRM_Locationfetcher_Load(object sender, EventArgs e)
        {
            try
            {
                //BTN_Save.Visible = false;
                //BTN_CLEAR.Location = new Point(155, 19);
                //BTN_Cancel.Location = new Point(385, 19);

                //if (Convert.ToInt32(FRM_Login.authentication) == 2)
                //{

                //    BTN_Save.Visible = false;
                //    BTN_CLEAR.Location = new Point(155, 19);
                //    BTN_Cancel.Location = new Point(385, 19);

                   
                //}
                timer_timnpics.Enabled = true;
                //lbl_username.Text = "User Name :" + FRM_Login.username;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void timer_timnpics_Tick(object sender, EventArgs e)
        {
            try
            {
                DateTime time = System.DateTime.Now;
                //lbl_time.Text = Convert.ToString(time.ToLongTimeString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void BTN_CLEAR_Click(object sender, EventArgs e)
        {
            try
            {
                txt_latitude.Text = "";
                txt_location.Text = "";
                txt_longitude.Text = "";
                txt_rich_locfetcher.Text = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void FRM_Locationfetcher_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                //FRM_SOLAR_POWER_SIMULATE frm_simulate_obj = new FRM_SOLAR_POWER_SIMULATE();
                //frm_simulate_obj.Show();
                
                
                
                
                //this.Hide();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void txt_rich_locfetcher_TextChanged(object sender, EventArgs e)
        {

        }

        private void txt_latitude_TextChanged(object sender, EventArgs e)
        {

        }

        private void txt_longitude_TextChanged(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void BTN_Cancel_Click(object sender, EventArgs e)
        {
            try
            {
                //FRM_SOLAR_POWER_SIMULATE frm_simulate_obj = new FRM_SOLAR_POWER_SIMULATE();
                //frm_simulate_obj.Show();
                this.Hide();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        string location;
        private void BTN_Save_Click(object sender, EventArgs e)
        {
            try
            {
                OleDbConnection sc = sqlconnection_obj.Database_Connection_Method();
                sc.Open();
                string q = "select LOCATION from location_cache where LOCATION = '" + txt_location.Text + "' ";
                OleDbCommand c = new OleDbCommand(q, sc);
                OleDbDataReader dr;
                dr = c.ExecuteReader();
                while (dr.Read())
                {

                    location =dr[0].ToString();
                    
                }

                
                //sc.Close();
                if (txt_location.Text != "" && txt_location.Text ==location )
                {
                    MessageBox.Show("Data already exist in database");
                }
                else if (txt_location.Text != "")
                {
                    decimal latitude, longitude;
                    latitude = Convert.ToDecimal(txt_latitude.Text);
                    longitude = Convert.ToDecimal(txt_longitude.Text);
                    OleDbConnection sql_con = sqlconnection_obj.Database_Connection_Method();
                    sql_con.Open();
                    string query_string = "INSERT INTO [location_cache]([LATITUDE],[LONGITIDE],[LOCATION])VALUES(" + latitude + "," + longitude + ",'" + txt_location.Text + "')";
                    OleDbCommand cmd = new OleDbCommand(query_string, sql_con);
                    cmd.ExecuteNonQuery();
                    sql_con.Close();
                    txt_latitude.Text = "";
                    txt_location.Text = "";
                    txt_longitude.Text = "";
                    txt_rich_locfetcher.Text = "";
                    MessageBox.Show("Data Successfully Saved ");
                }
                else { MessageBox.Show("Enter proper data"); }

                //if(txt_location.Text!="")
                //{
                //    decimal latitude, longitude;
                //    latitude = Convert.ToDecimal(txt_latitude.Text);
                //    longitude = Convert.ToDecimal(txt_longitude.Text);
                //    OleDbConnection sql_con = sqlconnection_obj.Database_Connection_Method();
                //    sql_con.Open();
                //    string query_string = "INSERT INTO [location_cache]([LATITUDE],[LONGITIDE],[LOCATION])VALUES(" + latitude + "," + longitude + ",'" + txt_location.Text + "')";
                //    OleDbCommand cmd = new OleDbCommand(query_string, sql_con);
                //    cmd.ExecuteNonQuery();
                //    sql_con.Close();
                //    txt_latitude.Text = "";
                //    txt_location.Text = "";
                //    txt_longitude.Text = "";
                //    txt_rich_locfetcher.Text = "";
                //    MessageBox.Show("Data Successfully Saved ");
                //}
                //else { MessageBox.Show("Enter proper data"); }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void lbl_location_Click(object sender, EventArgs e)
        {

        }


        private void BTN_LOC_SEARCH_Click(object sender, EventArgs e)
        {


            

            try
            {
                if (txt_location.Text != "")
                {

                    //System.Net.WebClient web_client_obj = new System.Net.WebClient();


                    //place = "https://api.opencagedata.com/geocode/v1/json?q=placename&key=" + txt_location.Text;
                    //place = "https://api.opencagedata.com/geocode/v1/json?key=0bdc7fa9d2e64ed5a2fb59ccfdc86bde" + txt_location.Text;
                    //place = "http://api.openweathermap.org/data/2.5/weather?APPID=0a76237620c27ec0fc40d1a6905b09bb" ;

                    //place = "http://api.openweathermap.org/data/2.5/weather?" +  "@QUERY@=@LOC@&mode=xml&units=imperial&APPID=0a76237620c27ec0fc40d1a6905b09bb" + txt_location.Text;


                    place = "http://api.openweathermap.org/data/2.5/weather?q=" + txt_location.Text + "&mode=xml&units=imperial&APPID=0a76237620c27ec0fc40d1a6905b09bb";



                    //byte[] raw = web_client_obj.DownloadData(place);

                    //string webdata = System.Text.Encoding.UTF8.GetString(raw);


                    //txt_rich_locfetcher.Text = webdata;
                    XmlDocument xDoc = new XmlDocument();
                    xDoc.Load(place);




                    //save
                    //xDoc.Save(@"C:\Users\admin\Desktop\test.xml");



                    XmlNode loc_node = xDoc.SelectSingleNode("current/city");
                    //string split_place = loc_node.Attributes["name"].Value;
                    string split_contry = loc_node.SelectSingleNode("country").InnerText;




                    XmlNode geo_node = loc_node.SelectSingleNode("coord");
                    txt_latitude.Text = geo_node.Attributes["lat"].Value;
                    txt_longitude.Text = geo_node.Attributes["lon"].Value;









                    txt_rich_locfetcher.Text = "Selected Place \t:" + txt_location.Text +  "\n\nSelected Country\t:" + split_contry ;

                    FRM_CLIMATEFETCHER.selected_place = txt_location.Text;
                    FRM_CLIMATEFETCHER.selected_lonid = txt_longitude.Text;
                    FRM_CLIMATEFETCHER.selected_latid = txt_latitude.Text;

                }



                else { MessageBox.Show("write proper place"); }
            }


            catch (WebException ex)
            {
                DisplayError(ex);
            }

            catch (Exception ex)
            {
                MessageBox.Show("Unknown error\n" + ex.Message);
            }


        }





        private void DisplayError(WebException exception)
        {
            try
            {
                StreamReader reader = new StreamReader(exception.Response.GetResponseStream());
                XmlDocument response_doc = new XmlDocument();
                response_doc.LoadXml(reader.ReadToEnd());
                XmlNode message_node = response_doc.SelectSingleNode("//message");
                MessageBox.Show(message_node.InnerText);
            }
            catch (Exception ex)
            {
                MessageBox.Show("unkonown error\n" + ex.Message);
            }

        }


        

        private void txt_location_TextChanged(object sender, EventArgs e)
        {
                
        }

        
      
    }
}
