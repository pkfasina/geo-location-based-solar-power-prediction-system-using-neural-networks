﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.Xml.Linq;
using System.Xml;
using System.Net;
using System.Web;
using System.Web.Util;
using System.IO;
using System.Data.OleDb;

namespace solarpanel
{
    public partial class FRM_MainHome : Form
    {
        public GroupBox g1; public GroupBox g2; public MonthCalendar m1;
        public FRM_MainHome()
        {
            InitializeComponent();

            g1 = groupBox1;
            g2 = groupBox2;
            m1 = monthCalendar1;
        }
        sqlconnectionClass1 sqlconnection_obj = new sqlconnectionClass1();
        public static string username, authentication;
        ////**********not moving form*************//
        protected override void WndProc(ref Message message)
        {
            const int WM_SYSCOMMAND = 0x0112;
            const int SC_MOVE = 0xF010;

            switch (message.Msg)
            {
                case WM_SYSCOMMAND:
                    int command = message.WParam.ToInt32() & 0xfff0;
                    if (command == SC_MOVE)
                        return;
                    break;
            }

            base.WndProc(ref message);
        }
        ////**********************************//

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void FRM_MainHome_Load(object sender, EventArgs e)
        {
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
            this.Width = Screen.PrimaryScreen.WorkingArea.Width;


            //FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            //Left = Top = 0;
            //Width = Screen.PrimaryScreen.WorkingArea.Width;
            //Height = Screen.PrimaryScreen.WorkingArea.Height;

            try
            {
                //groupBox2.Size = new Size(this.Size.Width - 490, this.Size.Height - 80);
                //pictureB_change.Size = new Size(groupBox2.Size.Width - 50, groupBox2.Size.Width - 450);
                //if (Convert.ToInt32(FRM_Login.authentication) == 2)
                //{
                //    picReg.Visible = false; picView.Visible = false;
                //    panel1.Hide(); panel5.Hide();
                //    //picView.Location = new Point(15, 51);
                //    //picMainproject.Location = new Point(217, 51);
                //    //picWeather.Location = new Point(121, 100);


                //}
                timer_timnpics.Enabled = true;
                //lbl_loginName.Text = "User Name :" + FRM_Login.username;
                string var = System.Windows.Forms.Application.StartupPath;
                //string text = System.IO.File.ReadAllText(var + "\\connection_text.txt");
                pictureB_change.Image = Image.FromFile(var + "\\images\\1.jpg");
                //pictureB_change.Image = Image.FromFile(@"D:\Anaswara\SolarPower\Common_For_All_Project\Common_For_All_Project\images\1.jpg");
            }
            catch (Exception ex)
            {
                //  MessageBox.Show(ex.ToString());
            }
        }

       
        int picture_no = 1;
        private void timer_timnpics_Tick(object sender, EventArgs e)
        {
            try
            {
                DateTime time = System.DateTime.Now;
                lbl_time.Text = "Time :" + Convert.ToString(time.ToLongTimeString());

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }


        private void timer1_picturebox_Tick_1(object sender, EventArgs e)
        {
            try
            {
                if (picture_no <= 6)
                {
                    string var = System.Windows.Forms.Application.StartupPath;
                    //string text = System.IO.File.ReadAllText(var + "\\connection_text.txt");
                    pictureB_change.Image = Image.FromFile(var + "\\images\\" + picture_no + ".jpg");
                    picture_no++;
                    if (picture_no == 7)
                    { picture_no = 1; }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void BTN_REG_NAVIGATION_Click(object sender, EventArgs e)
        {
            try
            {
                FRM_Registration reg_obj = new FRM_Registration();
                reg_obj.Show(); reg_obj.MdiParent = this; hide();
                //  this.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        

        private void BTN_CANCEL_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("Exit or No?", "Solar", MessageBoxButtons.YesNo,
                                      MessageBoxIcon.Information) == DialogResult.No)
                {

                }
                else
                {
                    this.Dispose();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void FRM_MainHome_FormClosed(object sender, FormClosedEventArgs e)
        {
            
            
        }

        private void FRM_MainHome_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                if (MessageBox.Show("Exit or No?", "Solar", MessageBoxButtons.YesNo,
                                      MessageBoxIcon.Information) == DialogResult.No)
                {
                    e.Cancel = true;
                    Application.Exit();
            
                }
                else
                {
                    this.Dispose();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            


           
            
        }



        public void hide()
        {
            groupBox2.Hide(); monthCalendar1.Hide();//  groupBox1.Hide();
        }
        public void shw()
        {
            groupBox2.Show(); 
        }
        

        
        private bool CheckOpened(string name)
        {
            FormCollection fc = Application.OpenForms;
            foreach (Form frm in fc)
            {
                if (frm.Text == name)
                {
                    return true;
                }
            }
            return false;
        }
        





        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
        {

        }

       

       
        
        

        

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }




        

        private void pictureB_change_Click(object sender, EventArgs e)
        {

        }

        private void lbl_username_Click(object sender, EventArgs e)
        {

        }


        

        private void Btn_Login_Click_1(object sender, EventArgs e)
        {
            //groupBox1.Visible = false;
            //FRM_Home f = new FRM_Home();
            try
            {
                int flag = 0;
                OleDbConnection con = sqlconnection_obj.Database_Connection_Method();
                con.Open();
                OleDbCommand sqlcmd_select = new OleDbCommand("select * from Login", con);
                OleDbDataReader sql_reader;
                sql_reader = sqlcmd_select.ExecuteReader();
                while (sql_reader.Read())
                {
                    if (txt_username.Text == sql_reader[0].ToString() && txt_password.Text == sql_reader[1].ToString())
                    {
                        flag++;
                        username = sql_reader[0].ToString();
                        //authentication = sql_reader[2].ToString();
                    }
                }
                if (flag >= 1)
                {
                    //      Check whether enabled or disabled User

                    OleDbConnection cn = sqlconnection_obj.Database_Connection_Method();
                    cn.Open();
                    OleDbCommand sqlcmd = new OleDbCommand("select SOFTWARESTATUS from User_Registration where U_NAME='" + txt_username.Text + "' AND PASSWORD='" + txt_password.Text + "'", cn);
                    OleDbDataAdapter da = new OleDbDataAdapter(sqlcmd);
                    DataSet dr = new DataSet();
                    da.Fill(dr);
                    cn.Close();
                    if (dr.Tables[0].Rows.Count > 0)
                    {
                        if (dr.Tables[0].Rows[0][0].ToString() == "E")
                        {








                            FRM_Home frm_reg_obj = new FRM_Home();
                            frm_reg_obj.Show();
                            //frm_reg_obj.MdiParent = this;
                            this.Hide();
                        }
                        else
                        {
                            errorProvider1.SetError(label5, "D");
                            label5.Text = "Disabled User: Can't Login";
                            txt_password.Text = ""; // txt_username.Text = "";
                        }
                    }

                }
                else
                {
                    errorProvider1.SetError(label5, "D");
                    label5.Text = "Incorrect UserName or Password";
                    txt_password.Text = ""; //txt_username.Text = "";


                }


                



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }



        private void BTN_Cancel_Click_2(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("Exit or No?", "Solar", MessageBoxButtons.YesNo,
                                      MessageBoxIcon.Information) == DialogResult.No)
                {
                    Application.Exit();
            
                }
                else
                {
                    this.Dispose();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            
               

        }


        
        private void label4_Click(object sender, EventArgs e)
        {
            
            try
            {
                frm_PWD fp_obj = new frm_PWD();
                FormCollection fc = Application.OpenForms;
                if (CheckOpened("Forgot Password"))
                {
                    fp_obj.Activate(); g2.Visible = false; m1.Visible = false; g1.Visible = false;
                }
                else
                {

                    fp_obj.Show(); fp_obj.MdiParent = this;
                    g2.Visible = false; m1.Visible = false; g1.Visible = false;
                    
                }
                foreach (Form frm in fc)
                {
                    if ((frm.Text != "Registration") && (frm.Text != "Home") && (frm.Text != "Login") && (frm.Text != "Forgot Password"))
                    {
                        //frm.Close();
                        frm.Hide();
                        
                    }
                }


            }
            catch (Exception)
            {
                // MessageBox.Show(ex.ToString());
            }


        }



        private void label3_Click(object sender, EventArgs e)
        {
            try
            {
                FRM_Registration reg_obj = new FRM_Registration();
                FormCollection fc = Application.OpenForms;
                if (CheckOpened("Registration"))
                {
                    reg_obj.Activate(); g2.Visible = false; m1.Visible = false; 
                    g1.Visible = false;

                }
                else
                {

                    reg_obj.Show(); reg_obj.MdiParent = this;
                    g2.Visible = false; m1.Visible = false;
                    g1.Visible = false;
                }
                foreach (Form frm in fc)
                {
                    if ((frm.Text != "Registration") && (frm.Text != "Home") && (frm.Text != "Login"))
                    {
                        //frm.Close();
                        frm.Hide();
                    }
                }


            }
            catch (Exception)
            {
                // MessageBox.Show(ex.ToString());
            }
        }

        private void txt_password_TextChanged(object sender, EventArgs e)
        {

        }

        private void txt_username_TextChanged(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter_1(object sender, EventArgs e)
        {

        }

        private void groupBox2_Enter_1(object sender, EventArgs e)
        {

        }

        private void lbl_time_Click(object sender, EventArgs e)
        {

        }

        private void panel9_Paint(object sender, PaintEventArgs e)
        {

        }

        private void picExit_Click_1(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("Exit or No?", "Solar", MessageBoxButtons.YesNo,
                                      MessageBoxIcon.Information) == DialogResult.No)
                {

                }
                else
                {
                    this.Dispose();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void groupBox1_Enter_2(object sender, EventArgs e)
        {

        }

        private void pictureB_change_Click_1(object sender, EventArgs e)
        {

        }

        private void lbl_time_Click_1(object sender, EventArgs e)
        {

        }

        private void timer_timnpics_Tick_1(object sender, EventArgs e)
        {
            try
            {
                DateTime time = System.DateTime.Now;
                label6.Text = "Time :" + Convert.ToString(time.ToLongTimeString());

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        

        private void pictureBox2_Click_1(object sender, EventArgs e)
        {
            try
            {
                FRM_ABOUT_PROJECT view_obj = new FRM_ABOUT_PROJECT();
                FormCollection fc = Application.OpenForms;
                if (CheckOpened("About Project"))
                {
                    view_obj.Activate(); g2.Visible = false; m1.Visible = false; g1.Visible = false;
                }
                else
                {
                    view_obj.Show(); view_obj.MdiParent = this; g2.Visible = false; m1.Visible = false; g1.Visible = false;
                }
                foreach (Form frm in fc)
                {
                    if ((frm.Text != "About Project") && (frm.Text != "Home") && (frm.Text != "Login"))
                    {
                        //frm.Close();
                        frm.Hide();
                    }
                }
            }
            catch (Exception)
            {
                //MessageBox.Show(ex.ToString());
            }


           
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void timer_time_Tick(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void monthCalendar1_DateChanged_1(object sender, DateRangeEventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void FRM_MainHome_Resize(object sender, EventArgs e)
        {
            if ((this.WindowState == FormWindowState.Minimized) || (this.WindowState == FormWindowState.Normal)
                || (this.WindowState == FormWindowState.Maximized))
            {
                //this.ShowInTaskbar = true; 
                this.Activate();
            }
        }

        private void txt_username_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void txt_username_TextChanged_2(object sender, EventArgs e)
        {

        }

        private void txt_password_TextChanged_1(object sender, EventArgs e)
        {

        }

       
        private void groupBox2_Enter_2(object sender, EventArgs e)
        {

        }

        private void txt_username_TextChanged_3(object sender, EventArgs e)
        {

        }

        private void txt_password_TextChanged_2(object sender, EventArgs e)
        {

        }

        
       
        

        

        
        
        

        

        

        

        







    }
}
