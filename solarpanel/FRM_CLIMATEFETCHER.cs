﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using System.Xml;
using System.Windows.Forms;
using System.Reflection;
using System.Data.OleDb;

namespace solarpanel
{
    public partial class FRM_CLIMATEFETCHER : Form
    {
        public FRM_CLIMATEFETCHER()
        {
            InitializeComponent();
        }
        sqlconnectionClass1 sqlconnection_obj = new sqlconnectionClass1();
        string place;//, latid, lonid;
        public static string humid_selected = "00.00", wind_selected = "00.00", presure_selected = "00.00", mintemp_selected = "00.00" , maxtemp_selected = "00.00" , temptr_temp="00.00";
        public static string selected_climate_info = "00.00", temptr_temp1="00.00",selected_place = "--Select--", selected_latid = "00.00", selected_lonid = "00.00";

        private void txt_longitude_TextChanged(object sender, EventArgs e)
        {

        }

        private void BTN_LOC_fetch_Click(object sender, EventArgs e)
        {
            try
            {

                if (combo_location.Text != "" && combo_location.Text != "         --- Select Place ---")
                {



                       //System.Net.WebClient web_client_obj = new System.Net.WebClient();
                       //place = place + latid + "=" + "" + txt_latitude.Text + "" + lonid + "=" + "" + txt_longitude.Text + "";
                       //place = "http://api.openweathermap.org/data/2.5/weather?q=London,uk&APPID=0a76237620c27ec0fc40d1a6905b09bb" + combo_location.Text;
                       //place = "http://api.openweathermap.org/data/2.5/weather?APPID=0a76237620c27ec0fc40d1a6905b09bb" + combo_location.Text;
                    
                    //place = "https://api.openweathermap.org/data/2.5/weather?APPID=0a76237620c27ec0fc40d1a6905b09bb" + combo_location.Text;
                    //place = "https://api.openweathermap.org/data/2.5/weather?q=" +combo_location.Text+ "&APPID=0a76237620c27ec0fc40d1a6905b09bb";

                    //place = "https://api.openweathermap.org/data/2.5/weather?lat=" + txt_latitude.Text + "&lon=" + txt_longitude.Text + "&appid=0a76237620c27ec0fc40d1a6905b09bb";
                    place = "http://api.openweathermap.org/data/2.5/weather?q=" + combo_location.Text + "&mode=xml&units=imperial&APPID=0a76237620c27ec0fc40d1a6905b09bb";






                    XmlDocument xDoc = new XmlDocument();
                    xDoc.Load(place);

                    //xDoc.Save(@"C:\Users\admin\Desktop\test11.xml");



                    XmlNode loc_node = xDoc.SelectSingleNode("current/temperature");
                    string temp_selected = loc_node.Attributes["value"].Value;
                    string mintemp_selected = loc_node.Attributes["min"].Value;
                    string maxtemp_selected = loc_node.Attributes["max"].Value;

                    XmlNode loc1_node = xDoc.SelectSingleNode("current/humidity");
                    string humid_selected = loc1_node.Attributes["value"].Value;


                    XmlNode loc2_node = xDoc.SelectSingleNode("current/pressure");
                    string presure_selected = loc2_node.Attributes["value"].Value;


                    XmlNode loc3_node = xDoc.SelectSingleNode("current/wind/speed");
                    string wind_selected = loc3_node.Attributes["value"].Value;

                    XmlNode loc4_node = xDoc.SelectSingleNode("current/city/sun");
                    string sunrise = loc4_node.Attributes["rise"].Value;
                    string sunset = loc4_node.Attributes["set"].Value;





                    //selected_climate_info = Convert.ToString(Math.Floor(float.Parse(selected_climate_info) - 273));
                    //selected_latid = txt_latitude.Text;
                    //selected_lonid = txt_longitude.Text;
                    //selected_place = combo_location.SelectedItem.ToString();











                    //txt_rich_climate_info.Text = "Temperature  \t : " + temp_selected + " fahrenheit"
                    //                                             + "\n\nPressure \t : " + presure_selected + " hpa"
                    //                                             + "\n\nHumidity \t : " + humid_selected + " %"
                    //                                             + "\n\nWind - speed \t : " + wind_selected + " mph";




                    FRM_SOLAR_POWER_SIMULATE frm_simulate_obj = (FRM_SOLAR_POWER_SIMULATE)Application.OpenForms["FRM_SOLAR_POWER_SIMULATE"];

                    frm_simulate_obj.lbl_result_longi.Text = txt_longitude.Text;
                    frm_simulate_obj.lbl_result_location.Text = combo_location.SelectedItem.ToString();
                    frm_simulate_obj.lbl_result_latitude.Text = txt_latitude.Text;
                    frm_simulate_obj.lbl_result_climate.Text = Convert.ToString(temp_selected);
                    frm_simulate_obj.lbl_result_humid.Text = humid_selected;
                    frm_simulate_obj.lbl_result_pressure.Text = presure_selected;
                    this.Hide();
                    frm_simulate_obj.Refresh();



                    errorProvider1.Dispose();




                }
                else { MessageBox.Show("Select place"); }
            }


            catch (WebException ex)
            {
                DisplayError(ex);
            }

            //catch (Exception ex)
            //{
            //    MessageBox.Show("Unknown error\n" + ex.Message);
            //}



        }


        private void DisplayError(WebException exception)
        {
            try
            {
                StreamReader reader = new StreamReader(exception.Response.GetResponseStream());
                XmlDocument response_doc = new XmlDocument();
                response_doc.LoadXml(reader.ReadToEnd());
                XmlNode message_node = response_doc.SelectSingleNode("//message");
                MessageBox.Show(message_node.InnerText);
            }
            catch (Exception ex)
            {
                MessageBox.Show("unkonown error\n" + ex.Message);
            }

        }



        //private void BTN_Save_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (txt_latitude.Text != "" && txt_longitude.Text != "" && txt_rich_climate_info.Text != "")
        //        {
        //            decimal latitude, longitude;
        //            latitude = Convert.ToDecimal(txt_latitude.Text);
        //            longitude = Convert.ToDecimal(txt_longitude.Text);
        //            OleDbConnection con = sqlconnection_obj.Database_Connection_Method();
        //            con.Open();
        //            string query_string = "INSERT INTO [climate_cache]([LATITUDE],[LONGITUDE],[CLIMATE_LOCATION],[CLIMATE_INFO],[MAX_TEMP],[MIN_TEMP],[HUMIDITY],[PRESSURE])VALUES(" + latitude + "," + longitude + ",'" + combo_location.SelectedItem + "','" + txt_rich_climate_info.Text + "','" + maxtemp_selected + "' ,'" + mintemp_selected + "' , '" + humid_selected + "' ,'" + presure_selected + "')";
        //            OleDbCommand cmd = new OleDbCommand(query_string, con);
        //            cmd.ExecuteNonQuery();
        //            con.Close();
        //            txt_latitude.Text = "";
        //            combo_location.Text = "         --- Select Place ---";
        //            txt_longitude.Text = "";
        //            txt_rich_climate_info.Text = "";
        //            MessageBox.Show("Successfully Saved.");
        //        }
        //        else { MessageBox.Show("Select one place from combobox or Click fetch Values"); }
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.ToString());
        //    }
        //}

        

        

        //private void BTN_CLEAR_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        txt_latitude.Text = "";
        //        combo_location.Text = "         --- Select Place ---";
        //        txt_longitude.Text = "";
        //        txt_rich_climate_info.Text = "";
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.ToString());
        //    }
        //}

       

        //private void BTN_Cancel_Click(object sender, EventArgs e)
        //{
        //    try
        //    {

        //        //FRM_SOLAR_POWER_SIMULATE frm_simulate_obj = new FRM_SOLAR_POWER_SIMULATE();
        //        //frm_simulate_obj.Show();
        //        this.Hide();
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.ToString());
        //    }
        //}

        private void FRM_CLIMATEFETCHER_Load(object sender, EventArgs e)
        {
            try
            {
                

               // timer_timnpics.Enabled = true;
                //lbl_username.Text = "User Name :" + FRM_Login.username;
                OleDbConnection con = sqlconnection_obj.Database_Connection_Method();
                con.Open();
                OleDbCommand sqlcom = new OleDbCommand("select LOCATION from [location_cache] order by LOCATION", con);
                OleDbDataReader sql_reader;
                sql_reader = sqlcom.ExecuteReader();
                while (sql_reader.Read())
                {
                    combo_location.Items.Add(sql_reader[0].ToString());
                }
                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void timer_timnpics_Tick(object sender, EventArgs e)
        {
            try
            {
                DateTime time = System.DateTime.Now;
                //lbl_time.Text = Convert.ToString(time.ToLongTimeString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void FRM_CLIMATEFETCHER_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                //FRM_SOLAR_POWER_SIMULATE frm_simulate_obj = new FRM_SOLAR_POWER_SIMULATE();
                //frm_simulate_obj.Show();
                //this.Hide();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void combo_location_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (combo_location.Text != "         --- Select Place ---")
                {
                    OleDbConnection con = sqlconnection_obj.Database_Connection_Method();
                    con.Open();
                    OleDbCommand sqlcom = new OleDbCommand("select LATITUDE,LONGITIDE from [location_cache] where LOCATION='" + combo_location.SelectedItem + "'", con);
                    OleDbDataReader sql_reader;
                    sql_reader = sqlcom.ExecuteReader();
                    while (sql_reader.Read())
                    {
                        txt_latitude.Text = sql_reader[0].ToString();
                        txt_longitude.Text = sql_reader[1].ToString();
                    }
                    con.Close();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        

        

        private void txt_latitude_TextChanged(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
            
    }
}
