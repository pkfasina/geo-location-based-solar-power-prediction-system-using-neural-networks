﻿namespace solarpanel
{
    partial class FRM_BASECELLCONFIG
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1_basecell = new System.Windows.Forms.MenuStrip();
            this.searchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lbl_modulerate = new System.Windows.Forms.Label();
            this.lbl_voltage = new System.Windows.Forms.Label();
            this.lbl_coast = new System.Windows.Forms.Label();
            this.lbl_modulearea = new System.Windows.Forms.Label();
            this.lbl_panelmaterial = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txt_modulerating = new System.Windows.Forms.TextBox();
            this.txt_voltage = new System.Windows.Forms.TextBox();
            this.txt_cost = new System.Windows.Forms.TextBox();
            this.txt_moduleare = new System.Windows.Forms.TextBox();
            this.txt_module_material = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.BTN_Cancel = new System.Windows.Forms.Button();
            this.BTN_CLEAR = new System.Windows.Forms.Button();
            this.BTN_Save = new System.Windows.Forms.Button();
            this.timer_timnpics = new System.Windows.Forms.Timer(this.components);
            this.menuStrip1_basecell.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1_basecell
            // 
            this.menuStrip1_basecell.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.menuStrip1_basecell.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1_basecell.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.searchToolStripMenuItem});
            this.menuStrip1_basecell.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1_basecell.Name = "menuStrip1_basecell";
            this.menuStrip1_basecell.Size = new System.Drawing.Size(777, 25);
            this.menuStrip1_basecell.TabIndex = 0;
            this.menuStrip1_basecell.Text = "menuStrip1";
            // 
            // searchToolStripMenuItem
            // 
            this.searchToolStripMenuItem.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchToolStripMenuItem.Name = "searchToolStripMenuItem";
            this.searchToolStripMenuItem.Size = new System.Drawing.Size(69, 21);
            this.searchToolStripMenuItem.Text = "Search";
            this.searchToolStripMenuItem.Click += new System.EventHandler(this.searchToolStripMenuItem_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lbl_modulerate);
            this.groupBox1.Controls.Add(this.lbl_voltage);
            this.groupBox1.Controls.Add(this.lbl_coast);
            this.groupBox1.Controls.Add(this.lbl_modulearea);
            this.groupBox1.Controls.Add(this.lbl_panelmaterial);
            this.groupBox1.Location = new System.Drawing.Point(29, 36);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(243, 256);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            // 
            // lbl_modulerate
            // 
            this.lbl_modulerate.AutoSize = true;
            this.lbl_modulerate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_modulerate.Location = new System.Drawing.Point(20, 198);
            this.lbl_modulerate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_modulerate.Name = "lbl_modulerate";
            this.lbl_modulerate.Size = new System.Drawing.Size(116, 18);
            this.lbl_modulerate.TabIndex = 7;
            this.lbl_modulerate.Text = "Module Rating";
            // 
            // lbl_voltage
            // 
            this.lbl_voltage.AutoSize = true;
            this.lbl_voltage.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_voltage.Location = new System.Drawing.Point(20, 153);
            this.lbl_voltage.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_voltage.Name = "lbl_voltage";
            this.lbl_voltage.Size = new System.Drawing.Size(108, 18);
            this.lbl_voltage.TabIndex = 6;
            this.lbl_voltage.Text = "Voltage / Cell";
            // 
            // lbl_coast
            // 
            this.lbl_coast.AutoSize = true;
            this.lbl_coast.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_coast.Location = new System.Drawing.Point(20, 113);
            this.lbl_coast.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_coast.Name = "lbl_coast";
            this.lbl_coast.Size = new System.Drawing.Size(114, 18);
            this.lbl_coast.TabIndex = 5;
            this.lbl_coast.Text = "Cost / Module";
            // 
            // lbl_modulearea
            // 
            this.lbl_modulearea.AutoSize = true;
            this.lbl_modulearea.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_modulearea.Location = new System.Drawing.Point(20, 74);
            this.lbl_modulearea.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_modulearea.Name = "lbl_modulearea";
            this.lbl_modulearea.Size = new System.Drawing.Size(186, 18);
            this.lbl_modulearea.TabIndex = 4;
            this.lbl_modulearea.Text = "Module Area(unit x unit)";
            // 
            // lbl_panelmaterial
            // 
            this.lbl_panelmaterial.AutoSize = true;
            this.lbl_panelmaterial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_panelmaterial.Location = new System.Drawing.Point(20, 31);
            this.lbl_panelmaterial.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_panelmaterial.Name = "lbl_panelmaterial";
            this.lbl_panelmaterial.Size = new System.Drawing.Size(179, 18);
            this.lbl_panelmaterial.TabIndex = 3;
            this.lbl_panelmaterial.Text = "Type Of Panel Material";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txt_modulerating);
            this.groupBox2.Controls.Add(this.txt_voltage);
            this.groupBox2.Controls.Add(this.txt_cost);
            this.groupBox2.Controls.Add(this.txt_moduleare);
            this.groupBox2.Controls.Add(this.txt_module_material);
            this.groupBox2.Location = new System.Drawing.Point(336, 36);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(303, 256);
            this.groupBox2.TabIndex = 9;
            this.groupBox2.TabStop = false;
            // 
            // txt_modulerating
            // 
            this.txt_modulerating.Location = new System.Drawing.Point(47, 194);
            this.txt_modulerating.Margin = new System.Windows.Forms.Padding(4);
            this.txt_modulerating.Name = "txt_modulerating";
            this.txt_modulerating.Size = new System.Drawing.Size(249, 22);
            this.txt_modulerating.TabIndex = 4;
            // 
            // txt_voltage
            // 
            this.txt_voltage.Location = new System.Drawing.Point(47, 149);
            this.txt_voltage.Margin = new System.Windows.Forms.Padding(4);
            this.txt_voltage.Name = "txt_voltage";
            this.txt_voltage.Size = new System.Drawing.Size(249, 22);
            this.txt_voltage.TabIndex = 3;
            // 
            // txt_cost
            // 
            this.txt_cost.Location = new System.Drawing.Point(47, 109);
            this.txt_cost.Margin = new System.Windows.Forms.Padding(4);
            this.txt_cost.Name = "txt_cost";
            this.txt_cost.Size = new System.Drawing.Size(249, 22);
            this.txt_cost.TabIndex = 2;
            // 
            // txt_moduleare
            // 
            this.txt_moduleare.Location = new System.Drawing.Point(46, 70);
            this.txt_moduleare.Margin = new System.Windows.Forms.Padding(4);
            this.txt_moduleare.Name = "txt_moduleare";
            this.txt_moduleare.Size = new System.Drawing.Size(249, 22);
            this.txt_moduleare.TabIndex = 1;
            // 
            // txt_module_material
            // 
            this.txt_module_material.Location = new System.Drawing.Point(46, 27);
            this.txt_module_material.Margin = new System.Windows.Forms.Padding(4);
            this.txt_module_material.Name = "txt_module_material";
            this.txt_module_material.Size = new System.Drawing.Size(249, 22);
            this.txt_module_material.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.BTN_Cancel);
            this.groupBox3.Controls.Add(this.BTN_CLEAR);
            this.groupBox3.Controls.Add(this.BTN_Save);
            this.groupBox3.Location = new System.Drawing.Point(78, 300);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox3.Size = new System.Drawing.Size(561, 71);
            this.groupBox3.TabIndex = 10;
            this.groupBox3.TabStop = false;
            // 
            // BTN_Cancel
            // 
            this.BTN_Cancel.BackColor = System.Drawing.Color.DodgerBlue;
            this.BTN_Cancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_Cancel.ForeColor = System.Drawing.Color.White;
            this.BTN_Cancel.Location = new System.Drawing.Point(230, 20);
            this.BTN_Cancel.Margin = new System.Windows.Forms.Padding(4);
            this.BTN_Cancel.Name = "BTN_Cancel";
            this.BTN_Cancel.Size = new System.Drawing.Size(145, 37);
            this.BTN_Cancel.TabIndex = 17;
            this.BTN_Cancel.Text = "Cancel";
            this.BTN_Cancel.UseVisualStyleBackColor = false;
            this.BTN_Cancel.Click += new System.EventHandler(this.BTN_Cancel_Click);
            // 
            // BTN_CLEAR
            // 
            this.BTN_CLEAR.BackColor = System.Drawing.Color.DodgerBlue;
            this.BTN_CLEAR.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_CLEAR.ForeColor = System.Drawing.Color.White;
            this.BTN_CLEAR.Location = new System.Drawing.Point(408, 20);
            this.BTN_CLEAR.Margin = new System.Windows.Forms.Padding(4);
            this.BTN_CLEAR.Name = "BTN_CLEAR";
            this.BTN_CLEAR.Size = new System.Drawing.Size(145, 37);
            this.BTN_CLEAR.TabIndex = 16;
            this.BTN_CLEAR.Text = "Clear";
            this.BTN_CLEAR.UseVisualStyleBackColor = false;
            this.BTN_CLEAR.Click += new System.EventHandler(this.BTN_CLEAR_Click);
            // 
            // BTN_Save
            // 
            this.BTN_Save.BackColor = System.Drawing.Color.DodgerBlue;
            this.BTN_Save.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_Save.ForeColor = System.Drawing.Color.White;
            this.BTN_Save.Location = new System.Drawing.Point(49, 20);
            this.BTN_Save.Margin = new System.Windows.Forms.Padding(4);
            this.BTN_Save.Name = "BTN_Save";
            this.BTN_Save.Size = new System.Drawing.Size(145, 37);
            this.BTN_Save.TabIndex = 15;
            this.BTN_Save.Text = "Save";
            this.BTN_Save.UseVisualStyleBackColor = false;
            this.BTN_Save.Click += new System.EventHandler(this.BTN_Save_Click);
            // 
            // timer_timnpics
            // 
            this.timer_timnpics.Tick += new System.EventHandler(this.timer_timnpics_Tick);
            // 
            // FRM_BASECELLCONFIG
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(777, 414);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1_basecell);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MainMenuStrip = this.menuStrip1_basecell;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FRM_BASECELLCONFIG";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Base Cell Configuration";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FRM_BASECELLCONFIG_FormClosed);
            this.Load += new System.EventHandler(this.FRM_BASECELLCONFIG_Load);
            this.menuStrip1_basecell.ResumeLayout(false);
            this.menuStrip1_basecell.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1_basecell;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lbl_voltage;
        private System.Windows.Forms.Label lbl_coast;
        private System.Windows.Forms.Label lbl_modulearea;
        private System.Windows.Forms.Label lbl_panelmaterial;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txt_voltage;
        private System.Windows.Forms.TextBox txt_cost;
        private System.Windows.Forms.TextBox txt_moduleare;
        private System.Windows.Forms.TextBox txt_module_material;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button BTN_Cancel;
        private System.Windows.Forms.Button BTN_CLEAR;
        private System.Windows.Forms.Button BTN_Save;
        private System.Windows.Forms.Timer timer_timnpics;
        private System.Windows.Forms.ToolStripMenuItem searchToolStripMenuItem;
        private System.Windows.Forms.Label lbl_modulerate;
        private System.Windows.Forms.TextBox txt_modulerating;
    }
}