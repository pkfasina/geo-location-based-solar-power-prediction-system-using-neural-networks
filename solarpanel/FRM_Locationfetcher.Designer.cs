﻿namespace solarpanel
{
    partial class FRM_Locationfetcher
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txt_longitude = new System.Windows.Forms.TextBox();
            this.txt_latitude = new System.Windows.Forms.TextBox();
            this.lbl_LONGITUDE = new System.Windows.Forms.Label();
            this.lbl_LATITUDE = new System.Windows.Forms.Label();
            this.BTN_LOC_SEARCH = new System.Windows.Forms.Button();
            this.txt_rich_locfetcher = new System.Windows.Forms.RichTextBox();
            this.txt_location = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lbl_location_fetched = new System.Windows.Forms.Label();
            this.lbl_location = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.BTN_Cancel = new System.Windows.Forms.Button();
            this.BTN_Save = new System.Windows.Forms.Button();
            this.BTN_CLEAR = new System.Windows.Forms.Button();
            this.timer_timnpics = new System.Windows.Forms.Timer(this.components);
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txt_longitude);
            this.groupBox1.Controls.Add(this.txt_latitude);
            this.groupBox1.Controls.Add(this.lbl_LONGITUDE);
            this.groupBox1.Controls.Add(this.lbl_LATITUDE);
            this.groupBox1.Controls.Add(this.BTN_LOC_SEARCH);
            this.groupBox1.Controls.Add(this.txt_rich_locfetcher);
            this.groupBox1.Controls.Add(this.txt_location);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Location = new System.Drawing.Point(19, 30);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(872, 262);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // txt_longitude
            // 
            this.txt_longitude.BackColor = System.Drawing.Color.White;
            this.txt_longitude.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_longitude.ForeColor = System.Drawing.SystemColors.Highlight;
            this.txt_longitude.Location = new System.Drawing.Point(688, 213);
            this.txt_longitude.Margin = new System.Windows.Forms.Padding(4);
            this.txt_longitude.Name = "txt_longitude";
            this.txt_longitude.ReadOnly = true;
            this.txt_longitude.Size = new System.Drawing.Size(168, 30);
            this.txt_longitude.TabIndex = 11;
            this.txt_longitude.TextChanged += new System.EventHandler(this.txt_longitude_TextChanged);
            // 
            // txt_latitude
            // 
            this.txt_latitude.BackColor = System.Drawing.Color.White;
            this.txt_latitude.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_latitude.ForeColor = System.Drawing.SystemColors.Highlight;
            this.txt_latitude.Location = new System.Drawing.Point(389, 213);
            this.txt_latitude.Margin = new System.Windows.Forms.Padding(4);
            this.txt_latitude.Name = "txt_latitude";
            this.txt_latitude.ReadOnly = true;
            this.txt_latitude.Size = new System.Drawing.Size(168, 30);
            this.txt_latitude.TabIndex = 10;
            this.txt_latitude.TextChanged += new System.EventHandler(this.txt_latitude_TextChanged);
            // 
            // lbl_LONGITUDE
            // 
            this.lbl_LONGITUDE.AutoSize = true;
            this.lbl_LONGITUDE.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_LONGITUDE.Location = new System.Drawing.Point(581, 219);
            this.lbl_LONGITUDE.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_LONGITUDE.Name = "lbl_LONGITUDE";
            this.lbl_LONGITUDE.Size = new System.Drawing.Size(81, 18);
            this.lbl_LONGITUDE.TabIndex = 9;
            this.lbl_LONGITUDE.Text = "Longitude";
            // 
            // lbl_LATITUDE
            // 
            this.lbl_LATITUDE.AutoSize = true;
            this.lbl_LATITUDE.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_LATITUDE.Location = new System.Drawing.Point(297, 219);
            this.lbl_LATITUDE.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_LATITUDE.Name = "lbl_LATITUDE";
            this.lbl_LATITUDE.Size = new System.Drawing.Size(67, 18);
            this.lbl_LATITUDE.TabIndex = 4;
            this.lbl_LATITUDE.Text = "Latitude";
            // 
            // BTN_LOC_SEARCH
            // 
            this.BTN_LOC_SEARCH.BackColor = System.Drawing.Color.DodgerBlue;
            this.BTN_LOC_SEARCH.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_LOC_SEARCH.ForeColor = System.Drawing.Color.White;
            this.BTN_LOC_SEARCH.Location = new System.Drawing.Point(712, 28);
            this.BTN_LOC_SEARCH.Margin = new System.Windows.Forms.Padding(4);
            this.BTN_LOC_SEARCH.Name = "BTN_LOC_SEARCH";
            this.BTN_LOC_SEARCH.Size = new System.Drawing.Size(145, 37);
            this.BTN_LOC_SEARCH.TabIndex = 8;
            this.BTN_LOC_SEARCH.Text = "Fetch Value";
            this.BTN_LOC_SEARCH.UseVisualStyleBackColor = false;
            this.BTN_LOC_SEARCH.Click += new System.EventHandler(this.BTN_LOC_SEARCH_Click);
            // 
            // txt_rich_locfetcher
            // 
            this.txt_rich_locfetcher.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_rich_locfetcher.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txt_rich_locfetcher.Location = new System.Drawing.Point(297, 73);
            this.txt_rich_locfetcher.Margin = new System.Windows.Forms.Padding(4);
            this.txt_rich_locfetcher.Name = "txt_rich_locfetcher";
            this.txt_rich_locfetcher.Size = new System.Drawing.Size(559, 117);
            this.txt_rich_locfetcher.TabIndex = 7;
            this.txt_rich_locfetcher.Text = "";
            this.txt_rich_locfetcher.TextChanged += new System.EventHandler(this.txt_rich_locfetcher_TextChanged);
            // 
            // txt_location
            // 
            this.txt_location.Location = new System.Drawing.Point(297, 32);
            this.txt_location.Margin = new System.Windows.Forms.Padding(4);
            this.txt_location.Name = "txt_location";
            this.txt_location.Size = new System.Drawing.Size(405, 22);
            this.txt_location.TabIndex = 6;
            this.txt_location.TextChanged += new System.EventHandler(this.txt_location_TextChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lbl_location_fetched);
            this.groupBox2.Controls.Add(this.lbl_location);
            this.groupBox2.Location = new System.Drawing.Point(11, 14);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(241, 185);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Enter += new System.EventHandler(this.groupBox2_Enter);
            // 
            // lbl_location_fetched
            // 
            this.lbl_location_fetched.AutoSize = true;
            this.lbl_location_fetched.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_location_fetched.Location = new System.Drawing.Point(46, 106);
            this.lbl_location_fetched.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_location_fetched.Name = "lbl_location_fetched";
            this.lbl_location_fetched.Size = new System.Drawing.Size(138, 18);
            this.lbl_location_fetched.TabIndex = 3;
            this.lbl_location_fetched.Text = "Location Fetched";
            // 
            // lbl_location
            // 
            this.lbl_location.AutoSize = true;
            this.lbl_location.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_location.Location = new System.Drawing.Point(73, 23);
            this.lbl_location.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_location.Name = "lbl_location";
            this.lbl_location.Size = new System.Drawing.Size(78, 18);
            this.lbl_location.TabIndex = 2;
            this.lbl_location.Text = "Location ";
            this.lbl_location.Click += new System.EventHandler(this.lbl_location_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.BTN_Cancel);
            this.groupBox3.Controls.Add(this.BTN_Save);
            this.groupBox3.Controls.Add(this.BTN_CLEAR);
            this.groupBox3.Location = new System.Drawing.Point(19, 295);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox3.Size = new System.Drawing.Size(872, 79);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            // 
            // BTN_Cancel
            // 
            this.BTN_Cancel.BackColor = System.Drawing.Color.DodgerBlue;
            this.BTN_Cancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_Cancel.ForeColor = System.Drawing.Color.White;
            this.BTN_Cancel.Location = new System.Drawing.Point(585, 23);
            this.BTN_Cancel.Margin = new System.Windows.Forms.Padding(4);
            this.BTN_Cancel.Name = "BTN_Cancel";
            this.BTN_Cancel.Size = new System.Drawing.Size(145, 37);
            this.BTN_Cancel.TabIndex = 11;
            this.BTN_Cancel.Text = "Cancel";
            this.BTN_Cancel.UseVisualStyleBackColor = false;
            this.BTN_Cancel.Click += new System.EventHandler(this.BTN_Cancel_Click);
            // 
            // BTN_Save
            // 
            this.BTN_Save.BackColor = System.Drawing.Color.DodgerBlue;
            this.BTN_Save.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_Save.ForeColor = System.Drawing.Color.White;
            this.BTN_Save.Location = new System.Drawing.Point(242, 23);
            this.BTN_Save.Margin = new System.Windows.Forms.Padding(4);
            this.BTN_Save.Name = "BTN_Save";
            this.BTN_Save.Size = new System.Drawing.Size(145, 37);
            this.BTN_Save.TabIndex = 9;
            this.BTN_Save.Text = "Save";
            this.BTN_Save.UseVisualStyleBackColor = false;
            this.BTN_Save.Click += new System.EventHandler(this.BTN_Save_Click);
            // 
            // BTN_CLEAR
            // 
            this.BTN_CLEAR.BackColor = System.Drawing.Color.DodgerBlue;
            this.BTN_CLEAR.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_CLEAR.ForeColor = System.Drawing.Color.White;
            this.BTN_CLEAR.Location = new System.Drawing.Point(412, 23);
            this.BTN_CLEAR.Margin = new System.Windows.Forms.Padding(4);
            this.BTN_CLEAR.Name = "BTN_CLEAR";
            this.BTN_CLEAR.Size = new System.Drawing.Size(145, 37);
            this.BTN_CLEAR.TabIndex = 10;
            this.BTN_CLEAR.Text = "Clear";
            this.BTN_CLEAR.UseVisualStyleBackColor = false;
            this.BTN_CLEAR.Click += new System.EventHandler(this.BTN_CLEAR_Click);
            // 
            // timer_timnpics
            // 
            this.timer_timnpics.Tick += new System.EventHandler(this.timer_timnpics_Tick);
            // 
            // FRM_Locationfetcher
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(975, 417);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FRM_Locationfetcher";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Location Fetcher";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FRM_Locationfetcher_FormClosed);
            this.Load += new System.EventHandler(this.FRM_Locationfetcher_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lbl_location_fetched;
        private System.Windows.Forms.Label lbl_location;
        private System.Windows.Forms.TextBox txt_longitude;
        private System.Windows.Forms.TextBox txt_latitude;
        private System.Windows.Forms.Label lbl_LONGITUDE;
        private System.Windows.Forms.Label lbl_LATITUDE;
        private System.Windows.Forms.Button BTN_LOC_SEARCH;
        private System.Windows.Forms.RichTextBox txt_rich_locfetcher;
        private System.Windows.Forms.TextBox txt_location;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button BTN_Cancel;
        private System.Windows.Forms.Button BTN_CLEAR;
        private System.Windows.Forms.Button BTN_Save;
        private System.Windows.Forms.Timer timer_timnpics;
    }
}