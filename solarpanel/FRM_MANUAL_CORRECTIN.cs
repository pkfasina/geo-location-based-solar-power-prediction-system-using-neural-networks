﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace solarpanel
{
    public partial class FRM_MANUAL_CORRECTIN : Form
    {
        public FRM_MANUAL_CORRECTIN()
        {
            InitializeComponent();
        }
        public static string selected_climate_condition = "--Select--", selected_error = "00.00";// selected_error = "00%";
        sqlconnectionClass1 sqlcon_class_obj = new sqlconnectionClass1();
        private void aDDToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            try
            {
                txt_CLIMATE.Text = "";
                txt_DESCRIPTION.Text = "";
                txt_ERRORLEVEL.Text = "";
                button_ADD.Visible = true;
                txt_CLIMATE.Visible = true;
                comboBox_CLIMATECONDITION.Visible = false;
                button_DELETE.Visible = false;
                button_UPDATE.Visible = false;
                
                BTN_OK.Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void dELETEToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                txt_CLIMATE.Text = "";
                txt_DESCRIPTION.Text = "";
                txt_ERRORLEVEL.Text = "";
                comboBox_CLIMATECONDITION.Text = "--Select--";
                OleDbConnection sqlcon = sqlcon_class_obj.Database_Connection_Method();
                sqlcon.Open();
                OleDbCommand cmd = new OleDbCommand("SELECT ERROR_CONDITION FROM MANNUAL_CORRECTION", sqlcon);
                OleDbDataReader dr = cmd.ExecuteReader();
                comboBox_CLIMATECONDITION.Items.Clear();
                //comboBox_manualid.Items.Clear();
                while (dr.Read())
                {
                    comboBox_CLIMATECONDITION.Items.Add(dr[0].ToString());
                }
                sqlcon.Close();
                button_ADD.Visible = false;
                //comboBox_CLIMATECONDITION.Visible = false;
                button_DELETE.Visible = true;
                button_UPDATE.Visible = false;
                comboBox_CLIMATECONDITION.Visible = true;
                txt_CLIMATE.Visible = false;
                label1.Visible = true;
                BTN_OK.Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void eDITToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                txt_CLIMATE.Text = "";
                txt_DESCRIPTION.Text = "";
                txt_ERRORLEVEL.Text = "";
                comboBox_CLIMATECONDITION.Text = "--Select--";
                comboBox_CLIMATECONDITION.Items.Clear();
                OleDbConnection sqlcon = sqlcon_class_obj.Database_Connection_Method();
                sqlcon.Open();
                OleDbCommand cmd = new OleDbCommand("SELECT ERROR_CONDITION FROM MANNUAL_CORRECTION", sqlcon);
                OleDbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    comboBox_CLIMATECONDITION.Items.Add(dr[0].ToString());
                }
                sqlcon.Close();
                button_ADD.Visible = false;
                button_DELETE.Visible = false;
                button_UPDATE.Visible = true;
                comboBox_CLIMATECONDITION.Visible = true;
                txt_CLIMATE.Visible = false;
                label1.Visible = true;
                BTN_OK.Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void sEARCHToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                txt_CLIMATE.Text = "";
                txt_DESCRIPTION.Text = "";
                txt_ERRORLEVEL.Text = "";
                comboBox_CLIMATECONDITION.Text = "--Select--";
                OleDbConnection sqlcon = sqlcon_class_obj.Database_Connection_Method();
                sqlcon.Open();
                OleDbCommand cmd = new OleDbCommand("SELECT ERROR_CONDITION FROM MANNUAL_CORRECTION", sqlcon);
                OleDbDataReader dr = cmd.ExecuteReader();
                comboBox_CLIMATECONDITION.Items.Clear();
                while (dr.Read())
                {
                    comboBox_CLIMATECONDITION.Items.Add(dr[0].ToString());
                } sqlcon.Close();
                button_ADD.Visible = false;
                comboBox_CLIMATECONDITION.Visible = true;
                button_DELETE.Visible = false;
                button_UPDATE.Visible = false;
                txt_CLIMATE.Visible = false;
                label1.Visible = true;
                BTN_OK.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        //private void vIEWToolStripMenuItem_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        //txt_CLIMATE.Text = "";
        //        //txt_DESCRIPTION.Text = "";
        //        //txt_ERRORLEVEL.Text = "";
        //        //BTN_OK.Visible = false;
        //        //FRM_View_Mannual_Correction frm_obj_manal_view = new FRM_View_Mannual_Correction();
        //        //frm_obj_manal_view.Show();
        //        //this.Dispose();


        //        ////fgfg
        //             txt_CLIMATE.Text = "";
        //        txt_DESCRIPTION.Text = "";
        //        txt_ERRORLEVEL.Text = "";
        //        BTN_OK.Visible = false;
        //        FRM_View_Mannual_Correction frm_obj_manal_view = new FRM_View_Mannual_Correction();
        //        frm_obj_manal_view.Show();
        //        frm_obj_manal_view.MdiParent = this.MdiParent;
        //        this.Dispose();
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.ToString());
        //    }
        //}

        private void eXITToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                //FRM_SOLAR_POWER_SIMULATE frm_simulate_obj = new FRM_SOLAR_POWER_SIMULATE();
                //frm_simulate_obj.Show();
                this.Hide();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void FRM_MANUAL_CORRECTIN_Load(object sender, EventArgs e)
        {
            try
            {
                button_ADD.Visible = true;
                button_UPDATE.Visible = false;
                button_DELETE.Visible = false;
                BTN_OK.Visible = false;
                txt_CLIMATE.Visible = true;
                comboBox_CLIMATECONDITION.Visible = false;
                menuStrip1.Visible = true;
                txt_CLIMATE.Text = "";
                txt_DESCRIPTION.Text = "";
                txt_ERRORLEVEL.Text = "";
                comboBox_CLIMATECONDITION.Text = "--Select--";
                OleDbConnection sqlcon = sqlcon_class_obj.Database_Connection_Method();
                sqlcon.Open();
                OleDbCommand cmd = new OleDbCommand("SELECT ERROR_CONDITION FROM MANNUAL_CORRECTION", sqlcon);
                OleDbDataReader dr = cmd.ExecuteReader();
                comboBox_CLIMATECONDITION.Items.Clear();
                while (dr.Read())
                {
                    comboBox_CLIMATECONDITION.Items.Add(dr[0].ToString());
                }
                sqlcon.Close();
                BTN_CANCEL.Visible = true;
                

                
                
                
               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void button_UPDATE_Click(object sender, EventArgs e)
        {
            try
            {
                if (comboBox_CLIMATECONDITION.Text!=""&& txt_DESCRIPTION.Text != "" && txt_ERRORLEVEL.Text != "")
                {
                    OleDbConnection sqlcon = sqlcon_class_obj.Database_Connection_Method();
                    sqlcon.Open();
                    string query = "UPDATE MANNUAL_CORRECTION set ERROR_LEVEL='" + txt_ERRORLEVEL.Text + "',DESCRIPTION='" + txt_DESCRIPTION.Text + "' where ERROR_CONDITION='" + comboBox_CLIMATECONDITION.SelectedItem + "'";
                    OleDbCommand cmd = new OleDbCommand(query, sqlcon);
                    cmd.ExecuteNonQuery();
                    sqlcon.Close();
                    txt_CLIMATE.Text = "";
                    txt_DESCRIPTION.Text = "";
                    txt_ERRORLEVEL.Text = "";
                    MessageBox.Show("Successfully Updated");
                }
                else { MessageBox.Show("Enter Data"); }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void button_DELETE_Click(object sender, EventArgs e)
        {
            try
            {
                OleDbConnection sqlcon = sqlcon_class_obj.Database_Connection_Method();
                sqlcon.Open();
                string query = "DELETE FROM MANNUAL_CORRECTION where ERROR_CONDITION='" + comboBox_CLIMATECONDITION.SelectedItem + "'";
                OleDbCommand cmd = new OleDbCommand(query, sqlcon);
                cmd.ExecuteNonQuery();
                sqlcon.Close();
                MessageBox.Show("Deleted");
                txt_CLIMATE.Text = "";
                txt_DESCRIPTION.Text = "";
                txt_ERRORLEVEL.Text = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        int uid;
        private void button_ADD_Click(object sender, EventArgs e)
        {
            try
            {
                if (txt_CLIMATE.Text != "" && txt_DESCRIPTION.Text != "" && txt_ERRORLEVEL.Text != "")
                {
                    
                    OleDbConnection sqlcon = sqlcon_class_obj.Database_Connection_Method();
                    sqlcon.Open();
                    OleDbCommand cmd_select = new OleDbCommand("SELECT U_ID FROM User_Registration where U_NAME='"+FRM_MainHome.username+"'", sqlcon);
                    OleDbDataReader dr = cmd_select.ExecuteReader();
                    while(dr.Read())
                    { uid =Convert.ToInt32(dr[0].ToString()); }
                    sqlcon.Close();
                    sqlcon.Open();
                    string query = "insert into MANNUAL_CORRECTION (USER_ID,ERROR_CONDITION,ERROR_LEVEL,DESCRIPTION)values(" +uid+ ",'" + txt_CLIMATE.Text + "','" + txt_ERRORLEVEL.Text + "','" + txt_DESCRIPTION.Text + "')";
                    OleDbCommand cmd = new OleDbCommand(query, sqlcon);
                    cmd.ExecuteNonQuery();
                    sqlcon.Close();
                    txt_CLIMATE.Text = "";
                    txt_DESCRIPTION.Text = "";
                    txt_ERRORLEVEL.Text = "";
                    MessageBox.Show("Successfully Inserted");
                }
                else { MessageBox.Show("Enter Data"); }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void FRM_MANUAL_CORRECTIN_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                //FRM_SOLAR_POWER_SIMULATE frm_simulate_obj = new FRM_SOLAR_POWER_SIMULATE();
                //frm_simulate_obj.Show();
                this.Hide();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void timer_timnpics_Tick(object sender, EventArgs e)
        {
            try
            {
                DateTime time = System.DateTime.Now;
                //lbl_time.Text = Convert.ToString(time.ToLongTimeString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void comboBox_CLIMATECONDITION_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                OleDbConnection sqlcon = sqlcon_class_obj.Database_Connection_Method();
                sqlcon.Open();
                OleDbCommand cmd = new OleDbCommand("SELECT * FROM MANNUAL_CORRECTION where ERROR_CONDITION='" + comboBox_CLIMATECONDITION.SelectedItem + "'", sqlcon);
                OleDbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    txt_CLIMATE.Text = dr[1].ToString();
                    txt_DESCRIPTION.Text = dr[3].ToString(); ;
                    txt_ERRORLEVEL.Text = dr[2].ToString();
                }
                sqlcon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

       

        private void operationsToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void BTN_OK_Click(object sender, EventArgs e)
        {
            try
            {
                if (txt_ERRORLEVEL.Text == "") { MessageBox.Show("Please select one value"); }
                else
                {
                    selected_climate_condition = comboBox_CLIMATECONDITION.SelectedItem.ToString();
                    selected_error = txt_ERRORLEVEL.Text;

                    FRM_SOLAR_POWER_SIMULATE frm_simulate_obj = (FRM_SOLAR_POWER_SIMULATE)Application.OpenForms["FRM_SOLAR_POWER_SIMULATE"];
                    //frm_simulate_obj.Show();
                    frm_simulate_obj.lbl_result_climatecondition.Text = FRM_MANUAL_CORRECTIN.selected_climate_condition;
                    frm_simulate_obj.lbl_result_errorlevel.Text = FRM_MANUAL_CORRECTIN.selected_error;
                    this.Hide();
                    frm_simulate_obj.Refresh();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void BTN_CANCEL_Click(object sender, EventArgs e)
        {
            try
            {
                //FRM_SOLAR_POWER_SIMULATE frm_simulate_obj = new FRM_SOLAR_POWER_SIMULATE();
                //frm_simulate_obj.Show();
                this.Hide();
            }           
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
    }
}
