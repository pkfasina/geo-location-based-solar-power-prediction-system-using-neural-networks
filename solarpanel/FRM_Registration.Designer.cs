﻿namespace solarpanel
{
    partial class FRM_Registration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.userDetailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.insertionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lbl_U_ID = new System.Windows.Forms.Label();
            this.comboBox_U_ID = new System.Windows.Forms.ComboBox();
            this.monthCalendar1 = new System.Windows.Forms.MonthCalendar();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lbl_Validation_password = new System.Windows.Forms.Label();
            this.lbl_Validation_User_Name = new System.Windows.Forms.Label();
            this.txt_confirmpassword = new System.Windows.Forms.TextBox();
            this.txt_password = new System.Windows.Forms.TextBox();
            this.txt_User_Name = new System.Windows.Forms.TextBox();
            this.lbl_confirm_password = new System.Windows.Forms.Label();
            this.lbl_password = new System.Windows.Forms.Label();
            this.lbl_User_name = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lbl_Validation_email = new System.Windows.Forms.Label();
            this.lbl_Validation_address = new System.Windows.Forms.Label();
            this.lbl_Validation_status = new System.Windows.Forms.Label();
            this.checkB_disable = new System.Windows.Forms.CheckBox();
            this.checkB_Enable = new System.Windows.Forms.CheckBox();
            this.combo_YEAR = new System.Windows.Forms.ComboBox();
            this.combo_MONTH = new System.Windows.Forms.ComboBox();
            this.combo_date = new System.Windows.Forms.ComboBox();
            this.radio_female = new System.Windows.Forms.RadioButton();
            this.radio_Male = new System.Windows.Forms.RadioButton();
            this.richtxt_address = new System.Windows.Forms.RichTextBox();
            this.txt_email = new System.Windows.Forms.TextBox();
            this.txt_name = new System.Windows.Forms.TextBox();
            this.lbl_Status = new System.Windows.Forms.Label();
            this.lbl_address = new System.Windows.Forms.Label();
            this.lbl_Dob = new System.Windows.Forms.Label();
            this.lbl_email = new System.Windows.Forms.Label();
            this.lbl_gender = new System.Windows.Forms.Label();
            this.lbl_Name = new System.Windows.Forms.Label();
            this.Btn_Update = new System.Windows.Forms.Button();
            this.Btn_Insert = new System.Windows.Forms.Button();
            this.Btn_Cancel = new System.Windows.Forms.Button();
            this.lbl_Validation_Notification = new System.Windows.Forms.Label();
            this.txt_username_search = new System.Windows.Forms.TextBox();
            this.lbl_username_search = new System.Windows.Forms.Label();
            this.button_search = new System.Windows.Forms.Button();
            this.lbl_time = new System.Windows.Forms.Label();
            this.timer_time = new System.Windows.Forms.Timer(this.components);
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.userDetailsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(9, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(1107, 28);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // userDetailsToolStripMenuItem
            // 
            this.userDetailsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.insertionToolStripMenuItem,
            this.updationToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.userDetailsToolStripMenuItem.Name = "userDetailsToolStripMenuItem";
            this.userDetailsToolStripMenuItem.Size = new System.Drawing.Size(102, 24);
            this.userDetailsToolStripMenuItem.Text = "User Details";
            this.userDetailsToolStripMenuItem.Click += new System.EventHandler(this.userDetailsToolStripMenuItem_Click);
            // 
            // insertionToolStripMenuItem
            // 
            this.insertionToolStripMenuItem.Name = "insertionToolStripMenuItem";
            this.insertionToolStripMenuItem.Size = new System.Drawing.Size(154, 26);
            this.insertionToolStripMenuItem.Text = "Insertion";
            this.insertionToolStripMenuItem.Click += new System.EventHandler(this.insertionToolStripMenuItem_Click);
            // 
            // updationToolStripMenuItem
            // 
            this.updationToolStripMenuItem.Name = "updationToolStripMenuItem";
            this.updationToolStripMenuItem.Size = new System.Drawing.Size(154, 26);
            this.updationToolStripMenuItem.Text = "Updation";
            this.updationToolStripMenuItem.Click += new System.EventHandler(this.updationToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(154, 26);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // lbl_U_ID
            // 
            this.lbl_U_ID.AutoSize = true;
            this.lbl_U_ID.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.lbl_U_ID.Location = new System.Drawing.Point(810, 6);
            this.lbl_U_ID.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_U_ID.Name = "lbl_U_ID";
            this.lbl_U_ID.Size = new System.Drawing.Size(66, 18);
            this.lbl_U_ID.TabIndex = 1;
            this.lbl_U_ID.Text = "User ID :";
            // 
            // comboBox_U_ID
            // 
            this.comboBox_U_ID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox_U_ID.FormattingEnabled = true;
            this.comboBox_U_ID.Location = new System.Drawing.Point(894, 0);
            this.comboBox_U_ID.Margin = new System.Windows.Forms.Padding(4);
            this.comboBox_U_ID.Name = "comboBox_U_ID";
            this.comboBox_U_ID.Size = new System.Drawing.Size(127, 26);
            this.comboBox_U_ID.TabIndex = 2;
            this.comboBox_U_ID.Text = "- Select -";
            this.comboBox_U_ID.SelectedIndexChanged += new System.EventHandler(this.comboBox_U_ID_SelectedIndexChanged);
            // 
            // monthCalendar1
            // 
            this.monthCalendar1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.monthCalendar1.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.monthCalendar1.Location = new System.Drawing.Point(815, 54);
            this.monthCalendar1.Margin = new System.Windows.Forms.Padding(14, 12, 14, 12);
            this.monthCalendar1.Name = "monthCalendar1";
            this.monthCalendar1.TabIndex = 4;
            this.monthCalendar1.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.monthCalendar1_DateChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.lbl_Validation_password);
            this.groupBox1.Controls.Add(this.lbl_Validation_User_Name);
            this.groupBox1.Controls.Add(this.txt_confirmpassword);
            this.groupBox1.Controls.Add(this.txt_password);
            this.groupBox1.Controls.Add(this.txt_User_Name);
            this.groupBox1.Controls.Add(this.lbl_confirm_password);
            this.groupBox1.Controls.Add(this.lbl_password);
            this.groupBox1.Controls.Add(this.lbl_User_name);
            this.groupBox1.Location = new System.Drawing.Point(10, 36);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(787, 213);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            // 
            // lbl_Validation_password
            // 
            this.lbl_Validation_password.AutoSize = true;
            this.lbl_Validation_password.BackColor = System.Drawing.Color.Transparent;
            this.lbl_Validation_password.Font = new System.Drawing.Font("Courier New", 8.5F, System.Drawing.FontStyle.Bold);
            this.lbl_Validation_password.ForeColor = System.Drawing.Color.Red;
            this.lbl_Validation_password.Location = new System.Drawing.Point(480, 102);
            this.lbl_Validation_password.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_Validation_password.Name = "lbl_Validation_password";
            this.lbl_Validation_password.Size = new System.Drawing.Size(269, 34);
            this.lbl_Validation_password.TabIndex = 12;
            this.lbl_Validation_password.Text = "Use Combination of letters,\r\ndigits and special characters";
            this.lbl_Validation_password.Click += new System.EventHandler(this.lbl_Validation_password_Click);
            // 
            // lbl_Validation_User_Name
            // 
            this.lbl_Validation_User_Name.AutoSize = true;
            this.lbl_Validation_User_Name.BackColor = System.Drawing.Color.Transparent;
            this.lbl_Validation_User_Name.Font = new System.Drawing.Font("Courier New", 8.5F, System.Drawing.FontStyle.Bold);
            this.lbl_Validation_User_Name.ForeColor = System.Drawing.Color.Red;
            this.lbl_Validation_User_Name.Location = new System.Drawing.Point(499, 52);
            this.lbl_Validation_User_Name.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_Validation_User_Name.Name = "lbl_Validation_User_Name";
            this.lbl_Validation_User_Name.Size = new System.Drawing.Size(170, 17);
            this.lbl_Validation_User_Name.TabIndex = 10;
            this.lbl_Validation_User_Name.Text = "Digits not Allowed";
            // 
            // txt_confirmpassword
            // 
            this.txt_confirmpassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_confirmpassword.Location = new System.Drawing.Point(184, 153);
            this.txt_confirmpassword.Margin = new System.Windows.Forms.Padding(4);
            this.txt_confirmpassword.Name = "txt_confirmpassword";
            this.txt_confirmpassword.PasswordChar = '*';
            this.txt_confirmpassword.Size = new System.Drawing.Size(284, 24);
            this.txt_confirmpassword.TabIndex = 2;
            this.txt_confirmpassword.TextChanged += new System.EventHandler(this.txt_confirmpassword_TextChanged);
            // 
            // txt_password
            // 
            this.txt_password.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_password.Location = new System.Drawing.Point(184, 103);
            this.txt_password.Margin = new System.Windows.Forms.Padding(4);
            this.txt_password.Name = "txt_password";
            this.txt_password.PasswordChar = '*';
            this.txt_password.Size = new System.Drawing.Size(284, 24);
            this.txt_password.TabIndex = 1;
            this.txt_password.TextChanged += new System.EventHandler(this.txt_password_TextChanged);
            // 
            // txt_User_Name
            // 
            this.txt_User_Name.Location = new System.Drawing.Point(184, 53);
            this.txt_User_Name.Margin = new System.Windows.Forms.Padding(4);
            this.txt_User_Name.Name = "txt_User_Name";
            this.txt_User_Name.Size = new System.Drawing.Size(284, 24);
            this.txt_User_Name.TabIndex = 0;
            this.txt_User_Name.TextChanged += new System.EventHandler(this.txt_User_Name_TextChanged);
            // 
            // lbl_confirm_password
            // 
            this.lbl_confirm_password.AutoSize = true;
            this.lbl_confirm_password.BackColor = System.Drawing.Color.Transparent;
            this.lbl_confirm_password.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_confirm_password.Location = new System.Drawing.Point(23, 157);
            this.lbl_confirm_password.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_confirm_password.Name = "lbl_confirm_password";
            this.lbl_confirm_password.Size = new System.Drawing.Size(136, 17);
            this.lbl_confirm_password.TabIndex = 4;
            this.lbl_confirm_password.Text = "Confirm password";
            // 
            // lbl_password
            // 
            this.lbl_password.AutoSize = true;
            this.lbl_password.BackColor = System.Drawing.Color.Transparent;
            this.lbl_password.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_password.Location = new System.Drawing.Point(53, 103);
            this.lbl_password.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_password.Name = "lbl_password";
            this.lbl_password.Size = new System.Drawing.Size(77, 17);
            this.lbl_password.TabIndex = 3;
            this.lbl_password.Text = "Password";
            this.lbl_password.Click += new System.EventHandler(this.lbl_password_Click);
            // 
            // lbl_User_name
            // 
            this.lbl_User_name.AutoSize = true;
            this.lbl_User_name.BackColor = System.Drawing.Color.Transparent;
            this.lbl_User_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_User_name.Location = new System.Drawing.Point(42, 53);
            this.lbl_User_name.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_User_name.Name = "lbl_User_name";
            this.lbl_User_name.Size = new System.Drawing.Size(88, 17);
            this.lbl_User_name.TabIndex = 2;
            this.lbl_User_name.Text = "User Name";
            this.lbl_User_name.Click += new System.EventHandler(this.lbl_User_name_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lbl_Validation_email);
            this.groupBox2.Controls.Add(this.lbl_Validation_address);
            this.groupBox2.Controls.Add(this.lbl_Validation_status);
            this.groupBox2.Controls.Add(this.checkB_disable);
            this.groupBox2.Controls.Add(this.checkB_Enable);
            this.groupBox2.Controls.Add(this.combo_YEAR);
            this.groupBox2.Controls.Add(this.combo_MONTH);
            this.groupBox2.Controls.Add(this.combo_date);
            this.groupBox2.Controls.Add(this.radio_female);
            this.groupBox2.Controls.Add(this.radio_Male);
            this.groupBox2.Controls.Add(this.richtxt_address);
            this.groupBox2.Controls.Add(this.txt_email);
            this.groupBox2.Controls.Add(this.txt_name);
            this.groupBox2.Controls.Add(this.lbl_Status);
            this.groupBox2.Controls.Add(this.lbl_address);
            this.groupBox2.Controls.Add(this.lbl_Dob);
            this.groupBox2.Controls.Add(this.lbl_email);
            this.groupBox2.Controls.Add(this.lbl_gender);
            this.groupBox2.Controls.Add(this.lbl_Name);
            this.groupBox2.Location = new System.Drawing.Point(10, 287);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(1065, 271);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Personal Details";
            this.groupBox2.Enter += new System.EventHandler(this.groupBox2_Enter);
            // 
            // lbl_Validation_email
            // 
            this.lbl_Validation_email.AutoSize = true;
            this.lbl_Validation_email.BackColor = System.Drawing.Color.Transparent;
            this.lbl_Validation_email.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Validation_email.ForeColor = System.Drawing.Color.Red;
            this.lbl_Validation_email.Location = new System.Drawing.Point(1040, 56);
            this.lbl_Validation_email.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_Validation_email.Name = "lbl_Validation_email";
            this.lbl_Validation_email.Size = new System.Drawing.Size(19, 20);
            this.lbl_Validation_email.TabIndex = 24;
            this.lbl_Validation_email.Text = "*";
            // 
            // lbl_Validation_address
            // 
            this.lbl_Validation_address.AutoSize = true;
            this.lbl_Validation_address.BackColor = System.Drawing.Color.Transparent;
            this.lbl_Validation_address.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Validation_address.ForeColor = System.Drawing.Color.Red;
            this.lbl_Validation_address.Location = new System.Drawing.Point(1040, 147);
            this.lbl_Validation_address.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_Validation_address.Name = "lbl_Validation_address";
            this.lbl_Validation_address.Size = new System.Drawing.Size(19, 20);
            this.lbl_Validation_address.TabIndex = 23;
            this.lbl_Validation_address.Text = "*";
            // 
            // lbl_Validation_status
            // 
            this.lbl_Validation_status.AutoSize = true;
            this.lbl_Validation_status.BackColor = System.Drawing.Color.Transparent;
            this.lbl_Validation_status.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Validation_status.ForeColor = System.Drawing.Color.Red;
            this.lbl_Validation_status.Location = new System.Drawing.Point(408, 227);
            this.lbl_Validation_status.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_Validation_status.Name = "lbl_Validation_status";
            this.lbl_Validation_status.Size = new System.Drawing.Size(19, 20);
            this.lbl_Validation_status.TabIndex = 22;
            this.lbl_Validation_status.Text = "*";
            // 
            // checkB_disable
            // 
            this.checkB_disable.AutoSize = true;
            this.checkB_disable.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkB_disable.Location = new System.Drawing.Point(297, 223);
            this.checkB_disable.Margin = new System.Windows.Forms.Padding(4);
            this.checkB_disable.Name = "checkB_disable";
            this.checkB_disable.Size = new System.Drawing.Size(84, 21);
            this.checkB_disable.TabIndex = 7;
            this.checkB_disable.Text = "Disable";
            this.checkB_disable.UseVisualStyleBackColor = true;
            this.checkB_disable.CheckedChanged += new System.EventHandler(this.checkB_disable_CheckedChanged);
            // 
            // checkB_Enable
            // 
            this.checkB_Enable.AutoSize = true;
            this.checkB_Enable.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkB_Enable.Location = new System.Drawing.Point(184, 223);
            this.checkB_Enable.Margin = new System.Windows.Forms.Padding(4);
            this.checkB_Enable.Name = "checkB_Enable";
            this.checkB_Enable.Size = new System.Drawing.Size(80, 21);
            this.checkB_Enable.TabIndex = 6;
            this.checkB_Enable.Text = "Enable";
            this.checkB_Enable.UseVisualStyleBackColor = true;
            this.checkB_Enable.CheckedChanged += new System.EventHandler(this.checkB_Enable_CheckedChanged);
            // 
            // combo_YEAR
            // 
            this.combo_YEAR.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.combo_YEAR.FormattingEnabled = true;
            this.combo_YEAR.Location = new System.Drawing.Point(361, 161);
            this.combo_YEAR.Margin = new System.Windows.Forms.Padding(4);
            this.combo_YEAR.Name = "combo_YEAR";
            this.combo_YEAR.Size = new System.Drawing.Size(118, 25);
            this.combo_YEAR.TabIndex = 5;
            this.combo_YEAR.Text = "YYYY";
            // 
            // combo_MONTH
            // 
            this.combo_MONTH.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.combo_MONTH.FormattingEnabled = true;
            this.combo_MONTH.Location = new System.Drawing.Point(271, 161);
            this.combo_MONTH.Margin = new System.Windows.Forms.Padding(4);
            this.combo_MONTH.Name = "combo_MONTH";
            this.combo_MONTH.Size = new System.Drawing.Size(68, 25);
            this.combo_MONTH.TabIndex = 4;
            this.combo_MONTH.Text = "MM";
            // 
            // combo_date
            // 
            this.combo_date.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.combo_date.FormattingEnabled = true;
            this.combo_date.Location = new System.Drawing.Point(179, 161);
            this.combo_date.Margin = new System.Windows.Forms.Padding(4);
            this.combo_date.Name = "combo_date";
            this.combo_date.Size = new System.Drawing.Size(73, 25);
            this.combo_date.TabIndex = 3;
            this.combo_date.Text = "DD";
            this.combo_date.SelectedIndexChanged += new System.EventHandler(this.combo_date_SelectedIndexChanged);
            // 
            // radio_female
            // 
            this.radio_female.AutoSize = true;
            this.radio_female.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radio_female.Location = new System.Drawing.Point(297, 109);
            this.radio_female.Margin = new System.Windows.Forms.Padding(4);
            this.radio_female.Name = "radio_female";
            this.radio_female.Size = new System.Drawing.Size(81, 21);
            this.radio_female.TabIndex = 2;
            this.radio_female.TabStop = true;
            this.radio_female.Text = "Female";
            this.radio_female.UseVisualStyleBackColor = true;
            // 
            // radio_Male
            // 
            this.radio_Male.AutoSize = true;
            this.radio_Male.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radio_Male.Location = new System.Drawing.Point(181, 109);
            this.radio_Male.Margin = new System.Windows.Forms.Padding(4);
            this.radio_Male.Name = "radio_Male";
            this.radio_Male.Size = new System.Drawing.Size(63, 21);
            this.radio_Male.TabIndex = 1;
            this.radio_Male.TabStop = true;
            this.radio_Male.Text = "Male";
            this.radio_Male.UseVisualStyleBackColor = true;
            // 
            // richtxt_address
            // 
            this.richtxt_address.Location = new System.Drawing.Point(640, 94);
            this.richtxt_address.Margin = new System.Windows.Forms.Padding(4);
            this.richtxt_address.Name = "richtxt_address";
            this.richtxt_address.Size = new System.Drawing.Size(390, 155);
            this.richtxt_address.TabIndex = 9;
            this.richtxt_address.Text = "";
            // 
            // txt_email
            // 
            this.txt_email.Location = new System.Drawing.Point(640, 53);
            this.txt_email.Margin = new System.Windows.Forms.Padding(4);
            this.txt_email.Name = "txt_email";
            this.txt_email.Size = new System.Drawing.Size(390, 24);
            this.txt_email.TabIndex = 8;
            this.txt_email.TextChanged += new System.EventHandler(this.txt_email_TextChanged);
            // 
            // txt_name
            // 
            this.txt_name.Location = new System.Drawing.Point(177, 47);
            this.txt_name.Margin = new System.Windows.Forms.Padding(4);
            this.txt_name.Name = "txt_name";
            this.txt_name.Size = new System.Drawing.Size(329, 24);
            this.txt_name.TabIndex = 0;
            // 
            // lbl_Status
            // 
            this.lbl_Status.AutoSize = true;
            this.lbl_Status.BackColor = System.Drawing.Color.Transparent;
            this.lbl_Status.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Status.Location = new System.Drawing.Point(8, 227);
            this.lbl_Status.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_Status.Name = "lbl_Status";
            this.lbl_Status.Size = new System.Drawing.Size(54, 17);
            this.lbl_Status.TabIndex = 8;
            this.lbl_Status.Text = "Status";
            // 
            // lbl_address
            // 
            this.lbl_address.AutoSize = true;
            this.lbl_address.BackColor = System.Drawing.Color.Transparent;
            this.lbl_address.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_address.Location = new System.Drawing.Point(536, 161);
            this.lbl_address.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_address.Name = "lbl_address";
            this.lbl_address.Size = new System.Drawing.Size(67, 17);
            this.lbl_address.TabIndex = 7;
            this.lbl_address.Text = "Address";
            // 
            // lbl_Dob
            // 
            this.lbl_Dob.AutoSize = true;
            this.lbl_Dob.BackColor = System.Drawing.Color.Transparent;
            this.lbl_Dob.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Dob.Location = new System.Drawing.Point(8, 166);
            this.lbl_Dob.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_Dob.Name = "lbl_Dob";
            this.lbl_Dob.Size = new System.Drawing.Size(103, 17);
            this.lbl_Dob.TabIndex = 6;
            this.lbl_Dob.Text = "Date Of Birth";
            // 
            // lbl_email
            // 
            this.lbl_email.AutoSize = true;
            this.lbl_email.BackColor = System.Drawing.Color.Transparent;
            this.lbl_email.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_email.Location = new System.Drawing.Point(558, 58);
            this.lbl_email.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_email.Name = "lbl_email";
            this.lbl_email.Size = new System.Drawing.Size(47, 17);
            this.lbl_email.TabIndex = 5;
            this.lbl_email.Text = "Email";
            // 
            // lbl_gender
            // 
            this.lbl_gender.AutoSize = true;
            this.lbl_gender.BackColor = System.Drawing.Color.Transparent;
            this.lbl_gender.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_gender.Location = new System.Drawing.Point(9, 109);
            this.lbl_gender.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_gender.Name = "lbl_gender";
            this.lbl_gender.Size = new System.Drawing.Size(62, 17);
            this.lbl_gender.TabIndex = 4;
            this.lbl_gender.Text = "Gender";
            // 
            // lbl_Name
            // 
            this.lbl_Name.AutoSize = true;
            this.lbl_Name.BackColor = System.Drawing.Color.Transparent;
            this.lbl_Name.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Name.Location = new System.Drawing.Point(8, 52);
            this.lbl_Name.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_Name.Name = "lbl_Name";
            this.lbl_Name.Size = new System.Drawing.Size(80, 17);
            this.lbl_Name.TabIndex = 3;
            this.lbl_Name.Text = "Full Name";
            // 
            // Btn_Update
            // 
            this.Btn_Update.BackColor = System.Drawing.Color.DodgerBlue;
            this.Btn_Update.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Update.ForeColor = System.Drawing.SystemColors.Window;
            this.Btn_Update.Location = new System.Drawing.Point(518, 578);
            this.Btn_Update.Margin = new System.Windows.Forms.Padding(4);
            this.Btn_Update.Name = "Btn_Update";
            this.Btn_Update.Size = new System.Drawing.Size(156, 42);
            this.Btn_Update.TabIndex = 4;
            this.Btn_Update.Text = "Edit";
            this.Btn_Update.UseVisualStyleBackColor = false;
            this.Btn_Update.Click += new System.EventHandler(this.Btn_Update_Click);
            // 
            // Btn_Insert
            // 
            this.Btn_Insert.BackColor = System.Drawing.Color.DodgerBlue;
            this.Btn_Insert.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Insert.ForeColor = System.Drawing.SystemColors.Window;
            this.Btn_Insert.Location = new System.Drawing.Point(518, 578);
            this.Btn_Insert.Margin = new System.Windows.Forms.Padding(4);
            this.Btn_Insert.Name = "Btn_Insert";
            this.Btn_Insert.Size = new System.Drawing.Size(156, 42);
            this.Btn_Insert.TabIndex = 0;
            this.Btn_Insert.Text = "ADD";
            this.Btn_Insert.UseVisualStyleBackColor = false;
            this.Btn_Insert.Click += new System.EventHandler(this.Btn_Insert_Click);
            // 
            // Btn_Cancel
            // 
            this.Btn_Cancel.BackColor = System.Drawing.Color.DodgerBlue;
            this.Btn_Cancel.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Cancel.ForeColor = System.Drawing.SystemColors.Window;
            this.Btn_Cancel.Location = new System.Drawing.Point(697, 578);
            this.Btn_Cancel.Margin = new System.Windows.Forms.Padding(4);
            this.Btn_Cancel.Name = "Btn_Cancel";
            this.Btn_Cancel.Size = new System.Drawing.Size(156, 42);
            this.Btn_Cancel.TabIndex = 5;
            this.Btn_Cancel.Text = "Clear";
            this.Btn_Cancel.UseVisualStyleBackColor = false;
            this.Btn_Cancel.Click += new System.EventHandler(this.Btn_Cancel_Click);
            // 
            // lbl_Validation_Notification
            // 
            this.lbl_Validation_Notification.AutoSize = true;
            this.lbl_Validation_Notification.Font = new System.Drawing.Font("Courier New", 8.5F, System.Drawing.FontStyle.Bold);
            this.lbl_Validation_Notification.ForeColor = System.Drawing.Color.Red;
            this.lbl_Validation_Notification.Location = new System.Drawing.Point(13, 590);
            this.lbl_Validation_Notification.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_Validation_Notification.Name = "lbl_Validation_Notification";
            this.lbl_Validation_Notification.Size = new System.Drawing.Size(359, 17);
            this.lbl_Validation_Notification.TabIndex = 8;
            this.lbl_Validation_Notification.Text = "Note:*Enter your data in regular format";
            // 
            // txt_username_search
            // 
            this.txt_username_search.Location = new System.Drawing.Point(423, 2);
            this.txt_username_search.Margin = new System.Windows.Forms.Padding(4);
            this.txt_username_search.Name = "txt_username_search";
            this.txt_username_search.Size = new System.Drawing.Size(238, 24);
            this.txt_username_search.TabIndex = 9;
            this.txt_username_search.TextChanged += new System.EventHandler(this.txt_username_search_TextChanged);
            // 
            // lbl_username_search
            // 
            this.lbl_username_search.AutoSize = true;
            this.lbl_username_search.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.lbl_username_search.Location = new System.Drawing.Point(314, 7);
            this.lbl_username_search.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_username_search.Name = "lbl_username_search";
            this.lbl_username_search.Size = new System.Drawing.Size(92, 18);
            this.lbl_username_search.TabIndex = 10;
            this.lbl_username_search.Text = "User Name :";
            // 
            // button_search
            // 
            this.button_search.BackColor = System.Drawing.Color.DodgerBlue;
            this.button_search.ForeColor = System.Drawing.SystemColors.Window;
            this.button_search.Location = new System.Drawing.Point(670, 0);
            this.button_search.Margin = new System.Windows.Forms.Padding(4);
            this.button_search.Name = "button_search";
            this.button_search.Size = new System.Drawing.Size(112, 32);
            this.button_search.TabIndex = 11;
            this.button_search.Text = "Search";
            this.button_search.UseVisualStyleBackColor = false;
            this.button_search.Click += new System.EventHandler(this.button_search_Click);
            // 
            // lbl_time
            // 
            this.lbl_time.AutoSize = true;
            this.lbl_time.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_time.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.lbl_time.Location = new System.Drawing.Point(963, 590);
            this.lbl_time.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_time.Name = "lbl_time";
            this.lbl_time.Size = new System.Drawing.Size(45, 18);
            this.lbl_time.TabIndex = 13;
            this.lbl_time.Text = "Time";
            this.lbl_time.Click += new System.EventHandler(this.lbl_time_Click);
            // 
            // timer_time
            // 
            this.timer_time.Tick += new System.EventHandler(this.timer_time_Tick);
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Courier New", 8.5F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(505, 161);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(161, 17);
            this.label1.TabIndex = 13;
            this.label1.Text = "Password Mismatch";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // FRM_Registration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.ClientSize = new System.Drawing.Size(1107, 639);
            this.Controls.Add(this.Btn_Update);
            this.Controls.Add(this.lbl_time);
            this.Controls.Add(this.button_search);
            this.Controls.Add(this.lbl_username_search);
            this.Controls.Add(this.txt_username_search);
            this.Controls.Add(this.lbl_Validation_Notification);
            this.Controls.Add(this.Btn_Cancel);
            this.Controls.Add(this.Btn_Insert);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.monthCalendar1);
            this.Controls.Add(this.comboBox_U_ID);
            this.Controls.Add(this.lbl_U_ID);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.8F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FRM_Registration";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Registration";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FRM_Registration_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FRM_Registration_FormClosed);
            this.Load += new System.EventHandler(this.FRM_Registration_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem userDetailsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem insertionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem updationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.Label lbl_U_ID;
        private System.Windows.Forms.ComboBox comboBox_U_ID;
        private System.Windows.Forms.MonthCalendar monthCalendar1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txt_confirmpassword;
        private System.Windows.Forms.TextBox txt_password;
        private System.Windows.Forms.TextBox txt_User_Name;
        private System.Windows.Forms.Label lbl_confirm_password;
        private System.Windows.Forms.Label lbl_password;
        private System.Windows.Forms.Label lbl_User_name;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox checkB_Enable;
        private System.Windows.Forms.ComboBox combo_YEAR;
        private System.Windows.Forms.ComboBox combo_MONTH;
        private System.Windows.Forms.ComboBox combo_date;
        private System.Windows.Forms.RadioButton radio_female;
        private System.Windows.Forms.RadioButton radio_Male;
        private System.Windows.Forms.RichTextBox richtxt_address;
        private System.Windows.Forms.TextBox txt_email;
        private System.Windows.Forms.TextBox txt_name;
        private System.Windows.Forms.Label lbl_Status;
        private System.Windows.Forms.Label lbl_address;
        private System.Windows.Forms.Label lbl_Dob;
        private System.Windows.Forms.Label lbl_email;
        private System.Windows.Forms.Label lbl_gender;
        private System.Windows.Forms.Label lbl_Name;
        private System.Windows.Forms.CheckBox checkB_disable;
        private System.Windows.Forms.Button Btn_Insert;
        private System.Windows.Forms.Button Btn_Cancel;
        private System.Windows.Forms.Label lbl_Validation_password;
        private System.Windows.Forms.Label lbl_Validation_User_Name;
        private System.Windows.Forms.Label lbl_Validation_email;
        private System.Windows.Forms.Label lbl_Validation_address;
        private System.Windows.Forms.Label lbl_Validation_status;
        private System.Windows.Forms.Label lbl_Validation_Notification;
        private System.Windows.Forms.TextBox txt_username_search;
        private System.Windows.Forms.Label lbl_username_search;
        private System.Windows.Forms.Button button_search;
        private System.Windows.Forms.Label lbl_time;
        private System.Windows.Forms.Timer timer_time;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.Button Btn_Update;
        private System.Windows.Forms.Label label1;
    }
}

