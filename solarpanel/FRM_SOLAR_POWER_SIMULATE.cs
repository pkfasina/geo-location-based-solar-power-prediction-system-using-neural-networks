﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Drawing.Imaging;  
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace solarpanel
{
    public partial class FRM_SOLAR_POWER_SIMULATE : Form

    {



        
        public FRM_SOLAR_POWER_SIMULATE()

           
        {
            InitializeComponent();
           
        }

        [System.Runtime.InteropServices.DllImportAttribute("gdi32.dll")]
        private System.IO.Stream streamToPrint;
        string streamType;
        private static extern bool BitBlt
        (
            IntPtr hdcDest, // handle to destination DC  
            int nXDest, // x-coord of destination upper-left corner  
            int nYDest, // y-coord of destination upper-left corner  
            int nWidth, // width of destination rectangle  
            int nHeight, // height of destination rectangle  
            IntPtr hdcSrc, // handle to source DC  
            int nXSrc, // x-coordinate of source upper-left corner  
            int nYSrc, // y-coordinate of source upper-left corner  
            System.Int32 dwRop // raster operation code  
        ); 


       

        ////**********not moving form*************//
        protected override void WndProc(ref Message message)
        {
            const int WM_SYSCOMMAND = 0x0112;
            const int SC_MOVE = 0xF010;

            switch (message.Msg)
            {
                case WM_SYSCOMMAND:
                    int command = message.WParam.ToInt32() & 0xfff0;
                    if (command == SC_MOVE)
                        return;
                    break;
            }

            base.WndProc(ref message);
        }
        ////**********************************//


        //string username;
        public int stat = 0;
        private void BTN_CONFIG_Click(object sender, EventArgs e)
        {
            try
            {
                FRM_Configuration config_obj = new FRM_Configuration();
                config_obj.Show();
                config_obj.MdiParent = this.MdiParent;
                //this.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void BTN_BASECELL_CONFIG_Click(object sender, EventArgs e)
        {
            try
            {
                BTN_BASECELL_CONFIG.Text = "Cell Configuration View";
                //FRM_VIEW_BASE_CELL_CONFIG view_obj = new FRM_VIEW_BASE_CELL_CONFIG();
                //view_obj.Show();
                //view_obj.MdiParent = this.MdiParent;
                FRM_BASECELLCONFIG base_config_obj = new FRM_BASECELLCONFIG();
                base_config_obj.Show();
                base_config_obj.MdiParent = this.MdiParent;

                //if (Convert.ToInt32(FRM_Login.authentication) == 2)
                //{
                //    BTN_BASECELL_CONFIG.Text = "Cell Configuration View";
                //    FRM_VIEW_BASE_CELL_CONFIG view_obj = new FRM_VIEW_BASE_CELL_CONFIG();
                //    view_obj.Show();
                //    view_obj.MdiParent = this.MdiParent;
                //    //this.Hide();
                //}
                //else 
                //{
                //    FRM_BASECELLCONFIG base_config_obj = new FRM_BASECELLCONFIG();
                //    base_config_obj.Show();
                //    base_config_obj.MdiParent = this.MdiParent;
                //   // this.Dispose();
                //}
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void BTN_LOCATIONFETCHER_Click(object sender, EventArgs e)
        {
            try
            {
                FRM_Locationfetcher loc_fecth_obj = new FRM_Locationfetcher();
                loc_fecth_obj.Show();
                loc_fecth_obj.MdiParent = this.MdiParent;
               // this.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void BTN_Climatefetcher_Click(object sender, EventArgs e)
        {
            try
            {
                FRM_CLIMATEFETCHER climate_obj = new FRM_CLIMATEFETCHER();
                climate_obj.Show();
                climate_obj.MdiParent = this.MdiParent;
               // this.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void BTN_mannualcorrection_Click(object sender, EventArgs e)
        {
            try
            {
                FRM_MANUAL_CORRECTIN manual_obj = new FRM_MANUAL_CORRECTIN();
                manual_obj.Show();
                manual_obj.MdiParent = this.MdiParent;
                //this.Hide();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void BTN_Calculate_Click(object sender, EventArgs e)
        {
            try
            {
                double angle; double pi_value = Math.PI;
                DateTime year = System.DateTime.Now;
                string yera_ = year.Year.ToString();
                int year_compare = Convert.ToInt32(yera_);
                string month = year.Month.ToString();
                int mon_compare = Convert.ToInt32(month);
                

                DateTime constdate = new DateTime(year_compare, 04, 21);
                double double_day_diff = Convert.ToDouble((dateTimePic_calc.Value - constdate).TotalDays);
                int day_diff = Convert.ToInt32(double_day_diff);
                if (constdate > dateTimePic_calc.Value)
                {
                    day_diff = 365 - day_diff;
                }
                double z = Math.Round((2 * pi_value) * (day_diff / 365.25), 1);
                double y = Math.Sin(z);
                double x = (23.5 * y);
              

              
                double angle_summer = Convert.ToDouble(lbl_result_latitude.Text) * 0.9 - 23.5;
                double angle_winter = Convert.ToDouble(lbl_result_latitude.Text) * 0.9 + 29;
                double angle_spring = Convert.ToDouble(lbl_result_latitude.Text) * 0.9 - 2.5;
                double angle_fall = Convert.ToDouble(lbl_result_latitude.Text) * 0.9 - 2.5;
                angle = Convert.ToDouble(23.5 * Math.Sin(Math.Round((2 * pi_value) * (day_diff / 365.25), 1)));
                double sun_const = 137 * (Math.Cos(Convert.ToDouble(lbl_result_latitude.Text) - Convert.ToDouble(angle)));
                double dblCelsius, modulrating = Convert.ToDouble(lbl_result_modulerating.Text);
                double dblFahrenheit, cell_voltage = Convert.ToDouble(lbl_result_voltage.Text);
                double syscapacity = Convert.ToDouble(txt_user_poer_requirement.Text);
              //syscapacity = FRM_Settings.txttot.Text;
                dblFahrenheit = double.Parse(lbl_result_climate.Text);
                dblCelsius = dblFahrenheit - 273.15;
                double manual_error = Convert.ToDouble(lbl_result_errorlevel.Text);
                double manual_error_substraction = (manual_error * dblCelsius) / 100;
                double sun_const_after_errorlevel = sun_const - manual_error_substraction;
                double temp_diff = 35 - dblCelsius;
                double sun_const_after_subtrac_temp = sun_const_after_errorlevel - temp_diff;
                double temp = (24 / pi_value);
                double tan_temp = (pi_value / 180) * (Math.Tan(Convert.ToDouble((-1) * angle_summer)));
                double tan_temp2 = (pi_value / 180) * Math.Tan(Convert.ToDouble(lbl_result_latitude.Text));
                double temp_mulit = tan_temp * tan_temp2;
                double cos_temp = Math.Acos(temp_mulit);
                double length_day = temp * cos_temp;
                double energy = sun_const_after_subtrac_temp * (3600 * length_day / pi_value);
                double parellel = Math.Round((Convert.ToDouble(txt_current.Text) / modulrating),2)*100;
                double serial = Math.Round(Convert.ToDouble(txt_voltage.Text) / cell_voltage);
                double noofmodules = Convert.ToDouble(txt_user_poer_requirement.Text) / (modulrating * length_day);
                noofmodules = Math.Floor(noofmodules * Convert.ToDouble(FRM_Search_BaseCell.modul_area) * Convert.ToDouble(FRM_Search_BaseCell.modul_area));
                if (lbl_result_climatecondition.Text == "sunny")
                {
                    lbl_result_angle.Text = Convert.ToString(Math.Abs(Math.Round(angle_summer, 2)));
                }
                else if (lbl_result_climatecondition.Text == "winter")
                {
                    lbl_result_angle.Text = Convert.ToString(Math.Abs(Math.Round(angle_winter, 2)));
                }
                else if (lbl_result_climatecondition.Text == "spring")
                {
                    lbl_result_angle.Text = Convert.ToString(Math.Abs(Math.Round(angle_spring, 2)));
                }
                else if (lbl_result_climatecondition.Text == "Fall")
                {
                    lbl_result_angle.Text = Convert.ToString(Math.Abs(Math.Round(angle_fall, 2)));
                }
                else{
                    lbl_result_angle.Text = "38";
                }
                
               
                lbl_result_no_modules_parallel.Text = Convert.ToString(parellel);
                lbl_result_no_modules_serial.Text = Convert.ToString(serial);
                lbl_resultlength.Text = Convert.ToString(Math.Round(length_day, 2));
                //lbl_result_no_modules_serial.Text =  Convert.ToString(Math.Round((Convert.ToDouble(txt_voltage.Text) / modulrating),2)*100);
                //lbl_result_no_modules.Text = Convert.ToString(noofmodules);
                //lbl_result_angle.Text = Convert.ToString(Math.Round(angle, 2));
                //lbl_result_no_modules_serial.Text = Convert.ToString(Math.Round( noofmodules /parellel));
                double noofpanels = Convert.ToDouble(parellel*serial);
                double nopanelsadd = Convert.ToDouble(parellel + serial);

                lbl_result_no_modules.Text = Convert.ToString(noofpanels);


                label11.Text = Convert.ToString(Math.Round(syscapacity *length_day* modulrating * 0.75)/10000);
                //label_result_energy.Text = Convert.ToString(Math.Round( modulrating*noofpanels/(syscapacity),2));
                lbl_result_totalCost.Text = Convert.ToString(Math.Round((Convert.ToDouble(FRM_Search_BaseCell.cost) * noofmodules), 2));
          
                errorProvider1.Dispose();

            }
            catch (Exception ex)
            {
                errorProvider1.Dispose();
                if (txt_user_poer_requirement.Text == "" && txt_voltage.Text == "" && txt_current.Text == "")
                {

                    errorProvider1.SetError(txt_user_poer_requirement, "Please enter the Required Power");
                    errorProvider1.SetError(txt_voltage, "Please enter Voltage");
                    errorProvider1.SetError(txt_current, "Please enter Current");
                }
                else if (txt_user_poer_requirement.Text == "")
                {

                    errorProvider1.SetError(txt_user_poer_requirement, "Please enter the Required Power");
                }
                else if (txt_voltage.Text == "")
                {

                    errorProvider1.SetError(txt_voltage, "Please enter Voltage");
                }
                else if (txt_current.Text == "")
                {

                    errorProvider1.SetError(txt_current, "Please enter Current");
                }
                else
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }
        private void linklbl_home_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                try
                {
                    FRM_Home frm_obj = (FRM_Home)Application.OpenForms["FRM_Home"];
                    frm_obj.shw();

                    //FRM_Home home_frm_obj = new FRM_Home();
                    //home_frm_obj.Show();
                    //this.Dispose();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
                //FRM_Home home_frm_obj = new FRM_Home();
                //home_frm_obj.Show();
                //this.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        private void FRM_SOLAR_POWER_SIMULATE_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                //FRM_Home frm_obj = (FRM_Home)this.MdiParent;
                //frm_obj.g2.Show(); frm_obj.m1.Show(); frm_obj.g1.Show(); 
              
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        private void timer_timnpics_Tick(object sender, EventArgs e)
        {
            try
            {
                DateTime time = System.DateTime.Now;
                lbl_time.Text = "Time :" + Convert.ToString(time.ToLongTimeString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        } 
        internal  void FRM_SOLAR_POWER_SIMULATE_Load(object sender, EventArgs e)
        {
            try
            {
                FRM_Home f = (FRM_Home)Application.OpenForms["FRM_Home"];
                f.groupBox2.Hide(); f.monthCalendar1.Hide() ; f.groupBox1.Hide();


                //BTN_BASECELL_CONFIG.Location = new Point(42, 32);
                //button1.Location = new Point(42, 66);
                //BTN_LOCATIONFETCHER.Location = new Point(42, 88);
                //BTN_Climatefetcher.Location = new Point(42, 151);
                //BTN_mannualcorrection.Location = new Point(42, 206);
                //BTN_Calculate.Location = new Point(42, 260);
                //BTN_CLEAR.Location = new Point(42, 313);
                //if (Convert.ToInt32(FRM_Login.authentication) == 2)
                //{
                //    //BTN_CONFIG.Visible = false;
                //    BTN_BASECELL_CONFIG.Location = new Point(42, 32);
                //    BTN_LOCATIONFETCHER.Location = new Point(42, 88);
                //    BTN_Climatefetcher.Location = new Point(42, 151);
                //    BTN_mannualcorrection.Location = new Point(42, 206);
                //    BTN_Calculate.Location = new Point(42, 260);
                //    BTN_CLEAR.Location = new Point(42, 313);
                //}
                timer_timnpics.Enabled = true;
                lbl_loginname.Text ="User Name :"+ FRM_MainHome.username;

                //lbl_result_cost_module.Text = FRM_Search_BaseCell.cost;
                //lbl_result_typematerial.Text = FRM_Search_BaseCell.material;
                //lbl_result_voltage.Text = FRM_Search_BaseCell.voltage;
                //lbl_result_modultarea.Text = FRM_Search_BaseCell.area;
                //lbl_result_modulerating.Text = FRM_Search_BaseCell.modulerating;

                //lbl_result_longi.Text = selected_lonid;
                //lbl_result_location.Text = selected_place;
                //lbl_result_latitude.Text = selected_latid;
                //lbl_result_climate.Text = selected_climate_info;
                //lbl_result_humid.Text = humid_selected;
                //lbl_result_pressure.Text = presure_selected;

                //lbl_result_climatecondition.Text = FRM_MANUAL_CORRECTIN.selected_climate_condition;
                //lbl_result_errorlevel.Text = FRM_MANUAL_CORRECTIN.selected_error;


                //var frm = Application.OpenForms.Cast<Form>().Where(x => x.Name == "FRM_Settings").FirstOrDefault();
                //if (null != frm)
                //{
                //    FRM_Settings frmSet= (FRM_Settings)frm;
                //    txt_user_poer_requirement.Text = frmSet.txttot.Text; 
                //  //  frm.Close();
                //  //  frm = null;
                //}



                //txt_user_poer_requirement.Text = GlobalParams.TotalPower;

                //FRM_Settings frm = new FRM_Settings();
                //txt_user_poer_requirement.Text = frm.txttot.Text;





                

            }
            catch (Exception )
            {
                //MessageBox.Show(ex.ToString());
            }
        }
        private void link_lbl_logout_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                    this.Close();      //Application.Exit();   
                    FRM_Home frm_obj = (FRM_Home)Application.OpenForms["FRM_Home"];
                    frm_obj.Close();
                    
            }
            catch (Exception )
            {
              //  MessageBox.Show(ex.ToString());
            }
        }
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                FRM_Home frm_obj = (FRM_Home)Application.OpenForms["FRM_Home"];
                frm_obj.g1.Show(); frm_obj.g2.Show(); frm_obj.m1.Show();
                //FRM_Home home_frm_obj = new FRM_Home();
                //home_frm_obj.Show();
                this.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start("Firefox.exe");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void benifitsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                FRM_Benifits FRM_Benifitsbenifit_obj = new FRM_Benifits();
                FRM_Benifitsbenifit_obj.Show(); 
               // this.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void aboutProjectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                FRM_ABOUT_PROJECT project_obj = new FRM_ABOUT_PROJECT();
                project_obj.Show();
                //this.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        

        private void lbl_result_typematerial_Click(object sender, EventArgs e)
        {
            try
            {
                FRM_Search_BaseCell base_config_obj = new FRM_Search_BaseCell();
                base_config_obj.Show();
                base_config_obj.MdiParent = this.MdiParent;

                //this.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void lbl_result_modultarea_Click(object sender, EventArgs e)
        {
            try
            {
                FRM_Search_BaseCell base_config_obj = new FRM_Search_BaseCell();
                base_config_obj.Show();
                //this.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void lbl_result_cost_module_Click(object sender, EventArgs e)
        {
            try
            {
                FRM_Search_BaseCell base_config_obj = new FRM_Search_BaseCell();
                base_config_obj.Show();
                //this.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void lbl_result_voltage_Click(object sender, EventArgs e)
        {
            try
            {
                FRM_Search_BaseCell base_config_obj = new FRM_Search_BaseCell();
                base_config_obj.Show();
                //this.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void lbl_result_location_Click(object sender, EventArgs e)
        {
            try
            {
                FRM_CLIMATEFETCHER climate_obj = new FRM_CLIMATEFETCHER();
                climate_obj.Show();
                climate_obj.MdiParent = this.MdiParent;
                //this.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void lbl_result_latitude_Click(object sender, EventArgs e)
        {
            try
            {
                FRM_CLIMATEFETCHER climate_obj = new FRM_CLIMATEFETCHER();
                climate_obj.Show();
               // this.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void lbl_result_longi_Click(object sender, EventArgs e)
        {
            try
            {
                FRM_CLIMATEFETCHER climate_obj = new FRM_CLIMATEFETCHER();
                climate_obj.Show();
               // this.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void lbl_result_climate_Click(object sender, EventArgs e)
        {
            try
            {
                FRM_CLIMATEFETCHER climate_obj = new FRM_CLIMATEFETCHER();
                climate_obj.Show();
               // this.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void lbl_result_climatecondition_Click(object sender, EventArgs e)
        {
            try
            {
                FRM_MANUAL_CORRECTIN manual_obj = new FRM_MANUAL_CORRECTIN();
                manual_obj.Show();
                manual_obj.MdiParent = this.MdiParent;
                //this.Hide();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void lbl_result_errorlevel_Click(object sender, EventArgs e)
        {
            try
            {
                FRM_MANUAL_CORRECTIN manual_obj = new FRM_MANUAL_CORRECTIN();
                manual_obj.Show();
               // this.Hide();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        private void lbl_result_temprature(object sender, EventArgs e)
        {
            try
            {
                FRM_CLIMATEFETCHER climate_obj = new FRM_CLIMATEFETCHER();
                climate_obj.Show();
               // this.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        private void BTN_CLEAR_Click(object sender, EventArgs e)
        {
            try
            {
                stat = 1;
                lbl_result_angle.Text = "";
                lbl_result_climate.Text = "00.00";
                lbl_result_climatecondition.Text = "--Select--";
                lbl_result_cost_module.Text = "00.00";
                lbl_result_errorlevel.Text = "00.00";
                lbl_result_latitude.Text = "00.00";
                lbl_result_location.Text = "--Select--";
                lbl_result_longi.Text = "00.00";
                lbl_result_modultarea.Text = "--Select--";
                lbl_result_no_modules.Text = "";
                lbl_result_no_modules_parallel.Text = "";
                lbl_result_no_modules_serial.Text = "";
               // lbl_result_total_no_of_modules.Text = "";
                lbl_result_totalCost.Text = "";
                label11.Text = "";
                lbl_result_typematerial.Text = "--Select--";
                lbl_result_voltage.Text = "00.00";
                lbl_result_pressure.Text = "00.00";
                lbl_result_humid.Text = "00.00";
                lbl_resultlength.Text = "";
                txt_current.Text = "";
                txt_user_poer_requirement.Text = "";
                txt_voltage.Text = "";
               lbl_result_modulerating.Text = "00.00";
               //lbl_result_location.Text = "";
               FRM_CLIMATEFETCHER f = new FRM_CLIMATEFETCHER();
               //f.txt_rich_climate_info.Clear();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void groupBox5_Enter(object sender, EventArgs e)
        {

        }

        private void lbl_result_pressure_Click(object sender, EventArgs e)
        {

        }

        private void groupBox4_Enter(object sender, EventArgs e)
        {

        }

        private void txt_user_poer_requirement_TextChanged(object sender, EventArgs e)
        {

        }

        private void txt_user_poer_requirement_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar)&& !char.IsDigit(e.KeyChar)&& e.KeyChar != '.')
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if (e.KeyChar == '.'&& (sender as TextBox).Text.IndexOf('.') > -1)
            {
                e.Handled = true;
            }
        }

        private void FRM_SOLAR_POWER_SIMULATE_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                FRM_Home frm_obj = (FRM_Home)this.MdiParent;
                frm_obj.g2.Visible = true; frm_obj.m1.Visible = true; frm_obj.g1.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void txt_voltage_Validated(object sender, EventArgs e)
        {
            if (txt_voltage.Text!=""&& double.Parse(txt_voltage.Text) > 0 && double.Parse(txt_voltage.Text) < 100)
            {
                MessageBox.Show("Please enter a valid voltage.","Warning",MessageBoxButtons.OK,MessageBoxIcon.Warning);
                txt_voltage.Text = "";
                txt_voltage.Focus();
                return;
            }
        }

        private void txt_current_Validated(object sender, EventArgs e)
        {
            if (txt_current.Text!="" && double.Parse(txt_current.Text) > 0 && double.Parse(txt_current.Text) > 100)
            {
                MessageBox.Show("Please enter a valid current.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txt_current.Text = "";
                txt_current.Focus();
                return;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                FRM_Settings pwr = new FRM_Settings();
                pwr.Show();
                pwr.MdiParent = this.MdiParent;
                // this.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void txt_user_poer_requirement_Click(object sender, EventArgs e)
        {

        }

        private void lbl_USERPOWERREQUIREMENT_Click(object sender, EventArgs e)
        {

        }


        
        private void button2_Click(object sender, EventArgs e)
        {

            


            ////CaptureScreen();
            ////printDocument1.Print();
            //Panel panel = new Panel();
            //this.Controls.Add(panel);
            //Graphics grp = panel.CreateGraphics();
            //Size formSize = this.ClientSize;
            //bitmap = new Bitmap(formSize.Width, formSize.Height, grp);
            //grp = Graphics.FromImage(bitmap);
            //Point panelLocation = PointToScreen(panel.Location);
            //grp.CopyFromScreen(panelLocation.X, panelLocation.Y, 0, 0, formSize);
            //printPreviewDialog1.Document = printDocument1;
            //printPreviewDialog1.PrintPreviewControl.Zoom = 1;
            //printPreviewDialog1.ShowDialog();
           

        }


        //Bitmap bitmap;
        //private void CaptureScreen()
        //{
        //    Graphics myGraphics = this.CreateGraphics();
        //    Size s = this.Size;
        //    bitmap = new Bitmap(s.Width, s.Height, myGraphics);
        //    Graphics memoryGraphics = Graphics.FromImage(bitmap);
        //    memoryGraphics.CopyFromScreen(this.Location.X, this.Location.Y, 0, 0, s);
        //}

        

        private void printPreviewDialog1_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }



        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            
            //e.Graphics.DrawImage(bitmap, 0, 0);
        }



    }
}
