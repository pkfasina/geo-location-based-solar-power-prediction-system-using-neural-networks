﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace solarpanel
{
    class ValidationClass1
    {
        public bool id(string id)
        {
            if (!Regex.IsMatch(id, @"^[0-9]{3}$"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool name(string name)
        {
            if (!Regex.IsMatch(name, @"^[a-zA-Z.\s]{1,40}$"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool date(string dates)
        {
            if (!Regex.IsMatch(dates, @"^[0-9/]{8,10}$"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool email(string email)
        {
            if (!Regex.IsMatch(email, @"^([0-9a-zA-Z]([-\.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$"))
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public bool password(string password)
        {
            if(!Regex.IsMatch(password, @"^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6-10}$"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool other(string other)
        {
            if (!Regex.IsMatch(other, @"^[a-zA-Z\d.\s]{1,20}$"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool address(string address)
        {
            if (!Regex.IsMatch(address, @"^[a-zA-Z\d./  \s\w]{1,100}$"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
