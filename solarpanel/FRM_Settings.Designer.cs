﻿namespace solarpanel
{
    partial class FRM_Settings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FRM_Settings));
            this.label1 = new System.Windows.Forms.Label();
            this.dgvpower = new System.Windows.Forms.DataGridView();
            this.Appliances = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ApproximateLoad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NoofEquipments = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AverageHours = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ApproximateUnits = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label2 = new System.Windows.Forms.Label();
            this.txttot = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnadd = new System.Windows.Forms.Button();
            this.txtload = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtappliance = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtunit = new System.Windows.Forms.TextBox();
            this.comboappl = new System.Windows.Forms.ComboBox();
            this.comboload = new System.Windows.Forms.ComboBox();
            this.combohours = new System.Windows.Forms.ComboBox();
            this.btnrowadd = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.txtno = new wmgCMS.WaterMarkTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvpower)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label1.Location = new System.Drawing.Point(401, 39);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(310, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "Calculation of Total Power Required";
            // 
            // dgvpower
            // 
            this.dgvpower.AllowUserToAddRows = false;
            this.dgvpower.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.dgvpower.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvpower.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvpower.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dgvpower.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvpower.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvpower.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvpower.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Appliances,
            this.ApproximateLoad,
            this.NoofEquipments,
            this.AverageHours,
            this.ApproximateUnits});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.InactiveCaption;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvpower.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvpower.Location = new System.Drawing.Point(456, 142);
            this.dgvpower.Margin = new System.Windows.Forms.Padding(4);
            this.dgvpower.Name = "dgvpower";
            this.dgvpower.ReadOnly = true;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.ActiveCaption;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvpower.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvpower.RowHeadersVisible = false;
            this.dgvpower.RowHeadersWidth = 51;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.dgvpower.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvpower.Size = new System.Drawing.Size(849, 373);
            this.dgvpower.TabIndex = 6;
            this.dgvpower.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvpower_CellClick);
            this.dgvpower.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvpower_CellContentClick);
            // 
            // Appliances
            // 
            this.Appliances.HeaderText = "Appliances";
            this.Appliances.MinimumWidth = 6;
            this.Appliances.Name = "Appliances";
            this.Appliances.ReadOnly = true;
            this.Appliances.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // ApproximateLoad
            // 
            this.ApproximateLoad.HeaderText = "ApproximateLoad";
            this.ApproximateLoad.MinimumWidth = 6;
            this.ApproximateLoad.Name = "ApproximateLoad";
            this.ApproximateLoad.ReadOnly = true;
            this.ApproximateLoad.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // NoofEquipments
            // 
            this.NoofEquipments.HeaderText = "NoofEquipments";
            this.NoofEquipments.MinimumWidth = 6;
            this.NoofEquipments.Name = "NoofEquipments";
            this.NoofEquipments.ReadOnly = true;
            // 
            // AverageHours
            // 
            this.AverageHours.HeaderText = "AverageHours";
            this.AverageHours.MinimumWidth = 6;
            this.AverageHours.Name = "AverageHours";
            this.AverageHours.ReadOnly = true;
            this.AverageHours.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // ApproximateUnits
            // 
            this.ApproximateUnits.HeaderText = "ApproximateUnits";
            this.ApproximateUnits.MinimumWidth = 6;
            this.ApproximateUnits.Name = "ApproximateUnits";
            this.ApproximateUnits.ReadOnly = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(625, 539);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(232, 24);
            this.label2.TabIndex = 3;
            this.label2.Text = "Total Power Consumption:";
            // 
            // txttot
            // 
            this.txttot.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttot.Location = new System.Drawing.Point(976, 541);
            this.txttot.Margin = new System.Windows.Forms.Padding(4);
            this.txttot.Name = "txttot";
            this.txttot.Size = new System.Drawing.Size(199, 23);
            this.txttot.TabIndex = 7;
            this.txttot.Text = "0";
            this.txttot.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txttot.TextChanged += new System.EventHandler(this.txttot_TextChanged);
            this.txttot.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox1_KeyPress);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.groupBox1.Controls.Add(this.btnadd);
            this.groupBox1.Controls.Add(this.txtload);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtappliance);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(13, 126);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(409, 313);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Add New";
            // 
            // btnadd
            // 
            this.btnadd.Location = new System.Drawing.Point(132, 220);
            this.btnadd.Margin = new System.Windows.Forms.Padding(4);
            this.btnadd.Name = "btnadd";
            this.btnadd.Size = new System.Drawing.Size(151, 28);
            this.btnadd.TabIndex = 10;
            this.btnadd.Text = "Add New";
            this.btnadd.UseVisualStyleBackColor = true;
            this.btnadd.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtload
            // 
            this.txtload.Location = new System.Drawing.Point(173, 142);
            this.txtload.Margin = new System.Windows.Forms.Padding(4);
            this.txtload.Name = "txtload";
            this.txtload.Size = new System.Drawing.Size(199, 22);
            this.txtload.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(11, 143);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(119, 18);
            this.label4.TabIndex = 8;
            this.label4.Text = "Approx. Load/ Hr";
            // 
            // txtappliance
            // 
            this.txtappliance.Location = new System.Drawing.Point(173, 96);
            this.txtappliance.Margin = new System.Windows.Forms.Padding(4);
            this.txtappliance.Name = "txtappliance";
            this.txtappliance.Size = new System.Drawing.Size(199, 22);
            this.txtappliance.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(11, 97);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 18);
            this.label3.TabIndex = 6;
            this.label3.Text = "Appliance";
            // 
            // txtunit
            // 
            this.txtunit.Location = new System.Drawing.Point(1079, 96);
            this.txtunit.Margin = new System.Windows.Forms.Padding(4);
            this.txtunit.Name = "txtunit";
            this.txtunit.Size = new System.Drawing.Size(143, 22);
            this.txtunit.TabIndex = 4;
            this.txtunit.TextChanged += new System.EventHandler(this.txtunit_TextChanged);
            this.txtunit.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.comboappl_KeyPress);
            // 
            // comboappl
            // 
            this.comboappl.FormattingEnabled = true;
            this.comboappl.Location = new System.Drawing.Point(456, 96);
            this.comboappl.Margin = new System.Windows.Forms.Padding(4);
            this.comboappl.Name = "comboappl";
            this.comboappl.Size = new System.Drawing.Size(165, 24);
            this.comboappl.TabIndex = 0;
            this.comboappl.Text = "Select";
            this.comboappl.SelectedIndexChanged += new System.EventHandler(this.comboappl_SelectedIndexChanged);
            this.comboappl.Click += new System.EventHandler(this.comboappl_Click);
            this.comboappl.KeyDown += new System.Windows.Forms.KeyEventHandler(this.comboappl_KeyDown);
            this.comboappl.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.comboappl_KeyPress);
            // 
            // comboload
            // 
            this.comboload.FormattingEnabled = true;
            this.comboload.Location = new System.Drawing.Point(629, 95);
            this.comboload.Margin = new System.Windows.Forms.Padding(4);
            this.comboload.Name = "comboload";
            this.comboload.Size = new System.Drawing.Size(137, 24);
            this.comboload.TabIndex = 1;
            this.comboload.Text = "Select";
            this.comboload.SelectedIndexChanged += new System.EventHandler(this.comboload_SelectedIndexChanged);
            this.comboload.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.comboappl_KeyPress);
            // 
            // combohours
            // 
            this.combohours.FormattingEnabled = true;
            this.combohours.Location = new System.Drawing.Point(943, 95);
            this.combohours.Margin = new System.Windows.Forms.Padding(4);
            this.combohours.Name = "combohours";
            this.combohours.Size = new System.Drawing.Size(128, 24);
            this.combohours.TabIndex = 3;
            this.combohours.Text = "Select";
            this.combohours.SelectedIndexChanged += new System.EventHandler(this.combohours_SelectedIndexChanged);
            this.combohours.TextChanged += new System.EventHandler(this.combohours_TextChanged);
            this.combohours.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.comboappl_KeyPress);
            // 
            // btnrowadd
            // 
            this.btnrowadd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnrowadd.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnrowadd.Location = new System.Drawing.Point(1230, 95);
            this.btnrowadd.Margin = new System.Windows.Forms.Padding(4);
            this.btnrowadd.Name = "btnrowadd";
            this.btnrowadd.Size = new System.Drawing.Size(75, 25);
            this.btnrowadd.TabIndex = 5;
            this.btnrowadd.Text = "+ Add";
            this.btnrowadd.UseVisualStyleBackColor = false;
            this.btnrowadd.Click += new System.EventHandler(this.btnrowadd_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 1);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(176, 97);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 19;
            this.pictureBox1.TabStop = false;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.button1.Location = new System.Drawing.Point(405, 539);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(95, 28);
            this.button1.TabIndex = 20;
            this.button1.Text = "CLEAR";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.button2.Location = new System.Drawing.Point(1201, 532);
            this.button2.Margin = new System.Windows.Forms.Padding(4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(66, 35);
            this.button2.TabIndex = 21;
            this.button2.Text = "OK";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // txtno
            // 
            this.txtno.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtno.Location = new System.Drawing.Point(774, 95);
            this.txtno.Margin = new System.Windows.Forms.Padding(4);
            this.txtno.Name = "txtno";
            this.txtno.Size = new System.Drawing.Size(161, 23);
            this.txtno.TabIndex = 2;
            this.txtno.WaterMarkColor = System.Drawing.Color.Gray;
            this.txtno.WaterMarkText = "Enter No of Eqipments";
            this.txtno.TextChanged += new System.EventHandler(this.txtno_TextChanged);
            // 
            // FRM_Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1370, 607);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.txtno);
            this.Controls.Add(this.btnrowadd);
            this.Controls.Add(this.combohours);
            this.Controls.Add(this.comboload);
            this.Controls.Add(this.comboappl);
            this.Controls.Add(this.txtunit);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.txttot);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dgvpower);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FRM_Settings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Power Requirement";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FRM_Settings_FormClosing);
            this.Load += new System.EventHandler(this.FRM_Settings_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvpower)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgvpower;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnadd;
        private System.Windows.Forms.TextBox txtload;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtappliance;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Appliances;
        private System.Windows.Forms.DataGridViewTextBoxColumn ApproximateLoad;
        private System.Windows.Forms.DataGridViewTextBoxColumn NoofEquipments;
        private System.Windows.Forms.DataGridViewTextBoxColumn AverageHours;
        private System.Windows.Forms.DataGridViewTextBoxColumn ApproximateUnits;
        private System.Windows.Forms.TextBox txtunit;
        private System.Windows.Forms.ComboBox comboappl;
        private System.Windows.Forms.ComboBox comboload;
        private System.Windows.Forms.ComboBox combohours;
        private System.Windows.Forms.Button btnrowadd;
        private wmgCMS.WaterMarkTextBox txtno;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button button1;
        public System.Windows.Forms.TextBox txttot;
        private System.Windows.Forms.Button button2;
    }
}