﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace solarpanel
{
    public partial class FRM_ABOUT_PROJECT : Form
    {
        public FRM_ABOUT_PROJECT()
        {
            InitializeComponent();
        }

        private void BTN_Cancel_Click(object sender, EventArgs e)
        {
            try
            {
                //FRM_SOLAR_POWER_SIMULATE frm_simulate_obj = new FRM_SOLAR_POWER_SIMULATE();
                //frm_simulate_obj.Show();
                this.Hide();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            FRM_MainHome f = new FRM_MainHome();
            f.Show();
        }

        private void FRM_ABOUT_PROJECT_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                //FRM_SOLAR_POWER_SIMULATE frm_simulate_obj = new FRM_SOLAR_POWER_SIMULATE();
                //frm_simulate_obj.Show();
                this.Hide();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void lbl_loginName_Click(object sender, EventArgs e)
        {

        }

        private void FRM_ABOUT_PROJECT_Load(object sender, EventArgs e)
        {

        }

        private void FRM_ABOUT_PROJECT_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                //this.Close();
                //FRM_Login f = new FRM_Login(); f.Show();
                //FRM_MainHome frm_obj = (FRM_MainHome)Application.OpenForms["FRM_MainHome"];
                //frm_obj.Show();
                FRM_MainHome fm = new FRM_MainHome();
                fm.Show();
            }
            catch (Exception) { }
            //if (MessageBox.Show("Exit or No?", "Forgot Passwort", MessageBoxButtons.YesNo,
            //          MessageBoxIcon.Information) == DialogResult.No)
            //{
            //    e.Cancel = true;
            //}
            //else
            //{
            FRM_MainHome f = new FRM_MainHome();
            f.Show();
            //}
        }
    


}
}
