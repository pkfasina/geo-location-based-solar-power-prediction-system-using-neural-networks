﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace solarpanel
{
    public partial class FRM_Benifits : Form
    {
        public FRM_Benifits()
        {
            InitializeComponent();
        }

        private void BTN_Cancel_Click(object sender, EventArgs e)
        {
            try
            {
                //FRM_SOLAR_POWER_SIMULATE frm_simulate_obj = new FRM_SOLAR_POWER_SIMULATE();
                //frm_simulate_obj.Show();
                this.Hide();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void FRM_Benifits_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                //FRM_SOLAR_POWER_SIMULATE frm_simulate_obj = new FRM_SOLAR_POWER_SIMULATE();
                //frm_simulate_obj.Show();
                this.Hide();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
    }
}
