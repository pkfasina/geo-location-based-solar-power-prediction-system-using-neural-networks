﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.OleDb;

namespace solarpanel
{
    public partial class FRM_Registration : Form
    {
        public FRM_Registration()
        {
            InitializeComponent();
        }
        sqlconnectionClass1 sqlconnection_obj = new sqlconnectionClass1();
        //public string username = FRM_Login.username;
        ValidationClass1 validation_obj = new ValidationClass1();
        public int Validation_Method()
        {
            try
            {
                int count = 0;
                bool name, email, address,password;
                name = validation_obj.name(txt_User_Name.Text);
                if (name == true)
                {
                    count++;
                    lbl_Validation_User_Name.Visible = true;
                    lbl_Validation_Notification.Visible = true;
                }
                email = validation_obj.email(txt_email.Text);
                if (email == true)
                {
                    count++;
                    lbl_Validation_email.Visible = true;
                    lbl_Validation_Notification.Visible = true;
                }
                address = validation_obj.address(richtxt_address .Text);
                if (address == true)
                {
                    count++;
                    lbl_Validation_address.Visible = true;
                    lbl_Validation_Notification.Visible = true;
                }
                password = validation_obj.password(txt_password.Text);
                if(password==true)
                {
                    count++;
                    lbl_Validation_password.Visible = true;
                    lbl_Validation_Notification.Visible = true;
                }

                //usertype = validation_obj.other(combo_Privilege.Text);
                //if (usertype == true)
                //{
                //    count++;
                //    lbl_Validation_Privilege.Visible = true;
                //}
                if (combo_date.SelectedItem == null || combo_MONTH.SelectedItem == null || combo_YEAR.SelectedItem == null)
                {
                    count++;
                    //lbl_Validation_Date.Visible = true;
                    lbl_Validation_Notification.Visible = true;
                }
                if (radio_female.Checked == false && radio_Male.Checked == false)
                {
                    count++;
                    //lbl_Validation_gender.Visible = true;
                    lbl_Validation_Notification.Visible = true;
                }
                if (txt_password.Text != txt_confirmpassword.Text || txt_confirmpassword.Text == "" || txt_password.Text == "")
                {
                    count++;
                    label1.Visible = true;
                }
                return count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return (0);
            }
        }
        private void Btn_Insert_Click(object sender, EventArgs e)
        {
            try
            {
                OleDbConnection con = sqlconnection_obj.Database_Connection_Method();
                /*Code For Disable Validation Lables*/
                lbl_Validation_address.Visible = false;
                //lbl_Validation_Date.Visible = false;
                lbl_Validation_email.Visible = false;
                label1.Visible = false;

                //lbl_Validation_gender.Visible = false;
                //lbl_Validation_Name.Visible = false;
                lbl_Validation_Notification.Visible = false;
                lbl_Validation_password.Visible = false;
                //lbl_Validation_Privilege.Visible = false;
                lbl_Validation_status.Visible = false;
                lbl_Validation_User_Name.Visible = false;

                /*Code for Validation Check*/
                int validation_variable = Validation_Method();
                /*Code For is user name exist in database*/
                int count = 0;
                con.Open();
                OleDbCommand sqlcom = new OleDbCommand("select U_NAME from User_Registration", con);
                OleDbDataReader sql_reader;
                sql_reader = sqlcom.ExecuteReader();
                while (sql_reader.Read())
                {
                    if (sql_reader[0].ToString() == txt_User_Name.Text || txt_User_Name.Text == "")
                    {
                        count++;
                        lbl_Validation_User_Name.Visible = true;
                    }
                }
                con.Close();
                if (validation_variable == 0&&count==0)   //all correct
                {
                    con.Open();
                    string gender, DOB;
                    if (radio_female.Checked == true){gender = "F";}
                    else { gender = "M"; }
                    DOB = combo_MONTH.SelectedItem.ToString() + "/" + combo_date.SelectedItem.ToString() + "/" + combo_YEAR.SelectedItem.ToString();




                    /*string insert_query = "insert into User_Registration"
                                                    + " (U_NAME,PASSWORD,PRIVILEGE,NAME,GENDER,DOB,EMAILADDRESS,ADDRESS,ACTIVATIONDATE,SOFTWARESTATUS) " 
                                                    + " values('"+txt_User_Name.Text+"','"+txt_password.Text+"','"+combo_Privilege.SelectedItem+"','"+txt_name.Text+"','"+gender+"','"+DOB+"','"+txt_email.Text+"','"+richtxt_address.Text+"','"+System.DateTime.Now+"','E')";
                    OleDbCommand sqlcmd_insert = new OleDbCommand(insert_query, con);
                    sqlcmd_insert.ExecuteNonQuery();*/


                    string insert_query = "insert into [User_Registration]"
                                                    + " ([U_NAME],[PASSWORD],[NAME],[GENDER],[DOB],[EMAILADDRESS],[ADDRESS],[ACTIVATIONDATE],[SOFTWARESTATUS]) "
                                                    + " values('" + txt_User_Name.Text + "','" + txt_password.Text + "','" + txt_name.Text + "','" + gender + "','" + DOB + "','" + txt_email.Text + "','" + richtxt_address.Text + "','" + System.DateTime.Now + "','E')";
                    OleDbCommand sqlcmd_insert = new OleDbCommand(insert_query, con);
                    sqlcmd_insert.ExecuteNonQuery();




                    con.Close();
                    /*Code For inserting uasername And Password to loginTable*/
                    con.Open();
                    
                    string insert_query_login = "insert into [Login] ([USER_NAME],[PASSWORD]) values ('" + txt_User_Name.Text + "','" + txt_password.Text + "')";
                    OleDbCommand sqlcmd_insert_login = new OleDbCommand(insert_query_login, con);
                    sqlcmd_insert_login.ExecuteNonQuery();
                    con.Close();
                    txt_confirmpassword.Text = "";
                    txt_email.Text = "";
                    txt_name.Text = "";
                    txt_password.Text = "";
                    txt_User_Name.Text = "";
                    combo_YEAR.Text = "YYYY";
                    combo_MONTH.Text = "MM";
                    //combo_Privilege.Text = "  --- Select Privilege ---";
                    combo_date.Text = "DD";
                    radio_female.Checked = false;
                    radio_Male.Checked = false;
                    richtxt_address.Text = "";
                    MessageBox.Show("Datas are successfully Inserted");





                    //this.Hide();
                    //FRM_Home frm_reg_obj = new FRM_Home();
                    //frm_reg_obj.Show();
                }
            }
            catch (Exception ex)
            {
              MessageBox.Show(ex.ToString());
            }

        }

        private void Btn_Update_Click(object sender, EventArgs e)
        {
            try
            {
                OleDbConnection con = sqlconnection_obj.Database_Connection_Method();
                string DOB, Gender, status;
                int count = 0,validation_variable;
                /*Code For Disable Validation Lables*/
                lbl_Validation_address.Visible = false;
                //lbl_Validation_Date.Visible = false;
                lbl_Validation_email.Visible = false;
                //lbl_Validation_gender.Visible = false;
               // lbl_Validation_Name.Visible = false;
                lbl_Validation_Notification.Visible = false;
                label1.Visible = false;

                lbl_Validation_password.Visible = false;
                //lbl_Validation_Privilege.Visible = false;
                lbl_Validation_status.Visible = false;
                lbl_Validation_User_Name.Visible = false;
                if (radio_Male.Checked == true)
                {
                    Gender = "M";
                }
                else
                {
                    Gender = "F";
                }
                if (checkB_Enable.Checked == true)
                {
                    status = "E";
                    checkB_disable.Checked = false;
                }
                else
                {
                    status = "D";
                    checkB_Enable.Checked = false;
                }
              /*Code For Check existance of UserNAme*/
                con.Open();
                OleDbCommand sqlcmd = new OleDbCommand("select [U_NAME] from [User_Registration] ", con);
                OleDbDataReader sql_reader;
                sql_reader = sqlcmd.ExecuteReader();
                while (sql_reader.Read())
                {
                    if (sql_reader[0].ToString() == txt_User_Name.Text)
                    {
                        count++;
                        //lbl_Validation_User_Name.Visible = true;
                    }

                }
                con.Close();
                if (count != 0)
                {
                    con.Open();
                    OleDbCommand sqlcmd_existance = new OleDbCommand("select [U_NAME] from [User_Registration] where U_ID='" + comboBox_U_ID.SelectedItem + "' ", con);
                    OleDbDataReader sql_reader_Check;
                    sql_reader_Check = sqlcmd_existance.ExecuteReader();
                    while (sql_reader_Check.Read())
                    {
                        if (sql_reader_Check[0].ToString() != txt_User_Name.Text)
                        {
                            // count++;
                            lbl_Validation_User_Name.Visible = true;
                        }
                        else { count = 0; }

                    }
                    con.Close();
                }
                else 
                {
                    if (txt_User_Name.Text == "")
                    {
                        count++;
                    }
                }

                validation_variable = Validation_Method();
                if(validation_variable==0&&count==0)
                {                    
                    con.Open();
                    DOB = combo_MONTH.SelectedItem.ToString() + "/" + combo_date.SelectedItem.ToString() + "/" + combo_YEAR.SelectedItem.ToString();
                    string updation_query="UPDATE User_Registration SET [U_NAME]='" + txt_User_Name.Text + "',"+
                                                       "[PASSWORD]='" + txt_password.Text + "',"+
                                                       "[NAME]='" + txt_name.Text + "',[GENDER]= '" + Gender + "',[DOB]='" + DOB + "',"+
                                                       "[EMAILADDRESS]='" + txt_email.Text + "',[ADDRESS]='" + richtxt_address.Text + "',"+
                                                       "[SOFTWARESTATUS]='" + status + "' WHERE [U_ID]='" + comboBox_U_ID.SelectedItem + "'";
                    OleDbCommand sqlcmd_update = new OleDbCommand(updation_query, con);
                    sqlcmd_update.ExecuteNonQuery();
                    con.Close();
                    con.Open();
                    
                    string update_login_query = "UPDATE [Login] SET [USER_NAME]='" + txt_User_Name.Text + "'," +
                                                       "[PASSWORD]='" + txt_password.Text + "' WHERE [USER_NAME]='" + txt_username_search.Text + "'";
                    OleDbCommand sqlcmd_update_login = new OleDbCommand(update_login_query, con);
                    sqlcmd_update_login.ExecuteNonQuery();
                    con.Close();
                    txt_confirmpassword.Text = "";
                    txt_email.Text = "";
                    txt_name.Text = "";
                    txt_password.Text = "";
                    txt_User_Name.Text = "";
                    combo_YEAR.Text = "YYYY";
                    combo_MONTH.Text = "MM";
                    //combo_Privilege.Text = "  --- Select Privilege ---";
                    combo_date.Text = "DD";
                    radio_female.Checked = false;
                    radio_Male.Checked = false;
                    richtxt_address.Text = "";
                    checkB_disable.Checked = false;
                    checkB_Enable.Checked = false;
                    txt_username_search.Text = "";
                    comboBox_U_ID.Text = " - Select -";
                    MessageBox.Show("Successfully Updated ");

                }
            }
            catch (Exception ex)
            {
              //  MessageBox.Show(ex.ToString());
            }
        }

        private void FRM_Registration_Load(object sender, EventArgs e)
        {
            try
            {
                /*Code For Displaying UserName*/
                //lbl_Login_Name.Text ="Login :"+ username;
                /*Code for timer Enable*/
                timer_time.Enabled = true;
                /*Code For Disable Validation Lables*/
                lbl_Validation_address.Visible = false;
                //lbl_Validation_Date.Visible = false;
                lbl_Validation_email.Visible = false;
                //lbl_Validation_gender.Visible = false;
                //lbl_Validation_Name.Visible = false;
                lbl_Validation_Notification.Visible = false;
                lbl_Validation_password.Visible = false;
                //lbl_Validation_Privilege.Visible = false;
                lbl_Validation_status.Visible = false;
                lbl_Validation_User_Name.Visible = false;
                Btn_Update.Visible = false;
                label1.Visible = false;
                checkB_disable.Visible = false;
                checkB_Enable.Visible = false;
                comboBox_U_ID.Visible = false;
                lbl_U_ID.Visible = false;
                lbl_Status.Visible = false;
                lbl_username_search.Visible = false;
                txt_username_search.Visible = false;
                button_search.Visible = false;
                /*Code For Inserting values in date comboboxes*/
                for (int date_int = 1; date_int <= 31; date_int++)
                {
                    combo_date.Items.Add(date_int.ToString());
                }
                for (int month_int = 1; month_int <= 12; month_int++)
                {
                    combo_MONTH.Items.Add(month_int.ToString());
                }
                for (int year_int = 1950; year_int <= 2017; year_int++)
                {
                    combo_YEAR.Items.Add(year_int.ToString());
                }
                

            }
            catch (Exception ex)
            {
              //  MessageBox.Show(ex.ToString());
            }
        }

        private void comboBox_U_ID_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                /*Code For Disable Validation Lables*/
                lbl_Validation_address.Visible = false;
               // lbl_Validation_Date.Visible = false;
                lbl_Validation_email.Visible = false;
               // lbl_Validation_gender.Visible = false;
                //lbl_Validation_Name.Visible = false;
                lbl_Validation_Notification.Visible = false;
                lbl_Validation_password.Visible = false;
                //lbl_Validation_Privilege.Visible = false;
                lbl_Validation_status.Visible = false;
                lbl_Validation_User_Name.Visible = false;
                OleDbConnection con = sqlconnection_obj.Database_Connection_Method();
                con.Open();
                OleDbCommand sqlcmd_select = new OleDbCommand("select * from [User_Registration] where U_ID='" + comboBox_U_ID.SelectedItem.ToString() + "'", con);
                OleDbDataReader sql_reader;
                sql_reader = sqlcmd_select.ExecuteReader();
                while (sql_reader.Read())
                {
                    txt_username_search.Text = sql_reader[2].ToString();
                    txt_User_Name.Text = sql_reader[2].ToString();
                    txt_password.Text = sql_reader[1].ToString();
                    //combo_Privilege.Text = sql_reader.GetString(4).ToString();
                    txt_name.Text = sql_reader[5].ToString();
                    string gender = sql_reader[6].ToString();
                    if (gender == "F") { radio_female.Checked = true; }
                    else { radio_Male.Checked = true; }
                    DateTime dob_full = Convert.ToDateTime(sql_reader[7].ToString());
                    string day = Convert.ToString(dob_full.Day);
                    string month = Convert.ToString(dob_full.Month);
                    string year = Convert.ToString(dob_full.Year);
                    combo_date.Text = day;
                    combo_MONTH.Text = month;
                    combo_YEAR.Text = year;
                    txt_email.Text = sql_reader[1].ToString();
                    richtxt_address.Text = sql_reader[8].ToString();
                    string status = sql_reader[10].ToString();
                    if (status == "E") { checkB_Enable.Checked = true; checkB_disable.Checked = false; }
                    else { checkB_disable.Checked = true; checkB_Enable.Checked = false; }
                }
            }
            catch (Exception ex)
            {
               // MessageBox.Show(ex.ToString());
            }
        }

        private void Btn_Cancel_Click(object sender, EventArgs e)
        {
            try
            {
                lbl_Validation_address.Visible = false;
                //lbl_Validation_Date.Visible = false;
                lbl_Validation_email.Visible = false;
                //lbl_Validation_gender.Visible = false;
                //lbl_Validation_Name.Visible = false;
                lbl_Validation_Notification.Visible = false;
                lbl_Validation_password.Visible = false;
                //lbl_Validation_Privilege.Visible = false;
                lbl_Validation_status.Visible = false;
                label1.Visible = false;

                lbl_Validation_User_Name.Visible = false;
                txt_confirmpassword.Text = "";
                txt_email.Text = "";
                txt_name.Text = "";
                txt_password.Text = "";
                txt_User_Name.Text = "";
                combo_YEAR.Text = "YYYY";
                combo_MONTH.Text = "MM";
                //combo_Privilege.Text = "  --- Select Privilege ---";
                combo_date.Text = "DD";
                radio_female.Checked = false;
                radio_Male.Checked = false;
                richtxt_address.Text = "";
                checkB_disable.Checked = false;
                checkB_Enable.Checked = false;
                txt_username_search.Text = "";
                comboBox_U_ID.Text = " - Select -";
            }
            catch (Exception ex)
            {
               // MessageBox.Show(ex.ToString());
            }
        }

        private void insertionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                /*Code For Change Form Text*/
                this.Text = "Registration";
                lbl_Validation_address.Visible = false;
                //lbl_Validation_Date.Visible = false;
                lbl_Validation_email.Visible = false;
                //lbl_Validation_gender.Visible = false;
                //lbl_Validation_Name.Visible = false;
                lbl_Validation_Notification.Visible = false;
                lbl_Validation_password.Visible = false;
                //lbl_Validation_Privilege.Visible = false;
                lbl_Validation_status.Visible = false;
                lbl_Validation_User_Name.Visible = false;
                Btn_Update.Visible = false;
                checkB_disable.Visible = false;
                checkB_Enable.Visible = false;
                comboBox_U_ID.Visible = false;
                lbl_U_ID.Visible = false;
                lbl_Status.Visible = false;
                Btn_Insert.Visible = true;
                button_search.Visible = false;
                lbl_username_search.Visible = false;
                txt_username_search.Visible = false;
            }
            catch (Exception ex)
            {
               // MessageBox.Show(ex.ToString());
            }
        }

        private void updationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                /*Code For Filling User_ID Combobox from DataBase*/

                OleDbConnection con = sqlconnection_obj.Database_Connection_Method();
                con.Open();
                OleDbCommand cmd = new OleDbCommand("select [U_ID] from [User_Registration]", con);
                OleDbDataReader sql_reader;
                sql_reader = cmd.ExecuteReader();
                comboBox_U_ID.Text = "--Select--";
                comboBox_U_ID.Items.Add("--Select--");
                while (sql_reader.Read())
                {
                    comboBox_U_ID.Items.Add(sql_reader[0].ToString());

                    //comboBox_U_ID.Items.Add(sql_reader[10].ToString());


                }
                con.Close();
                /*Code For Change Form Text*/
                this.Text = "Updation";
                lbl_username_search.Visible = true;
                txt_username_search.Visible = true;
                lbl_Validation_address.Visible = false;
                //lbl_Validation_Date.Visible = false;
                lbl_Validation_email.Visible = false;
               // lbl_Validation_gender.Visible = false;
               // lbl_Validation_Name.Visible = false;
                lbl_Validation_Notification.Visible = false;
                lbl_Validation_password.Visible = false;
                //lbl_Validation_Privilege.Visible = false;
                lbl_Validation_status.Visible = false;
                lbl_Validation_User_Name.Visible = false;
                Btn_Insert.Visible = false;
                Btn_Update.Visible = true;
                checkB_disable.Visible = true;
                checkB_Enable.Visible = true;
                comboBox_U_ID.Visible = true;
                lbl_U_ID.Visible = true;
                lbl_Status.Visible = true;
                button_search.Visible = true;
            }
            catch (Exception ex)
            {
               // MessageBox.Show(ex.ToString());
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                //FRM_Home hom_obj = new FRM_Home();
                //hom_obj.Show();
                //this.Dispose();
                this.Close();
                //FRM_MainHome frm_obj = (FRM_MainHome)this.MdiParent;
                ////frm_obj.g2.Visible = true; frm_obj.m1.Visible = true;
                //frm_obj.Show(); frm_obj.MdiParent = this;
                //frm_obj.g2.Visible = false; frm_obj.m1.Visible = false; frm_obj.g1.Visible = false;
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
            }
        }

        private void button_search_Click(object sender, EventArgs e)
        {
            try
            {
                /*Code For Disable Validation Lables*/
                lbl_Validation_address.Visible = false;
               // lbl_Validation_Date.Visible = false;
                lbl_Validation_email.Visible = false;
              //  lbl_Validation_gender.Visible = false;
                //lbl_Validation_Name.Visible = false;
                lbl_Validation_Notification.Visible = false;
                lbl_Validation_password.Visible = false;
                //lbl_Validation_Privilege.Visible = false;
                lbl_Validation_status.Visible = false;
                lbl_Validation_User_Name.Visible = false;
                OleDbConnection con = sqlconnection_obj.Database_Connection_Method();
                con.Open();
                OleDbCommand sqlcmd_select = new OleDbCommand("select * from [User_Registration] where U_NAME='" + txt_username_search.Text + "'", con);
                OleDbDataReader sql_reader;
                sql_reader = sqlcmd_select.ExecuteReader();
                while (sql_reader.Read())
                {
                    comboBox_U_ID.Text = sql_reader[0].ToString();
                    txt_User_Name.Text = sql_reader[3].ToString();
                    txt_password.Text = sql_reader[2].ToString();
                    //combo_Privilege.Text = sql_reader.GetString(4).ToString();
                    txt_name.Text = sql_reader[5].ToString();
                    string gender = sql_reader[6].ToString();
                    if (gender == "F") { radio_female.Checked = true; }
                    else { radio_Male.Checked = true; }
                    DateTime dob_full = Convert.ToDateTime(sql_reader[7].ToString());
                    string day = Convert.ToString(dob_full.Day);
                    string month = Convert.ToString(dob_full.Month);
                    string year = Convert.ToString(dob_full.Year);
                    combo_date.Text = day;
                    combo_MONTH.Text = month;
                    combo_YEAR.Text = year;
                    txt_email.Text = sql_reader[0].ToString();
                    richtxt_address.Text = sql_reader[8].ToString();
                    string status = sql_reader[3].ToString();
                    if (status == "E") { checkB_Enable.Checked = true; checkB_disable.Checked = false; }
                    else { checkB_disable.Checked = true; checkB_Enable.Checked = false; }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void timer_time_Tick(object sender, EventArgs e)
        {
            try
            {
                /*code for time*/
                DateTime time = System.DateTime.Now;
                //lbl_time.Text =":"+ Convert.ToString(time.ToLongTimeString());
                lbl_time.Text = "Time :" + Convert.ToString(time.ToLongTimeString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void checkB_Enable_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (checkB_Enable.Checked == true) { checkB_disable.Checked = false; }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void checkB_disable_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (checkB_disable.Checked == true) { checkB_Enable.Checked= false; }
               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void FRM_Registration_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }

        private void txt_User_Name_TextChanged(object sender, EventArgs e)
        {

        }

        private void txt_username_search_TextChanged(object sender, EventArgs e)
        {

        }

        private void txt_email_TextChanged(object sender, EventArgs e)
        {

        }

        private void FRM_Registration_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                //FRM_Home frm_obj = (FRM_Home)Application.OpenForms["FRM_Home"];
                //FRM_MainHome frm_obj = (FRM_MainHome)this.MdiParent;
                //frm_obj.Show();
                //frm_obj.g2.Visible = true; frm_obj.m1.Visible = true;
                FRM_MainHome frm_obj = (FRM_MainHome)Application.OpenForms["FRM_MainHome"];
                frm_obj.Show();




            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            //if (MessageBox.Show("Exit or No?", "Registration", MessageBoxButtons.YesNo,
            //          MessageBoxIcon.Information) == DialogResult.No)
            //{
            //    e.Cancel = true;
            //}
            //else
            //{
            FRM_MainHome f = new FRM_MainHome();
            f.Show();
            //}
        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void lbl_Login_Name_Click(object sender, EventArgs e)
        {

        }

        private void userDetailsToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void txt_confirmpassword_TextChanged(object sender, EventArgs e)
        {

        }

        private void lbl_password_Click(object sender, EventArgs e)
        {

        }

        private void lbl_User_name_Click(object sender, EventArgs e)
        {

        }

        private void txt_password_TextChanged(object sender, EventArgs e)
        {

        }

        private void lbl_Validation_password_Click(object sender, EventArgs e)
        {

        }

        private void lbl_time_Click(object sender, EventArgs e)
        {

        }

        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
        {

        }

        private void combo_date_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
