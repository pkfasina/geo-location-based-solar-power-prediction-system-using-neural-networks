﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.Xml.Linq;
using System.Xml;
using System.Net;
using System.Web;
using System.Web.Util;
using System.IO;

namespace solarpanel
{
    public partial class FRM_Home : Form
    {
        public GroupBox g1; public GroupBox g2; public MonthCalendar m1;
        public FRM_Home()
        {
            InitializeComponent();
            g1 = groupBox1;
            g2 = groupBox2;
            m1 = monthCalendar1;
           
        }
        ////**********not moving form*************//
        protected override void WndProc(ref Message message)
        {
            const int WM_SYSCOMMAND = 0x0112;
            const int SC_MOVE = 0xF010;

            switch (message.Msg)
            {
                case WM_SYSCOMMAND:
                    int command = message.WParam.ToInt32() & 0xfff0;
                    if (command == SC_MOVE)
                        return;
                    break;
            }

            base.WndProc(ref message);
        }
        ////**********************************//

        private void groupBox2_Enter(object sender, EventArgs e)
        {
            
        }

        private void FRM_Home_Load(object sender, EventArgs e)
        {
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
            this.Width = Screen.PrimaryScreen.WorkingArea.Width;

            
            //FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            //Left = Top = 0;
            //Width = Screen.PrimaryScreen.WorkingArea.Width;
            //Height = Screen.PrimaryScreen.WorkingArea.Height;

            try 
            {
                //groupBox2.Size = new Size(this.Size.Width - 490, this.Size.Height - 80);
                //pictureB_change.Size = new Size(groupBox2.Size.Width - 50, groupBox2.Size.Width - 450);
                //if (Convert.ToInt32(FRM_Login.authentication) == 2) 
                //{
                //picReg.Visible = false; picView.Visible = false;
                //panel1.Hide(); panel5.Hide();
                    //picView.Location = new Point(15, 51);
                    //picMainproject.Location = new Point(217, 51);
                    //picWeather.Location = new Point(121, 100);


                //}
                timer_timnpics.Enabled = true;
                lbl_loginName.Text = "User Name : " + FRM_MainHome.username;
                string var = System.Windows.Forms.Application.StartupPath;
                //string text = System.IO.File.ReadAllText(var + "\\connection_text.txt");
                pictureB_change.Image = Image.FromFile(var + "\\images\\1.jpg");
                //pictureB_change.Image = Image.FromFile(@"D:\Anaswara\SolarPower\Common_For_All_Project\Common_For_All_Project\images\1.jpg");
            }
            catch(Exception ex)
            {
              //  MessageBox.Show(ex.ToString());
            }
        }

        private void link_lbl_logout_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

        }
        int picture_no = 1;
        private void timer_timnpics_Tick(object sender, EventArgs e)
        {
            try
            {
                DateTime time = System.DateTime.Now;
                label6.Text ="Time :"+ Convert.ToString(time.ToLongTimeString());

               
              }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void timer1_picturebox_Tick(object sender, EventArgs e)
        {
            try
            {
                if (picture_no <= 6)
                {
                    string var = System.Windows.Forms.Application.StartupPath;
                    //string text = System.IO.File.ReadAllText(var + "\\connection_text.txt");
                    pictureB_change.Image = Image.FromFile(var+"\\images\\" + picture_no + ".jpg");
                    picture_no++;
                    if(picture_no==7)
                    { picture_no = 1; }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        
        
        

        private void FRM_Home_FormClosed(object sender, FormClosedEventArgs e)
        {
            FRM_MainHome frm_reg_obj = new FRM_MainHome();
            frm_reg_obj.Show();
        }
        public void hide()
        {
            groupBox2.Hide(); monthCalendar1.Hide();//  groupBox1.Hide();
        }
        public void shw()
        {
            groupBox2.Show(); //
        }
        

        
        private void FRM_Home_FormClosing(object sender, FormClosingEventArgs e)
        {
            //try
            //{
            //    //FRM_SOLAR_POWER_SIMULATE main_frm_obj = new FRM_SOLAR_POWER_SIMULATE();
            //    FormCollection fc = Application.OpenForms;            
            //    foreach (Form frm in fc)
            //    {
            //        if ((frm.Text != "Home") && (frm.Text != "Login"))
            //        {
            //            //frm.Close();
            //            frm.Hide();
            //        }
            //    }
            //}
            //catch (Exception )
            //{
            //    //MessageBox.Show(ex.ToString());
            //}


            if (MessageBox.Show("Exit or No?", "Solar Home", MessageBoxButtons.YesNo,
                      MessageBoxIcon.Information) == DialogResult.No)
            {
                e.Cancel = true;
            }
            else
            { 
                FRM_MainHome f = new FRM_MainHome(); 
                f.Show();
            }
        }



        private void picExit_Click(object sender, EventArgs e)
        {
            try
            {
               // FRM_MainHome fa = new FRM_MainHome();
               // fa.Show();

               

         
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            FRM_MainHome m = new FRM_MainHome();
            m.Show();
          

        }




        private bool CheckOpened(string name)
        {
            FormCollection fc = Application.OpenForms;
            foreach (Form frm in fc)
            {
                if (frm.Text == name)
                {
                    return true;
                }
            }
                return false;           
        }
       
        

        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
        {

        }

        
        
        
       

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }



        private void pictureBox2_Click(object sender, EventArgs e)
        {
            try
            {
                FRM_Settings view_obj = new FRM_Settings();
                FormCollection fc = Application.OpenForms;
                if (CheckOpened("Power Requirement"))
                {
                    view_obj.Activate(); g2.Visible = false; m1.Visible = false;// g1.Visible = false;
                }
                else
                {
                    view_obj.Show(); view_obj.MdiParent = this; g2.Visible = false; m1.Visible = false;// g1.Visible = false;
                }
                foreach (Form frm in fc)
                {
                    if ((frm.Text != "Power Requirement") && (frm.Text != "Home") && (frm.Text != "Login"))
                    {
                        //frm.Close();
                        frm.Hide();
                    }
                }
            }
            catch (Exception)
            {
                //MessageBox.Show(ex.ToString());
            }
        }

        private void pictureBox1_Click_1(object sender, EventArgs e)
        {
            try
            {
                Change_pwd cd = new Change_pwd();
                FormCollection fc = Application.OpenForms;
                if (CheckOpened("Change_pwd"))
                {
                    cd.Activate(); g2.Visible = false; m1.Visible = false;
                }
                else
                {

                    cd.Show(); cd.MdiParent = this; g2.Visible = false; m1.Visible = false;
                }
                foreach (Form frm in fc)
                {
                    if ((frm.Text != "Change_pwd") && (frm.Text != "Home") && (frm.Text != "Login"))
                    {
                        frm.Hide();
                    }
                }
            }
            catch (Exception)
            {
                //MessageBox.Show(ex.ToString());
            }
        }

        private void FRM_Home_Resize(object sender, EventArgs e)
        {
            if ((this.WindowState == FormWindowState.Minimized) || (this.WindowState == FormWindowState.Normal)
                || (this.WindowState == FormWindowState.Maximized))
            {
                this.ShowInTaskbar = true; this.Activate();
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void pictureB_change_Click(object sender, EventArgs e)
        {

        }

        private void panel11_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel9_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel8_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel7_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel6_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel5_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {

        }

        private void lbl_loginName_Click(object sender, EventArgs e)
        {

        }

        private void lbl_time_Click(object sender, EventArgs e)
        {

        }

        private void panel10_Paint(object sender, PaintEventArgs e)
        {

        }

       

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void lbl_loginName_Click_1(object sender, EventArgs e)
        {

        }
        private void picMainproject_Click(object sender, EventArgs e)
        {
            try
            {
                   FRM_SOLAR_POWER_SIMULATE main_frm_obj = new FRM_SOLAR_POWER_SIMULATE();
                    FormCollection fc = Application.OpenForms;
                    if (CheckOpened("Solar Power"))
                    {
                        main_frm_obj.Activate(); g2.Visible = false;  m1.Visible = false; g1.Visible = false;
                    }
                    else
                    {

                        main_frm_obj.Show(); main_frm_obj.MdiParent = this;
                        g2.Visible = false;  m1.Visible = false; g1.Visible = false;
                    }
                    foreach (Form frm in fc)
                    {
                        if ((frm.Text != "Solar Power") && (frm.Text != "Home") && (frm.Text != "Login"))
                        {
                            //frm.Close();
                            frm.Hide();
                        }
                    }
            }
            catch (Exception )
            {
                //MessageBox.Show(ex.ToString());
            }
        }

        private void lbl_loginName_Click_2(object sender, EventArgs e)
        {

        }

        private void link_lbl_logout_LinkClicked_1(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                this.Close();      //Application.Exit();   
                //FRM_MainHome frm_obj = (FRM_MainHome)Application.OpenForms["FRM_MainHome"];
                //frm_obj.Show();


                FRM_MainHome f = new FRM_MainHome();
                f.Show();
            }
            
 
            catch (Exception)
            {
                //  MessageBox.Show(ex.ToString());
            }


            FRM_MainHome fm= new FRM_MainHome();
            fm.Show();

        }
    }
}
