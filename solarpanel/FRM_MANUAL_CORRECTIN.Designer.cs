﻿namespace solarpanel
{
    partial class FRM_MANUAL_CORRECTIN
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.operationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aDDToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dELETEToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eDITToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sEARCHToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eXITToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.comboBox_CLIMATECONDITION = new System.Windows.Forms.ComboBox();
            this.txt_CLIMATE = new System.Windows.Forms.TextBox();
            this.txt_DESCRIPTION = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_ERRORLEVEL = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.button_UPDATE = new System.Windows.Forms.Button();
            this.button_DELETE = new System.Windows.Forms.Button();
            this.button_ADD = new System.Windows.Forms.Button();
            this.timer_timnpics = new System.Windows.Forms.Timer(this.components);
            this.BTN_OK = new System.Windows.Forms.Button();
            this.BTN_CANCEL = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.operationsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(709, 28);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // operationsToolStripMenuItem
            // 
            this.operationsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aDDToolStripMenuItem,
            this.dELETEToolStripMenuItem,
            this.eDITToolStripMenuItem,
            this.sEARCHToolStripMenuItem,
            this.eXITToolStripMenuItem});
            this.operationsToolStripMenuItem.Name = "operationsToolStripMenuItem";
            this.operationsToolStripMenuItem.Size = new System.Drawing.Size(96, 24);
            this.operationsToolStripMenuItem.Text = "Operations";
            this.operationsToolStripMenuItem.Click += new System.EventHandler(this.operationsToolStripMenuItem_Click);
            // 
            // aDDToolStripMenuItem
            // 
            this.aDDToolStripMenuItem.Name = "aDDToolStripMenuItem";
            this.aDDToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.aDDToolStripMenuItem.Text = "ADD";
            this.aDDToolStripMenuItem.Click += new System.EventHandler(this.aDDToolStripMenuItem_Click);
            // 
            // dELETEToolStripMenuItem
            // 
            this.dELETEToolStripMenuItem.Name = "dELETEToolStripMenuItem";
            this.dELETEToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.dELETEToolStripMenuItem.Text = "DELETE";
            this.dELETEToolStripMenuItem.Click += new System.EventHandler(this.dELETEToolStripMenuItem_Click);
            // 
            // eDITToolStripMenuItem
            // 
            this.eDITToolStripMenuItem.Name = "eDITToolStripMenuItem";
            this.eDITToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.eDITToolStripMenuItem.Text = "EDIT";
            this.eDITToolStripMenuItem.Click += new System.EventHandler(this.eDITToolStripMenuItem_Click);
            // 
            // sEARCHToolStripMenuItem
            // 
            this.sEARCHToolStripMenuItem.Name = "sEARCHToolStripMenuItem";
            this.sEARCHToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.sEARCHToolStripMenuItem.Text = "SEARCH";
            this.sEARCHToolStripMenuItem.Click += new System.EventHandler(this.sEARCHToolStripMenuItem_Click);
            // 
            // eXITToolStripMenuItem
            // 
            this.eXITToolStripMenuItem.Name = "eXITToolStripMenuItem";
            this.eXITToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.eXITToolStripMenuItem.Text = "EXIT";
            this.eXITToolStripMenuItem.Click += new System.EventHandler(this.eXITToolStripMenuItem_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.comboBox_CLIMATECONDITION);
            this.groupBox1.Controls.Add(this.txt_CLIMATE);
            this.groupBox1.Controls.Add(this.txt_DESCRIPTION);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txt_ERRORLEVEL);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(13, 63);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Size = new System.Drawing.Size(667, 265);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // comboBox_CLIMATECONDITION
            // 
            this.comboBox_CLIMATECONDITION.FormattingEnabled = true;
            this.comboBox_CLIMATECONDITION.Location = new System.Drawing.Point(267, 64);
            this.comboBox_CLIMATECONDITION.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.comboBox_CLIMATECONDITION.Name = "comboBox_CLIMATECONDITION";
            this.comboBox_CLIMATECONDITION.Size = new System.Drawing.Size(203, 24);
            this.comboBox_CLIMATECONDITION.TabIndex = 84;
            this.comboBox_CLIMATECONDITION.SelectedIndexChanged += new System.EventHandler(this.comboBox_CLIMATECONDITION_SelectedIndexChanged);
            // 
            // txt_CLIMATE
            // 
            this.txt_CLIMATE.Location = new System.Drawing.Point(265, 64);
            this.txt_CLIMATE.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txt_CLIMATE.Name = "txt_CLIMATE";
            this.txt_CLIMATE.Size = new System.Drawing.Size(223, 22);
            this.txt_CLIMATE.TabIndex = 81;
            // 
            // txt_DESCRIPTION
            // 
            this.txt_DESCRIPTION.Location = new System.Drawing.Point(267, 151);
            this.txt_DESCRIPTION.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txt_DESCRIPTION.Multiline = true;
            this.txt_DESCRIPTION.Name = "txt_DESCRIPTION";
            this.txt_DESCRIPTION.Size = new System.Drawing.Size(348, 89);
            this.txt_DESCRIPTION.TabIndex = 80;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(27, 151);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(120, 18);
            this.label3.TabIndex = 79;
            this.label3.Text = "DESCRIPTION";
            // 
            // txt_ERRORLEVEL
            // 
            this.txt_ERRORLEVEL.Location = new System.Drawing.Point(267, 102);
            this.txt_ERRORLEVEL.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txt_ERRORLEVEL.Name = "txt_ERRORLEVEL";
            this.txt_ERRORLEVEL.Size = new System.Drawing.Size(201, 22);
            this.txt_ERRORLEVEL.TabIndex = 78;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(27, 111);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(167, 18);
            this.label5.TabIndex = 77;
            this.label5.Text = "ERROR LEVEL(in %)";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(27, 68);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(175, 18);
            this.label1.TabIndex = 76;
            this.label1.Text = "CLIMATE CONDITION";
            // 
            // button_UPDATE
            // 
            this.button_UPDATE.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_UPDATE.Location = new System.Drawing.Point(209, 364);
            this.button_UPDATE.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button_UPDATE.Name = "button_UPDATE";
            this.button_UPDATE.Size = new System.Drawing.Size(112, 38);
            this.button_UPDATE.TabIndex = 77;
            this.button_UPDATE.Text = "UPDATE";
            this.button_UPDATE.UseVisualStyleBackColor = true;
            this.button_UPDATE.Click += new System.EventHandler(this.button_UPDATE_Click);
            // 
            // button_DELETE
            // 
            this.button_DELETE.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_DELETE.Location = new System.Drawing.Point(210, 365);
            this.button_DELETE.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button_DELETE.Name = "button_DELETE";
            this.button_DELETE.Size = new System.Drawing.Size(107, 37);
            this.button_DELETE.TabIndex = 76;
            this.button_DELETE.Text = "DELETE";
            this.button_DELETE.UseVisualStyleBackColor = true;
            this.button_DELETE.Click += new System.EventHandler(this.button_DELETE_Click);
            // 
            // button_ADD
            // 
            this.button_ADD.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_ADD.Location = new System.Drawing.Point(213, 365);
            this.button_ADD.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button_ADD.Name = "button_ADD";
            this.button_ADD.Size = new System.Drawing.Size(108, 37);
            this.button_ADD.TabIndex = 75;
            this.button_ADD.Text = "ADD";
            this.button_ADD.UseVisualStyleBackColor = true;
            this.button_ADD.Click += new System.EventHandler(this.button_ADD_Click);
            // 
            // timer_timnpics
            // 
            this.timer_timnpics.Tick += new System.EventHandler(this.timer_timnpics_Tick);
            // 
            // BTN_OK
            // 
            this.BTN_OK.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_OK.Location = new System.Drawing.Point(210, 364);
            this.BTN_OK.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BTN_OK.Name = "BTN_OK";
            this.BTN_OK.Size = new System.Drawing.Size(112, 38);
            this.BTN_OK.TabIndex = 80;
            this.BTN_OK.Text = "OK";
            this.BTN_OK.UseVisualStyleBackColor = true;
            this.BTN_OK.Click += new System.EventHandler(this.BTN_OK_Click);
            // 
            // BTN_CANCEL
            // 
            this.BTN_CANCEL.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_CANCEL.Location = new System.Drawing.Point(422, 364);
            this.BTN_CANCEL.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BTN_CANCEL.Name = "BTN_CANCEL";
            this.BTN_CANCEL.Size = new System.Drawing.Size(112, 38);
            this.BTN_CANCEL.TabIndex = 81;
            this.BTN_CANCEL.Text = "Cancel";
            this.BTN_CANCEL.UseVisualStyleBackColor = true;
            this.BTN_CANCEL.Visible = false;
            this.BTN_CANCEL.Click += new System.EventHandler(this.BTN_CANCEL_Click);
            // 
            // FRM_MANUAL_CORRECTIN
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(709, 415);
            this.Controls.Add(this.BTN_CANCEL);
            this.Controls.Add(this.BTN_OK);
            this.Controls.Add(this.button_UPDATE);
            this.Controls.Add(this.button_DELETE);
            this.Controls.Add(this.button_ADD);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "FRM_MANUAL_CORRECTIN";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Mannual Correction";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FRM_MANUAL_CORRECTIN_FormClosed);
            this.Load += new System.EventHandler(this.FRM_MANUAL_CORRECTIN_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem operationsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aDDToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dELETEToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eDITToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sEARCHToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eXITToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox comboBox_CLIMATECONDITION;
        private System.Windows.Forms.TextBox txt_CLIMATE;
        private System.Windows.Forms.TextBox txt_DESCRIPTION;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_ERRORLEVEL;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button_UPDATE;
        private System.Windows.Forms.Button button_DELETE;
        private System.Windows.Forms.Button button_ADD;
        private System.Windows.Forms.Timer timer_timnpics;
        private System.Windows.Forms.Button BTN_OK;
        private System.Windows.Forms.Button BTN_CANCEL;
    }
}