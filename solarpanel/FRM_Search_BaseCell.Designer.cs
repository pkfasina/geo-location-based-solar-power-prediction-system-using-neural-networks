﻿namespace solarpanel
{
    partial class FRM_Search_BaseCell
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.list_view_Material = new System.Windows.Forms.ListBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lbl_modulerate = new System.Windows.Forms.Label();
            this.lbl_voltage = new System.Windows.Forms.Label();
            this.lbl_coast = new System.Windows.Forms.Label();
            this.lbl_modulearea = new System.Windows.Forms.Label();
            this.lbl_panelmaterial = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txt_modulerating = new System.Windows.Forms.TextBox();
            this.txt_voltage = new System.Windows.Forms.TextBox();
            this.txt_cost = new System.Windows.Forms.TextBox();
            this.txt_moduleare = new System.Windows.Forms.TextBox();
            this.txt_module_material = new System.Windows.Forms.TextBox();
            this.BTN_Cancel = new System.Windows.Forms.Button();
            this.BTN_HOME = new System.Windows.Forms.Button();
            this.Select = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.timer_timnpics = new System.Windows.Forms.Timer(this.components);
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // list_view_Material
            // 
            this.list_view_Material.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.list_view_Material.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.list_view_Material.FormattingEnabled = true;
            this.list_view_Material.ItemHeight = 22;
            this.list_view_Material.Location = new System.Drawing.Point(16, 31);
            this.list_view_Material.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.list_view_Material.Name = "list_view_Material";
            this.list_view_Material.Size = new System.Drawing.Size(205, 268);
            this.list_view_Material.TabIndex = 1;
            this.list_view_Material.SelectedIndexChanged += new System.EventHandler(this.list_view_Material_SelectedIndexChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lbl_modulerate);
            this.groupBox1.Controls.Add(this.lbl_voltage);
            this.groupBox1.Controls.Add(this.lbl_coast);
            this.groupBox1.Controls.Add(this.lbl_modulearea);
            this.groupBox1.Controls.Add(this.lbl_panelmaterial);
            this.groupBox1.Location = new System.Drawing.Point(231, 25);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Size = new System.Drawing.Size(243, 277);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // lbl_modulerate
            // 
            this.lbl_modulerate.AutoSize = true;
            this.lbl_modulerate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_modulerate.Location = new System.Drawing.Point(20, 239);
            this.lbl_modulerate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_modulerate.Name = "lbl_modulerate";
            this.lbl_modulerate.Size = new System.Drawing.Size(116, 18);
            this.lbl_modulerate.TabIndex = 8;
            this.lbl_modulerate.Text = "Module Rating";
            // 
            // lbl_voltage
            // 
            this.lbl_voltage.AutoSize = true;
            this.lbl_voltage.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_voltage.Location = new System.Drawing.Point(20, 197);
            this.lbl_voltage.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_voltage.Name = "lbl_voltage";
            this.lbl_voltage.Size = new System.Drawing.Size(108, 18);
            this.lbl_voltage.TabIndex = 6;
            this.lbl_voltage.Text = "Voltage / Cell";
            // 
            // lbl_coast
            // 
            this.lbl_coast.AutoSize = true;
            this.lbl_coast.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_coast.Location = new System.Drawing.Point(20, 145);
            this.lbl_coast.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_coast.Name = "lbl_coast";
            this.lbl_coast.Size = new System.Drawing.Size(114, 18);
            this.lbl_coast.TabIndex = 5;
            this.lbl_coast.Text = "Cost / Module";
            // 
            // lbl_modulearea
            // 
            this.lbl_modulearea.AutoSize = true;
            this.lbl_modulearea.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_modulearea.Location = new System.Drawing.Point(20, 97);
            this.lbl_modulearea.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_modulearea.Name = "lbl_modulearea";
            this.lbl_modulearea.Size = new System.Drawing.Size(102, 18);
            this.lbl_modulearea.TabIndex = 4;
            this.lbl_modulearea.Text = "Module Area";
            // 
            // lbl_panelmaterial
            // 
            this.lbl_panelmaterial.AutoSize = true;
            this.lbl_panelmaterial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_panelmaterial.Location = new System.Drawing.Point(20, 44);
            this.lbl_panelmaterial.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_panelmaterial.Name = "lbl_panelmaterial";
            this.lbl_panelmaterial.Size = new System.Drawing.Size(179, 18);
            this.lbl_panelmaterial.TabIndex = 3;
            this.lbl_panelmaterial.Text = "Type Of Panel Material";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txt_modulerating);
            this.groupBox2.Controls.Add(this.txt_voltage);
            this.groupBox2.Controls.Add(this.txt_cost);
            this.groupBox2.Controls.Add(this.txt_moduleare);
            this.groupBox2.Controls.Add(this.txt_module_material);
            this.groupBox2.Location = new System.Drawing.Point(481, 25);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Size = new System.Drawing.Size(331, 277);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Enter += new System.EventHandler(this.groupBox2_Enter);
            // 
            // txt_modulerating
            // 
            this.txt_modulerating.Location = new System.Drawing.Point(37, 233);
            this.txt_modulerating.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txt_modulerating.Name = "txt_modulerating";
            this.txt_modulerating.Size = new System.Drawing.Size(249, 22);
            this.txt_modulerating.TabIndex = 5;
            // 
            // txt_voltage
            // 
            this.txt_voltage.Location = new System.Drawing.Point(37, 191);
            this.txt_voltage.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txt_voltage.Name = "txt_voltage";
            this.txt_voltage.Size = new System.Drawing.Size(249, 22);
            this.txt_voltage.TabIndex = 3;
            // 
            // txt_cost
            // 
            this.txt_cost.Location = new System.Drawing.Point(37, 139);
            this.txt_cost.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txt_cost.Name = "txt_cost";
            this.txt_cost.Size = new System.Drawing.Size(249, 22);
            this.txt_cost.TabIndex = 2;
            // 
            // txt_moduleare
            // 
            this.txt_moduleare.Location = new System.Drawing.Point(37, 91);
            this.txt_moduleare.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txt_moduleare.Name = "txt_moduleare";
            this.txt_moduleare.Size = new System.Drawing.Size(249, 22);
            this.txt_moduleare.TabIndex = 1;
            // 
            // txt_module_material
            // 
            this.txt_module_material.Location = new System.Drawing.Point(37, 42);
            this.txt_module_material.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txt_module_material.Name = "txt_module_material";
            this.txt_module_material.Size = new System.Drawing.Size(249, 22);
            this.txt_module_material.TabIndex = 0;
            // 
            // BTN_Cancel
            // 
            this.BTN_Cancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_Cancel.ForeColor = System.Drawing.Color.RoyalBlue;
            this.BTN_Cancel.Location = new System.Drawing.Point(449, 20);
            this.BTN_Cancel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BTN_Cancel.Name = "BTN_Cancel";
            this.BTN_Cancel.Size = new System.Drawing.Size(145, 37);
            this.BTN_Cancel.TabIndex = 19;
            this.BTN_Cancel.Text = "Cancel";
            this.BTN_Cancel.UseVisualStyleBackColor = true;
            this.BTN_Cancel.Click += new System.EventHandler(this.BTN_Cancel_Click);
            // 
            // BTN_HOME
            // 
            this.BTN_HOME.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_HOME.ForeColor = System.Drawing.Color.RoyalBlue;
            this.BTN_HOME.Location = new System.Drawing.Point(280, 21);
            this.BTN_HOME.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BTN_HOME.Name = "BTN_HOME";
            this.BTN_HOME.Size = new System.Drawing.Size(145, 37);
            this.BTN_HOME.TabIndex = 20;
            this.BTN_HOME.Text = "Home";
            this.BTN_HOME.UseVisualStyleBackColor = true;
            this.BTN_HOME.Visible = false;
            this.BTN_HOME.Click += new System.EventHandler(this.BTN_HOME_Click);
            // 
            // Select
            // 
            this.Select.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Select.ForeColor = System.Drawing.Color.RoyalBlue;
            this.Select.Location = new System.Drawing.Point(280, 20);
            this.Select.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Select.Name = "Select";
            this.Select.Size = new System.Drawing.Size(145, 37);
            this.Select.TabIndex = 21;
            this.Select.Text = "Select";
            this.Select.UseVisualStyleBackColor = true;
            this.Select.Click += new System.EventHandler(this.Select_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.Select);
            this.groupBox3.Controls.Add(this.BTN_Cancel);
            this.groupBox3.Controls.Add(this.BTN_HOME);
            this.groupBox3.Location = new System.Drawing.Point(21, 319);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox3.Size = new System.Drawing.Size(796, 68);
            this.groupBox3.TabIndex = 22;
            this.groupBox3.TabStop = false;
            // 
            // timer_timnpics
            // 
            this.timer_timnpics.Tick += new System.EventHandler(this.timer_timnpics_Tick);
            // 
            // FRM_Search_BaseCell
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightGray;
            this.ClientSize = new System.Drawing.Size(833, 423);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.list_view_Material);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "FRM_Search_BaseCell";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Select Cell Material";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FRM_Search_BaseCell_FormClosed);
            this.Load += new System.EventHandler(this.FRM_Search_BaseCell_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox list_view_Material;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lbl_voltage;
        private System.Windows.Forms.Label lbl_coast;
        private System.Windows.Forms.Label lbl_modulearea;
        private System.Windows.Forms.Label lbl_panelmaterial;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txt_voltage;
        private System.Windows.Forms.TextBox txt_cost;
        private System.Windows.Forms.TextBox txt_moduleare;
        private System.Windows.Forms.TextBox txt_module_material;
        private System.Windows.Forms.Button BTN_Cancel;
        private System.Windows.Forms.Button BTN_HOME;
        private System.Windows.Forms.Button Select;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Timer timer_timnpics;
        private System.Windows.Forms.Label lbl_modulerate;
        private System.Windows.Forms.TextBox txt_modulerating;
    }
}