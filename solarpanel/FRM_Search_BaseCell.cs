﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;
namespace solarpanel
{
    public partial class FRM_Search_BaseCell : Form
    {
        public FRM_Search_BaseCell()
        {
            InitializeComponent();
        }
        sqlconnectionClass1 sqlconnection_obj = new sqlconnectionClass1();
        private void FRM_Search_BaseCell_Load(object sender, EventArgs e)
        {
            try
            {
                //timer_timnpics.Enabled = true;
                //lbl_loginname.Text = "User Name :" + FRM_Login.username;
                OleDbConnection sql_con = sqlconnection_obj.Database_Connection_Method();
                sql_con.Open();
                string select_query = "Select Type from [panel_configuration]";
                OleDbCommand cmd = new OleDbCommand(select_query, sql_con);
                OleDbDataReader sql_read = cmd.ExecuteReader();
                while (sql_read.Read())
                {                    
                        list_view_Material.Items.Add(sql_read[0].ToString());
                 }
                sql_con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        public static string cost = "00.00", area = "Unit x Unit", material = "--Select--", voltage = "00.00",modulerating="00.00",modul_area;
        private void Select_Click(object sender, EventArgs e)
        {
            try
            {
                if (list_view_Material.SelectedItem != null)
                {
                    cost = txt_cost.Text;
                    area = txt_moduleare.Text + "x" + txt_moduleare.Text;
                    material = txt_module_material.Text;
                    voltage = txt_voltage.Text;
                    modulerating = txt_modulerating.Text;
                    modul_area = txt_moduleare.Text;
                    FRM_SOLAR_POWER_SIMULATE frm_simulate_obj = (FRM_SOLAR_POWER_SIMULATE)Application.OpenForms["FRM_SOLAR_POWER_SIMULATE"];
                    //frm_simulate_obj.Show();
                    frm_simulate_obj.lbl_result_cost_module.Text = FRM_Search_BaseCell.cost;
                    frm_simulate_obj.lbl_result_typematerial.Text = FRM_Search_BaseCell.material;
                    frm_simulate_obj.lbl_result_voltage.Text = FRM_Search_BaseCell.voltage;
                    frm_simulate_obj.lbl_result_modultarea.Text = FRM_Search_BaseCell.area;
                    frm_simulate_obj.lbl_result_modulerating.Text = FRM_Search_BaseCell.modulerating;
                    this.Hide();
                    frm_simulate_obj.Refresh();

                    FRM_BASECELLCONFIG frm_obj = (FRM_BASECELLCONFIG)Application.OpenForms["FRM_BASECELLCONFIG"];
                    frm_obj.Close();
                }

            }
            catch (Exception )
            {
              //  MessageBox.Show(ex.ToString());
            }
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void BTN_Cancel_Click(object sender, EventArgs e)
        {
            try
            {
                //FRM_SOLAR_POWER_SIMULATE frm_simulate_obj = new FRM_SOLAR_POWER_SIMULATE();
                //frm_simulate_obj.Show();
                this.Hide();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        private void BTN_HOME_Click(object sender, EventArgs e)
        {
            try
            {
                //FRM_SOLAR_POWER_SIMULATE frm_simulate_obj = new FRM_SOLAR_POWER_SIMULATE();
                //frm_simulate_obj.Show();
                this.Hide();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void list_view_Material_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                OleDbConnection sql_con = sqlconnection_obj.Database_Connection_Method();
                sql_con.Open();
                string select_query = "Select * from [panel_configuration] where TYPE='" + list_view_Material.SelectedItem + "'";
                OleDbCommand select_cmd = new OleDbCommand(select_query,sql_con);
                OleDbDataReader sql_read= select_cmd.ExecuteReader();
                while(sql_read.Read())
                {
                    txt_cost.Text = sql_read[4].ToString();
                    txt_module_material.Text = sql_read[1].ToString();
                    txt_moduleare.Text = sql_read[2].ToString();
                    txt_voltage.Text = sql_read[3].ToString();
                    txt_modulerating.Text = sql_read[5].ToString();
                }
                sql_con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void FRM_Search_BaseCell_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                //FRM_SOLAR_POWER_SIMULATE frm_simulate_obj = new FRM_SOLAR_POWER_SIMULATE();
                //frm_simulate_obj.Show();
                this.Hide();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        private void timer_timnpics_Tick(object sender, EventArgs e)
        {
            try
            {
                DateTime time = System.DateTime.Now;
               // lbl_time.Text = "Time :" + Convert.ToString(time.ToLongTimeString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
    }
}
